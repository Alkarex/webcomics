# Transcript of Pepper&Carrot Episode 12 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 12: Autumn Clearout

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|9|True|Cling
Sound|10|True|Clang
Sound|11|False|Clong
Cayenne|1|False|Laughing Potions
Cayenne|2|False|Mega-Hairgrowth Potions
Cayenne|3|False|Stink-bubble Potions
Cayenne|4|False|"Bright-Side" Potions
Cayenne|5|False|Smoke Potions...
Cayenne|6|False|... to name just a few!
Pepper|7|True|Yeah, I know:
Pepper|8|False|"A-true-witch-of-Chaosah-wouldn't-create-these-kinds-of-potions".
Cayenne|13|True|I need to go to the market in Komona.
Cayenne|14|False|Make all this disappear while I'm gone; it wouldn't be good for little jokers to find it and play around with it.
Cayenne|12|False|Exactly. As well it should be.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|2|False|Ziooum!
Pepper|3|False|Shoot!
Pepper|4|True|Bury everything?!
Pepper|5|False|But that'll take hours, not to mention the blisters we'll have by the end of it!
Sound|6|False|Boom
Pepper|1|False|Make it disappear? Easy! Yipee!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|"And by no means use magic. A true witch of Chaosah does not use magic for daily chores."
Pepper|2|False|Grrr!
Pepper|3|True|CARROT!
Pepper|4|False|Book seven of Chaosah: "Gravitational Fields"... ...without delay!
Pepper|5|False|I'll show that old busy-body crow what a true witch of Chaosah is!
Sound|6|True|Tam
Sound|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|2|False|VROOoooooooooo
Cayenne|3|False|... A Chaosahn Black Hole?!...
Pepper|4|False|?!
Cayenne|5|False|... That's really what you understand by: "don't use magic"?
Pepper|6|False|... Hey... Weren't you supposed to be at the market?
Cayenne|7|True|Of course not!
Cayenne|8|False|And I was right too; it's simply impossible to leave you without constant surveillance!
Pepper|1|False|GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|wooowoowoowoo
Sound|7|False|wooowoowoowoo
Sound|6|False|Bam
Cayenne|8|True|And there you go: Nearly everything will gravitate to stable orbits or Lagrange points and stay floating around.
Carrot|10|False|?!
Cayenne|2|True|... Just look at this.
Cayenne|3|True|Insufficient mass!
Cayenne|4|True|Weak gravitational field differential!
Cayenne|5|False|Even the closest stable circular orbit is too small!
Cayenne|9|False|Even only a slightly larger Event Horizon would have done the trick!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Cayenne|2|False|!!!
Cayenne|3|False|CURSUS CANCELLARE MAXIMUS!
Sound|4|False|ShKlak!
Narrator|5|False|- FIN -
Credits|6|False|10/2015 - Art & Scenario: David Revoy, English Translation: Alex Gryson

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 575 Patrons:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|4|False|License: Creative Commons Attribution 4.0 Source: available at www.peppercarrot.com Software: this episode was 100% drawn with libre software Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 on Linux Mint 17.2
