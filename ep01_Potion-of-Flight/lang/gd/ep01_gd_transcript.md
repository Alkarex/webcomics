# Transcript of Pepper&Carrot Episode 01 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 1: An deoch-sgiathaidh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|...agus beagan gleus oirre
Peabar|4|False|...cha chreid mi nach eil i caran lag fhathast
Fuaim|2|True|SSS
Fuaim|3|False|SSS
Fuaim|5|True|PLUB
Fuaim|6|False|PLUB

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|taghta ... tha i foirfe a-nis!
Peabar|2|False|BIS! Na bean ris!
Fuaim|3|False|SPLAIS

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|'Eil thu toilichte ma-thà ?!
Urram|2|False|WWW.PEPPERCARROT.COM 05/2014
