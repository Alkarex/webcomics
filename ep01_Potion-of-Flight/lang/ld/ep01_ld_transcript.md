# Transcript of Pepper&Carrot Episode 01 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 1: Rana Shumáadethu

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|...ib thesh nonede
Loyud|4|False|...mmm do be thalenal waá
Zho|2|True|SHH
Zho|3|False|SHH
Zho|5|True|LOB
Zho|6|False|LOB
Zho|7|False|ASHAN

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|aá... shad be wa
Loyud|2|False|RA! Bó ne lith beth
Zho|3|False|AÁLÁAN

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Thena?!
Dohiná|2|False|WWW.PEPPERCARROT.COM 05/2014
