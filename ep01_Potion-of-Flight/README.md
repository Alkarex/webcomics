﻿# Episode 1: The Potion of Flight

![cover of episode 1](https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/Pepper-and-Carrot_by-David-Revoy_E01.jpg)

2016-01-08: To compile this episode before the 2016 refactoring, you'll need older Krita sources files in 2016-01-08_ep01_Potion-of-Flight.zip (available in /src folder of the episode [https://www.peppercarrot.com/0_sources/](https://www.peppercarrot.com/0_sources/)).
