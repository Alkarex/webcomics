# Transcript of Pepper&Carrot Episode 21 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 21: Co-fharpais nan cleasan-buidseachais

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sgrìobhte|1|False|On a tha fèill mhòr air Tha Baile Chomona a’ toirt dhuibh Farpais buidseachais
Sgrìobhte|2|False|50 000Co de dhuais
Sgrìobhte|3|False|Air a’ chleas-bhuidseachais as iongantaiche! 150Co aig an doras Raon-spòrs mòr cuibhrichte ùr Chomona Là na Sabaid Aig 10 uairean mocheirigh
Sgrìobhte|4|False|CUIREADH SÒNRAICHTE
Peabar|5|False|Tha mi deiseil! Tiugainn, a Churrain!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|A bheil sibh fhathast cinnteach nach dig sibh còmhla rinn?
Cìob|2|False|Tha. ’S e draoidheachd a bhios sinn ris san Dubh-choimeasgachd seach faoin-chleasan dhan mhòr-shluagh.
Sgrìobhte|3|False|AM PÀIPEAR MÒR
Sgrìobhte|4|False|THA DEAS-GHNÀTH FOSGLAIDH AIR RAON-SPÒRS ÙR CHOMONA AN-DIUGH
Sgrìobhte|5|False|THA DRAGH AIR LUCHD-CÒMHNAIDH MU CHÌSEAN
Sgrìobhte|6|False|CHAN EIL TICEAD AIR FHÀGAIL: ’S CINNTEACH GUN SOIRBHICH LEIS!
Sgrìobhte|7|False|AN-SHOCAIR PHOILITIGEACH ANN AN TÌR AN ACHD
Sgrìobhte|8|False|THA 80% DHE NA SGÌREAN FO BHUAIDH AIRGEADRA ACHD
Sgrìobhte|9|False|THA DEAGH LÈIRSINN AGAD
Peabar|10|True|Mòran taing gum faod mi pàirt a ghabhail ann!
Peabar|11|True|Guidheam gum buannaich mi e!
Peabar|12|False|Chì mi air an fheasgar sibh!
Cìob|14|False|...
Fuaim|13|False|splad!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
An èisteachd|2|False|Clapan
Àrd-bhàillidh Chomona|1|False|Mar àrd-bhàillidh Chomona, tha mi a’ gairm gu bheil an dàrna co-fharpais buideachais air fhosgladh!
Àrd-bhàillidh Chomona|3|True|Tha fios agam gun robh fadachd oirbh ris an tachartas seo!
Àrd-bhàillidh Chomona|4|False|Mòran taing gun dàinig sibh o cheithir rannan ruadha Theireabha ach am fidreadh sibh an sàr-raon-spòrs mòr cuibhrichte ùr againn!
Àrd-bhàillidh Chomona|5|False|Nise, cuirim ur n-aithne air na co-fharpaisich a bhios againn!
Peabar|7|True|A Churrain?! Dè fo na gealaichean?!
Peabar|8|True|Dùisg! Tha e air tòiseachadh!
Peabar|9|False|’S a h-uile duine a’ coimhead oirnn!
Fuaim|10|False|Brog!
Curran|6|False|Srann ...|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
An èisteachd|3|False|Clapan
Àrd-bhàillidh Chomona|1|True|Tha i deas-làmhach le draoidheachd ath-bheòthachail na Sòmbaidheachd ’s tha sinn moiteil fàilte a chur air...
Àrd-bhàillidh Chomona|2|False|COSTAG!
Àrd-bhàillidh Chomona|4|True|Seo bana-bhuidseach na Clann-fhlùrachd a tha làn-eòlach air rùintean-dìomhair nan lusan ’s nam beathaichean! Cuiribh fàilte air...
Àrd-bhàillidh Chomona|5|False|BUIDHEAG
Àrd-bhàillidh Chomona|6|False|Agus a-nis, taing do chomasan teicneolach Chomona, seo an t-aon àite air an t-saoghal far am faic sibh...
Fuaim|7|False|Flop!
Àrd-bhàillidh Chomona|8|True|...bana-bhuidseach à doimhne na mara a tha ’na maighstir draoidheachd chumhachdach na h-Uisgeachd!
Àrd-bhàillidh Chomona|9|False|MUIRLINN!
Peabar|10|False|An Uisgeachd?! An-seo?! Tha sin iongantach!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
An èisteachd|9|False|Clapan
Curran|1|False|Rrùdail
Peabar|2|True|A Churrain!
Peabar|3|True|Tha an turas againne a’ tighinn!
Peabar|4|True|Dèan foighidinn!
Peabar|5|True|Feuch e
Peabar|6|False|airson an-diugh!
Àrd-bhàillidh Chomona|7|True|Seo bana-bhuidseach an Achd spioradail chràbhaich, tha e ’na thoileachas mòr dhomh gu bheil i ’nar cuideachd an-diugh...
Àrd-bhàillidh Chomona|8|False|SIDIMI!
Peabar|10|False|A Churrain! Ri m’ CHOIS!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
An èisteachd|7|False|Hà!
Àrd-bhàillidh Chomona|1|False|Thàinig an t-àm gun ruigeamaid an co-fharpaisiche rùnach seo a nì ràiteachas dhe dhraoidheachd loinnreach na “Magmachd” dhuinn...
Àrd-bhàillidh Chomona|3|True|Ò! Cò shaoileadh! Seo ise! Cò th’ ann ach...
Àrd-bhàillidh Chomona|4|False|CRÒCH!
Fuaim|2|False|Clap!
Àrd-bhàillidh Chomona|6|False|À! Cha chreid mi nach eil duilgheadas aig co-fharpaisiche mu thràth!
Fuaim|5|False|Spliut!
Àrd-bhàillidh Chomona|8|False|A nighean!! Till dhan àite agad!!
Peabar|9|False|A Churrain! Sguir dheth!
Fuaim|10|False|Sraon!
Peabar|11|False|?!
Fuaim|12|False|Splais!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Hà-hà! Tapadh leat, a Mhuirlinn! ’S tha mi duilich...
Peabar|2|True|Cà ’il thusa a’ dol??!
Peabar|3|False|Ri m’ chois LÀRACH NAM BONN!
Àrd-bhàillidh Chomona|4|True|Sin sin a-nist! Seo dhuibh an co-fharpaisiche mu dheireadh ma-thà ’s bheir i dhuinn buidseachas... tha... àraid... na Dubh-choimeasgachd...
Àrd-bhàillidh Chomona|5|False|PEABAR!
Àrd-bhàillidh Chomona|6|False|On a bhuannaich i co-fharpais nan deoch, thairg sinn dreuchd shònraichte dhi!
Peabar|7|False|?!...
Àrd-bhàillidh Chomona|8|False|Seo as adhbhar gun...
Àrd-bhàillidh Chomona|9|False|... dèid i ’na britheamh!
Fuaim|10|False|Beum
An èisteachd|11|True|Clapan
An èisteachd|12|True|Clapan
An èisteachd|13|True|Clapan
An èisteachd|14|True|Clapan
An èisteachd|15|False|Clapan
Sgrìobhte|16|False|Farpais buidseachais Coiste oifigeil
Peabar|17|False|Hò-rò ’s hò-rò eile.
Sgrìobhte|18|False|A’ Bhanrigh Àine
Sgrìobhte|19|False|Peabar
Sgrìobhte|20|False|An Rìgh Reòthach
Neach-aithris|21|False|Ri leantainn...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|02/2017 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|2|False|Dian-bheachdachadh: Craig Maloney, Quiralta, Nicolas Artance, Talime, Valvin.
Urram|3|False|Piseach air na còmhraidhean: Craig Maloney, Jookia, Nicolas Artance ’s Valvin.
Urram|4|False|Taing shònraichte: Sgioba Inkscape ’s Mc gu h-àraid.
Urram|5|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|6|False|Bathar-bog: Krita 3.2.1, Inkscape 0.91 air Linux Mint 18.1 XFCE
Urram|7|False|Ceadachas: Creative Commons Attribution 4.0
Urram|9|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
Urram|8|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 816 pàtran a thug taic dhan eapasod seo:
