# Transcript of Pepper&Carrot Episode 24 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 24: De eenheidsboom

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|5|False|BAF!
Cayenne|1|True|Wát wil je hier opzetten?
Cayenne|4|False|Heksen van Chaosah doen dat NIET, begrepen?
Cayenne|6|False|Je zou je beter toeleggen op het leren van je “Spreuken der Destructie”!
Pepper|7|True|Destructie?
Pepper|8|True|In deze tijd van het jaar?
Pepper|9|False|Waarom leren we niet wat spreuken om iets mee te creëren?
Cayenne|10|False|De volgende Driemanenraad komt eraan, en je examens ook. Leer! Leer! LEER !!! EN STOP MET JEZELF AF TE LEIDEN MET DIE DOMME IDEEËN VAN JE !!!
Cayenne|2|False|Een eenheidsboom?!?
Cayenne|3|True|NEE!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Bam!
Pepper|5|True|Een winter zonder eenheidsboom?
Pepper|7|False|Als ik nu eens zelf een boom zou maken met magie?
Pepper|8|True|Hé! Ik zou zelfs voor het eerst eens Hippiah-en Chaosahmagie kunnen combineren!
Pepper|9|True|Een grote, groene boom, met sterren en sterren-stelseltjes erin!
Pepper|10|False|Aww, dat wordt zo mooi!
Pepper|11|True|En het wordt ook
Pepper|12|False|creatief!
Pepper|13|True|Maar ik kan dat soort magie echt niet hier doen …
Pepper|14|False|… mijn peettantes merken het meteen als ik niet bezig ben met mijn “Spreuken der Destructie”.
Carrot|3|False|tik
Geluid|2|False|Ffff
Pepper|6|False|Nooit!
Pepper|4|False|Grrrrr…!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|3|False|Frsj
Geluid|17|False|SjiiING
Geschrift|22|False|De KOMONER
Pepper|1|True|Tenzij …
Pepper|2|False|Hoe zat dat ook alweer …
Pepper|4|False|Ja! Thym raadt hier aan “Spreuken der Destructie” uit te proberen in een microdimensie. Maar kijk naar die waarschuwingen:
Pepper|16|False|Aan het werk!
Komijn|19|False|Heeft Pepper nu net een micro-dimensie gemaakt?
Geluid|18|False|Swoeemp!!
Thym|20|True|Goed!
Geschrift|8|False|“Breng NIETS terug mee naar buiten, uit veiligheidsoverweging.”
Geschrift|6|False|“Maak NIET al je Rea op bij het maken van de microdimensie.”
Geschrift|11|True|“Verwacht GEEN hulp van buiten de microdimensie …
Pepper|7|False|Uiteraard.
Pepper|10|False|Hier staat het!
Pepper|9|False|Pff … Duh!
Pepper|5|False|Mm …
Geschrift|12|False|… niemand kan zien wat er binnenin gebeurt!”
Thym|21|False|Ik hoop dat ze alle waarschuwingen snapt die ik schreef in het hoofdstuk over “Spreuken der Destructie”.
Pepper|15|True|Mooi zo!
Pepper|13|True|Niemand, hé?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Zo cool!
Pepper|2|True|Het is een container met alleen dat deel van de bibliotheek dat ik nodig heb. En volledig gescheiden van de echte wereld.
Pepper|3|False|De ideale, veilige omgeving om spreuken uit te proberen.
Pepper|4|False|Tijd om aan mijn meesterwerk te beginnen!
Geluid|5|False|SjiiING
Geluid|6|False|Bfffrrom!!
Geluid|8|False|Tchkrrr!!
Geluid|10|False|Sjling!!
Pepper|7|False|Haha, nee.
Pepper|9|False|Ieuw!
Pepper|11|False|Ja! Die is mooi, maar wel te klein …
Pepper|13|False|CARROT! NIET AANKOMEN!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Plooem!!
Pepper|3|True|Deze boom uit de microdimensie halen zal wel geen kwaad kunnen.
Pepper|2|True|Zo, ik heb hem wat vergroot. Nu ziet hij er perfect uit!
Pepper|6|True|Tijd om deze microdimensie naar de realiteit te brengen en mijn peettantes te tonen hoe creatief De Chaos kan zijn!
Geluid|8|False|Vwooormp!!
Thym|9|True|Hmm … Pepper is terug.
Thym|10|False|Bravo, Cayenne. Je didactische methoden werpen eindelijk hun vruchten af.
Cayenne|11|False|Dank u, meesteres Thym.
Pepper|7|False|Ik kan niet wachten hun gezicht te zien!
Geschrift|12|False|De KOMONER
Pepper|13|False|Ik bedoel, het is maar een eenheidsboom, geen “Spreuk der Destructie”.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|5|True|BRRrrr
Geluid|6|True|BRRr
Geluid|7|True|BRRrrr
Geluid|8|True|BRRrrr
Geluid|9|True|BRRrrr
Geluid|10|False|BRrr
Carrot|11|False|?
Carrot|13|False|!!
Geluid|14|True|BRRr
Geluid|15|False|BRRrrrr
Carrot|16|False|!!
Pepper|1|True|CARROT!
Pepper|2|True|Ik ga Thym, Cayenne en Cumin halen.
Pepper|3|False|Blijf van die boom af tot ik terug ben, oké?
Pepper|4|False|Ik blijf niet lang weg.
Pepper|17|True|Heksen van Chaosah, jullie zullen versteld staan. Zoiets hebben jullie nog nooit gezien.
Pepper|18|False|Volg mij maar naar boven voor …
Geluid|12|False|Grooesj!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|6|False|- EINDE -
Geluid|1|False|Grooefsj!!
Geluid|4|False|KRAK!!!
Geluid|3|False|Fwiuu!!!
Pepper|2|False|?!!
Pepper|5|False|Zo te zien heb ik een nieuwe “Spreuk der Destructie” ontdekt!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|2|False|12/2017 - www.peppercarrot.com - Tekeningen en verhaal: David Revoy - Vertaling: Midgard, Willem Sonke en Marno van der Maas.
Aftiteling|3|False|Dialoogverbeteringen: Craig Maloney, CalimeroTeknik, Jookia.
Aftiteling|5|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand en Alex Gryson.
Aftiteling|6|False|Software: Krita 3.2.3, Inkscape 0.92.2 op Ubuntu 17.10
Aftiteling|7|False|Licentie: Creative Commons Naamsvermelding 4.0
Aftiteling|9|False|Jij kan ook een patroon van Pepper&Carrot worden op www.patreon.com/davidrevoy
Aftiteling|8|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 810 patronen:
Aftiteling|4|False|Proeflezing/Beta-feedback: Alex Gryzon, Nicolas Artance, Ozdamark, Zveryok, Valvin en xHire.
Aftiteling|1|False|Prettige feestdagen!
