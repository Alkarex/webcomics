# Transcript of Pepper&Carrot Episode 33 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 33: Spell of War

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemy|1|False|huUWOOOOOOOOOOO!
Army|2|False|Rawwwr!
Army|5|False|Grrawwr!
Army|4|False|Grrrr!
Army|3|False|Yuurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|OK, young witch.
King|2|False|If you have a spell that can help us, it's now or never!
Pepper|3|True|Got it!
Pepper|4|False|Prepare to witness...
Sound|5|False|Dzziii !!
Pepper|6|False|...MY MASTERPIECE!
Sound|7|False|Dzziiii !!
Pepper|8|False|Realitas Hackeris Pepperus!
Sound|9|False|Dzziooo !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fiizz!
Sound|2|False|Dzii!
Sound|3|False|Schii!
Sound|4|False|Ffhii!
Sound|8|False|Dziing!
Sound|7|False|Fiizz!
Sound|6|False|Schii!
Sound|5|False|Ffhii!
King|9|True|So, does this spell give our swords more power...
King|10|False|...and weaken the enemy's weapons?
Sound|11|False|Dzii...
Pepper|12|True|Heh.
Pepper|13|True|You'll see!
Pepper|14|False|All I can say is you won't lose any soldiers today.
Pepper|15|False|But you will still have to fight and give it your best!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|That is what we have trained to do.
King|2|False|ON MY COMMAAAAAND!
Army|3|False|Yahhhh!
Army|4|False|Yeaahh!
Army|5|False|Yaaah!
Army|6|False|Yaawww!
Army|7|False|Yeaaah!
Army|8|False|Yaaah!
Army|9|False|Yaeeh!
Army|10|False|Yaeeh!
Army|11|False|Yaahhh!
King|12|False|CHAAAAAAAARGE!!!
Army|13|False|Yeaahh!
Army|14|False|Yahhh!
Army|15|False|Yaeeh!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Yahhhh!!!
Enemy|2|False|YUuurr!!!
Sound|3|False|Schwiiing !!
Sound|4|False|Swoosh !!
Writing|5|False|12
Enemy|6|False|?!!
Writing|7|False|8
King|8|False|?!!
Writing|9|False|64
Writing|10|False|32
Writing|11|False|72
Writing|12|False|0
Writing|13|False|64
Writing|14|False|0
Writing|15|False|56
Army|20|False|Yrrr!
Army|17|False|Yurr!
Army|19|False|Grrr!
Army|21|False|Yaaah!
Army|18|False|Yeaaah!
Army|16|False|Yaawww!
Sound|27|False|Sshing
Sound|25|False|Fffchh
Sound|22|False|wiizz
Sound|23|False|Swoosh
Sound|24|False|Chklong
Sound|26|False|Chkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|?!
Writing|2|False|3
Writing|3|False|24
Writing|4|False|38
Writing|5|False|6
Writing|6|False|12
Writing|7|False|0
Writing|8|False|5
Writing|9|False|0
Writing|10|False|37
Writing|11|False|21
Writing|12|False|62
Writing|13|False|27
Writing|14|False|4
King|15|False|!!
King|16|False|WIIIIIITCH!!!! WHAT HAVE YOU DONE?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa !
Pepper|2|True|“This"
Pepper|3|False|...is my masterpiece!
Pepper|4|False|It's a complex spell that alters reality and shows your opponent's remaining life points.
Writing|5|False|33
Pepper|6|True|When you have no more life points, you leave the field for the rest of the battle.
Pepper|7|True|First army to remove all of the opposing soldiers wins!
Pepper|8|False|Simple.
Writing|9|False|0
Army|10|False|?
Pepper|11|True|Isn't it great?
Pepper|12|True|No more death!
Pepper|13|True|No more wounded soldiers!
Pepper|14|False|This will revolutionize war as we know it!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Now that we know the rules, let me reset the scores so we can start all over again!
Pepper|2|False|It will be more fair.
Writing|3|False|64
Writing|4|False|45
Writing|5|False|6
Writing|6|False|2
Writing|7|False|0
Writing|8|False|0
Writing|9|False|0
Writing|10|False|0
Writing|11|False|9
Writing|12|False|5
Writing|13|False|0
Writing|14|False|0
Writing|15|False|0
Sound|16|False|Sshing!
Sound|17|False|Zooo!
Sound|19|False|tchac!
Sound|18|False|poc!
Pepper|20|True|Run faster, Carrot!
Pepper|21|False|The spell won't last much longer!
Narrator|22|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|4|False|June 29, 2020 Art & scenario: David Revoy. Beta readers: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. English version (original version) Proofreading: Craig Maloney, GunChleoc, Karl Ove Hufthammer, Martin Disch. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.3, Inkscape 1.0 on Kubuntu 19.10. License: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Did you know?
Pepper|6|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|7|False|For this episode, thanks go to 1190 patrons!
Pepper|8|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|9|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|10|True|Check www.peppercarrot.com for more info!
Pepper|11|False|Thank you!
