# Transcript of Pepper&Carrot Episode 33 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 33: Vojni urok

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sovražnik|1|False|tuuuuUUUUUUUT!
Vojska|2|False|Gruull!
Vojska|5|False|Raaaah!
Vojska|4|False|Grrrr!
Vojska|3|False|Juurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|True|V redu, mlada čarovnica.
Kralj|2|False|Če imaš urok, ki nam lahko pomaga, je zdaj pravi čas!
Paprika|3|True|Seveda!
Paprika|4|False|Pripravite se na …
Zvok|5|False|Čiing!!
Paprika|6|False|MOJO MOJSTROVINO!
Zvok|7|False|Čiiing!!
Paprika|8|False|Spopadus nadgradis Paprikus!
Zvok|9|False|Čiioo !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Fiiss!
Zvok|2|False|Čii!
Zvok|3|False|Šii!
Zvok|4|False|Ffhii!
Zvok|8|False|Čing!
Zvok|7|False|Fiiss!
Zvok|6|False|Šii!
Zvok|5|False|Ffhii!
Kralj|9|True|Predvidevam, da ta urok ojača naše meče …
Kralj|10|False|… in oslabi sovražnikove?
Zvok|11|False|Čiing …
Paprika|12|True|He he.
Paprika|13|True|Boste že videli!
Paprika|14|False|Lahko vam le povem, da danes ne boste izgubili nobenega vojaka.
Paprika|15|False|A še vedno se morate boriti in dati vse od sebe!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|False|Za to smo se urili.
Kralj|2|False|NA MOJ ZNAK!
Vojska|3|False|Da gospod!
Vojska|4|False|Jaaaa!
Vojska|5|False|Hoo!
Vojska|6|False|Hoo!
Vojska|7|False|Jaaa!
Vojska|8|False|Hoo!
Vojska|9|False|Da gospod!
Vojska|10|False|Jaa!
Vojska|11|False|Da gospod!
Kralj|12|False|V NAAPAAAAD!!!
Vojska|13|False|Jaaa!
Vojska|14|False|Naprej!
Vojska|15|False|Haa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|False|Aaaaa!!!
Sovražnik|2|False|JUuuur!!!
Zvok|3|False|Šviiing !!
Zvok|4|False|Švisst !!
Napis|5|False|12
Sovražnik|6|False|?!!
Napis|7|False|8
Kralj|8|False|?!!
Napis|9|False|64
Napis|10|False|32
Napis|11|False|72
Napis|12|False|0
Napis|13|False|64
Napis|14|False|0
Napis|15|False|56
Vojska|20|False|Raah!
Vojska|17|False|Jurr!
Vojska|19|False|Grrr!
Vojska|21|False|Jaaa!
Vojska|18|False|Aaah!
Vojska|16|False|Jaaa!
Zvok|27|False|Šing
Zvok|25|False|Ffš
Zvok|22|False|viš
Zvok|23|False|Svuš
Zvok|24|False|Kling
Zvok|26|False|Kling

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|False|?!
Napis|2|False|3
Napis|3|False|24
Napis|4|False|38
Napis|5|False|6
Napis|6|False|12
Napis|7|False|0
Napis|8|False|5
Napis|9|False|0
Napis|10|False|37
Napis|11|False|21
Napis|12|False|62
Napis|13|False|27
Napis|14|False|4
Kralj|15|False|!!
Kralj|16|False|ČAROVNICAAA!!!! KAJ JE ZDAJ TO?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ta-daaa!
Paprika|2|True|TO
Paprika|3|False|je moja mojstrovina!
Paprika|4|False|Zapleten urok, ki spremeni resničnost in prikaže življenjske točke nasprotnika.
Napis|5|False|33
Paprika|6|True|Kdor ostane brez življenjskih točk, mora zapustiti bojno polje do konca bitke.
Paprika|7|True|Prva vojska, ki odstrani vse nasprotnikove vojščake, zmaga!
Paprika|8|False|Preprosto!
Napis|9|False|0
Vojska|10|False|?
Paprika|11|True|A ni to čudovito?
Paprika|12|True|Nič več smrti!
Paprika|13|True|Nič več ranjenih vojakov!
Paprika|14|False|To bo revolucija vojskovanja!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|No, pravila so znana. Ponastavila bom točke, da boste lahko začeli znova!
Paprika|2|False|Da bo čisto pošteno.
Napis|3|False|64
Napis|4|False|45
Napis|5|False|6
Napis|6|False|2
Napis|7|False|0
Napis|8|False|0
Napis|9|False|0
Napis|10|False|0
Napis|11|False|9
Napis|12|False|5
Napis|13|False|0
Napis|14|False|0
Napis|15|False|0
Zvok|16|False|Šving!
Zvok|17|False|Vuš!
Zvok|19|False|Tšak!
Zvok|18|False|Tok!
Paprika|20|True|Hitreje, Korenček!
Paprika|21|False|Urok ne bo več dolgo zdržal!
Pripovedovalec|22|False|- KONEC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|4|False|29. junij 2020 Piše in riše: David Revoy. Testni bralci: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2 in Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 4.3 in Inkscape 1.0 na Kubuntu 19.10. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
Paprika|5|True|Že veš?
Paprika|6|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|7|False|Za to epizodo je zaslužnih 1190 podpornikov!
Paprika|8|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|9|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|10|True|Obišči www.peppercarrot.com za več informacij!
Paprika|11|False|Hvalaaaa!
