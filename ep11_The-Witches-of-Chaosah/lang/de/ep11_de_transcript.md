# Transcript of Pepper&Carrot Episode 11 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 11: Die Hexen von Chaosāh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymian|2|False|Cayenne hat recht, eine echte Hexe von Chaosāh muss Ehrfurcht, Angst und Respekt verbreiten.
Kümmel|3|False|... während du sogar unseren Dämonen Tee und Kuchen servierst*...
Fußnote|4|False|* siehe Episode 8: Peppers Geburtstagsfeier
Pepper|5|True|Aber...
Pepper|6|False|...Tanten...
Cayenne|1|False|Pepper, du bringst Schande über Chaosāh.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|SEI STILL!
Kümmel|9|False|Ta-daaa!
Thymian|1|True|Pepper, du bist sicherlich sehr talentiert, aber du bist auch unsere einzige Nachfahrin.
Thymian|2|False|Es ist unsere Pflicht, aus dir eine echte, böse Hexe von Chaosāh zu machen.
Pepper|3|False|Aber... Ich will nicht böse sein! Es ist... es ist gegen alles was ich...
Cayenne|5|True|...oder wir entziehen dir alle deine Kräfte!
Cayenne|6|False|Und du wirst wieder der kleine Dummkopf von Eichhorns Ruh'.
Thymian|7|False|Kümmel wird dich von jetzt an beobachten, um dir beim Üben zu helfen und uns Bericht zu erstatten.
Geräusch|8|False|Puff!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kümmel|1|False|Der neue König von Acren ist zu freundlich und sanft zu seinen Untergebenen. Die Leute müssen ihren König fürchten.
Kümmel|2|False|Deine erste Aufgabe ist es den König einzuschüchtern, um ihn zu manipulieren.
Kümmel|3|False|Eine echte Hexe von Chaosāh hat Einfluss auf die Mächtigen!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Der König?...
Pepper|2|False|...so jung?!
Pepper|3|False|Wir müssen im selben Alter sein...
Kümmel|4|False|Worauf wartest du?! Komm schon! Weck ihn, schüchtere ihn ein, erschrecke ihn!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Tzzz
Pepper|2|False|Du und ich, wir sind uns so ähnlich...
Pepper|3|True|jung...
Pepper|4|True|alleine...
Pepper|5|False|...Gefangene unseres Schicksals.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymian|6|False|Wir haben sehr viel Arbeit vor uns, Pepper...
Impressum|8|False|09/2015 - Grafik & Handlung: David Revoy - Übersetzung: Philipp Hemmer
Erzähler|7|False|- ENDE -
Geräusch|1|False|Puff!
Pepper|2|False|?!
Geräusch|3|False|Fluff!
Thymian|4|True|Erster Test:
Thymian|5|True|DURCHGEFALLEN

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 502 Förderer:
Impressum|3|False|https://www.patreon.com/davidrevoy
Impressum|2|True|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|4|False|Lizenz: Creative Commons Namensnennung 4.0 Quelldaten: verfügbar auf www.peppercarrot.com Software: Diese Episode wurde zu 100% mit freier Software erstellt Krita 2.9.6, G'MIC 1.6.5.2, Inkscape 0.91 sur Linux Mint 17
