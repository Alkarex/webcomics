# Transcript of Pepper&Carrot Episode 11 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XI : Chaosahis magae

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Caienna|1|False|Piper, Chaosahi ruborem afferes.
Thymus|2|False|Est ut Caienna dicit, si vis veram magam Chaosahis appellari, necesse est populum te timere, tibi oboedire, et te honorare.
Cuminum|3|False|... Et tu, etiam daemonibus nostris crustula ac theam das!...
Piper|4|True|Sed...
Piper|5|False|... matrinae...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Caienna|4|False|TACE !
Cuminum|9|False|Tadaaa !
Thymus|1|True|Piper, etsi instructissima es, sola tamen es ad nobis succedendum.
Thymus|2|False|Opus est nobis te facere veram improbam magam Chaosahis.
Piper|3|False|Sed... ego nolo me improbam esse! Est... est adversius...
Caienna|5|True|... nisi pareas recipiemus omnes potestates tuas !
Caienna|6|False|Et denuo, eris parvula stulta Sciurifiniensisque orba.
Thymus|7|False|Nunc, Cuminum ubique te comitabitur ut te doceat et nobis exploret an rem bene geras.
Sonus|8|False|Puf !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cuminum|1|False|Novus rex Acrinis nimis clemens bonusque cum civibus suis est. Oportet enim civibus Regem suum timere.
Cuminum|2|False|Prima probatio: debes monarchon terrere ut ille tibi oboediat.
Cuminum|3|False|Vera maga Chaosahis auctoritatem apud potentes habere debet!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|True|Rex ?...
Piper|2|False|...adulescentulus ?!
Piper|3|False|Fortasse eodem tempore nati sumus...
Cuminum|4|False|Cur manes!? Age! Eum excita, eum objurga, eum terre!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|2|False|Ego et tu, magis minusve similes sumus...
Piper|3|True|adulescentuli...
Piper|4|True|soli...
Piper|5|False|... a fato captivi.
Sonus|1|False|Dzzz

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymus|6|False|Multum laborandum est cum tibi o Piper...
Credits|8|False|Mense septembre A. MMXV - Designum et fabula : David Revoy, lingua latina translatio: Benjamin Touati & Valentine Guillocheau
Narrator|7|False|- FINIS -
Piper|2|False|?!
Thymus|4|True|Prima probatio :
Thymus|5|False|REPULSA !
Sonus|1|False|Puf !
Sonus|3|False|Fluf !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Piper&Carota omnio libera est, apertum fontis, ac iuvatur maecenatu lectorum. In hoc episodium, gratias ago 502 Maecenatibus :
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|Tu quoque, potes fieri maecenas Piperis&Carotae sequenti episodio :
Credits|4|False|Licentia : Creative Commons Attribution 4.0 Fontes : in promptu sunt apud www.peppercarrot.com Instrumenta : hoc episodium omnino designatum est liberis programmatibus Krita 2.9.6, G'MIC 1.6.5.2, Inkscape 0.91 cum Linux Mint 17
