# Transcript of Pepper&Carrot Episode 17 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 17: Nov začetek

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kumina|1|False|Daj no, Paprika … Ne odhajaj …
Paprika|2|False|NE! ŠLA BOM!
Paprika|3|False|Vaš pouk nima nič s čarovništvom! K aškim čarovnicam grem, da veste!
Zvok|4|False|Vuuuš!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|No, tako. Proti deželi zahajajočih lun.
Paprika|2|False|Šičimi nama lahko pove, kako se pridruživa aškim čarovnicam.
Paprika|3|False|Korenček, poišči zemljevid in kompas. Preveč oblačno je, da bi videla, kam grem.
Paprika|5|True|PIŠUKA!
Paprika|6|True|Vrtinčenje zraka!
Paprika|7|False|DRŽI SE!
Paprika|9|False|O, ne!!!
Zvok|8|False|BrrUuuuUum!!
Napis|4|False|S

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|TrrEsk!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pošast|6|False|Aah!!
Pošast|5|False|Vah!!
Paprika|1|False|Ne boj se, Korenček …
Kajenka|2|True|„Pošten mrk pogled je vreden ducat piškavih coprnij!
Paprika|7|False|Osebno bi se raje naučila kakšen dober napadalni urok. Kakorkoli že …
Paprika|8|True|Zlomka! Nimava več metle in opreme.
Paprika|9|False|Ta pot bo še dolga …
Kajenka|3|True|Glej zviška.
Kajenka|4|False|Nadvladaj!“

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kumina|4|False|„Če poznaš užitne rastline, ne potrebuješ napojev proti lakoti!“
Paprika|6|False|Ko bi me vsaj kakšnega naučila!
Paprika|2|True|Tudi jaz sem na koncu z močmi.
Paprika|3|False|Že nekaj dni nisva ničesar pojedla.
Korenček|1|False|Krul
Paprika|5|True|Ampak pravi napoji?

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timijana|1|False|„Prava kaosaška čarovnica ne potrebuje ne kompasa ne zemljevida. Zvezdnato nebo zadostuje.“
Paprika|3|True|Glavo pokonci, Korenček!
Paprika|4|False|Poglej, prispela sva!
Paprika|2|False|Raje bi imela pravi tečaj vedeževanja!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|7|False|Licenca: Creative Commons Priznanje avtorstva 4.0 - Programska oprema: Krita, G'MIC, Inkscape na Ubuntuju
Zasluge|6|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneya. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Pripovedovalec|4|False|- KONEC -
Zasluge|5|False|Junij 2016 - www.peppercarrot.com - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Paprika|2|False|Konec zgodbe.
Šičimi|3|False|Se pravi si prepotovala vso to pot, da mi poveš, da te niso ničesar naučile?
Paprika|1|True|… in tako sva zdaj tukaj.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 719 bralcev:
Zasluge|2|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
