# Transcript of Pepper&Carrot Episode 17 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 17: Un nuevo comienzo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumino|1|False|Pero Pimienta... ...Vuelve...
Pimienta|2|False|¡NO! ¡¡ME VOY!!
Pimienta|3|False|¡Con vosotras no aprendo auténtica hechicería! ¡Voy a ir a ver a las brujas de Ah!
Sonido|4|False|¡Zooosh!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|Veamos, tenemos que ir al país de las puestas de luna.
Pimienta|2|False|Shichimi nos dirá como podemos entrar en las brujas de Ah.
Pimienta|3|False|Zanahoria, saca el mapa y la brújula: hay demasiadas nubes y no sé hacia dónde vamos.
Pimienta|5|True|¡UPS!
Pimienta|6|True|¡Turbulencias!
Pimienta|7|False|¡AGÁRRATE BIEN!
Pimienta|9|False|¡¡¡Oh, no!!!
Sonido|8|False|¡¡BrrOoooOoo!!
Escritura|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡¡ CrrAsh !!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruo|6|False|¡¡ Kaii !!
Monstruo|5|False|¡¡ Kaii !!
Pimienta|1|False|Sobre todo no muestres miedo, Zanahoria...
Cayena|2|True|"Una mirada fija, penetrante y sombría puede evitar hechizos inútiles...
Pimienta|7|False|Yo habría preferido aprender algunos buenos hechizos de ataque; pero bueno...
Pimienta|8|True|Buf... ya no tengo escoba ni nada.
Pimienta|9|False|Va a ser un viaje largo...
Cayena|3|False|...¡Rebájalos! ¡Domínalos!"

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumino|4|False|"Conocer las plantas comestibles es conocer pociones ya preparadas contra el hambre."
Pimienta|6|False|Si al menos me hubiera enseñado una sola...
Pimienta|2|True|Yo también estoy a punto de desmayarme...
Pimienta|3|False|Y ya hace días que no hemos comido nada.
Zanahoria|1|False|Groo
Pimienta|5|True|¿Pero verdaderas pociones?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tomillo|1|False|"Una verdadera bruja de Chaosah no necesita ni mapas ni brújula. Le basta con un cielo estrellado."
Pimienta|3|True|¡Ánimo, Zanahoria!
Pimienta|4|False|¡Mira, hemos llegado!
Pimienta|2|False|... ¡yo habría preferido un verdadero curso de adivinación!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|7|False|Licencia: Creative Commons Attribution 4.0, Software: Krita, G'MIC, Inkscape en Ubuntu
Créditos|6|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Narrador|4|False|- FIN -
Créditos|5|False|Junio 2016 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Pimienta|2|False|Ahora conoces toda la historia.
Shichimi|3|False|...¿y dices que estás aquí porque ellas no te han enseñado nada?
Pimienta|1|True|...y aquí estamos.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto, y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 719 mecenas:
Créditos|2|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
