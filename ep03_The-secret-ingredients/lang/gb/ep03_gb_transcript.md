# Transcript of Pepper&Carrot Episode 03 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 3: Sirili Canfen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|...O, malxanse.
Pilpil|2|False|Xaher Komona, Din fe bazar.
Vendor|3|False|Bonsoba, Senyor, mi ger vole oco estareli kabaca, fe lutuf.
Pilpil|5|False|Prehay, kima sen 60 ko*.
Note|4|False|* Ko = pesali unxey de Komona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|Em, mafu, mi abil na cudu sol care...
Vendor|2|False|Grrr...
Safran|3|False|Salom, Gao Senyor. Fe lutuf, am jumbigi dua desduaxey fe moyto tas mi. Fe suprem kwalita, kom fe xugwan.
Vendor|4|False|To moywatu sen furaha, na servi yu, Juniyen Safran.
Safran|5|False|O, Pilpil presen.
Safran|6|False|O, bisnes xunjan in nongyogeo, kam no?
Pilpil|7|False|...
Safran|8|False|Mi jadin ki yu jumbigi canfen cel konkurexey fe iksir de jaxadin, kam?
Pilpil|9|True|...konkurexey?
Pilpil|10|False|...jaxadin?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|4|False|Fe bonxanse, mi haji hare un total din cel na jumbicu!
Pilpil|5|False|Imi am triunfa hin konkurexey!
Eskrixey|1|False|Konkurexey fe Iksir de Komona
Eskrixey|2|False|cuyo jayeza: 50 000 ko POR MAXIM BON IKSIR
Eskrixey|3|False|Fe Azardin, 3 de Pinku Mesi Cuyo Gwancan de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|2|False|O...! Mi jixi ...
Pilpil|3|False|...to preciso sen dento hu mi haja da!
Pilpil|4|True|Karote!
Pilpil|5|False|Am jumbicu, imi xa tongo toncudu canfen!
Pilpil|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|Unyum, mi haja bannumer cinju fe ukungu of den syahe megu...
Pilpil|2|False|...ji bannumer roso beri of xabahuli jungla.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|...ji pia ovohole of volkanoli wadi Fenix...
Pilpil|2|False|...ji fe fini, katru fe milko of juni DragonMumu.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|Dento sen moyto, Karote, mi fikir ki mi hare moyto hu mi haja da.
Pilpil|2|True|To okocu...
Pilpil|3|False|...perfeto!
Pilpil|4|True|Mmm...
Pilpil|5|True|Maxim... bon... kafe... he banwatu!
Pilpil|6|False|To sen moyto hu mi haja da cel na ergo total noce ji na fale maxim bon iksir cel konkurexey de jaxadin.
Narrator|7|False|To xa dure...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Credits|2|False|This episode couldn't exist without the support of my 93 patrons
Credits|1|False|This webcomic is totally free and open-source ( Creative Commons Attribution 3.0 Unported, hi-res source files available to download )
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|6|False|Special thanks to: David Tschumperlé (G'MIC) and all Krita team! Translation Globasa: Hector Ortega
Credits|7|False|This episode was made with 100% free(libre) and open-source tools Krita and G'MIC on Xubuntu (GNU/Linux)
Pilpil|5|False|Thank you!
