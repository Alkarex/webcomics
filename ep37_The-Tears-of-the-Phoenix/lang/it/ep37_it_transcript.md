# Transcript of Pepper&Carrot Episode 37 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Episodio 37: Le Lacrime della Fenice

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah...
Pepper|2|False|Che bello essere qui!
Pepper|3|False|Ehi, è il giorno del mercato.
Pepper|4|False|Dovremmo mangiare un boccone prima di salire sul vulcano, no?
Pepper|5|False|Ero sicura che l’idea ti sarebbe piaciuta!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Testo scritto|1|False|WANTED
Testo scritto|2|False|Torreya
Testo scritto|3|False|100.000 Ko
Testo scritto|4|False|Shichimi
Testo scritto|5|False|250.000 Ko
Testo scritto|6|False|Pepper
Testo scritto|7|False|1.000.000 Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Li hanno messi davvero ovunque…
Pepper|2|False|Anche negli angoli più remoti.
Pepper|3|False|Sbrighiamoci, andiamo avanti prima di destare sospetti.
Pepper|4|False|Abbiamo problemi ben più importanti in questo momento…
Pepper|5|False|Quella luce…
Pepper|6|False|Dovremmo essere vicino al suo nido.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fenice|1|False|Perché vieni ad importunarmi, umana?
Pepper|2|False|Ossequi, o grande Fenice!
Pepper|3|False|Mi chiamo Pepper e sono una strega di Chaosah.
Pepper|4|False|Ho ricevuto di recente un’enorme dose di Rea di drago. Da quel momento è comparso questo e si espande di giorno in giorno…
Pepper|5|False|Neutralizza tutti i miei poteri e potrebbe uccidermi.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fenice|1|True|Mmh...
Fenice|2|False|Vedo...
Fenice|3|False|…e ora sei qui davanti a me, perché vuoi delle lacrime di Fenice per poter guarire, giusto?
Pepper|4|False|Sì, è la mia unica possibilità.
Fenice|5|False|*sigh*
Fenice|6|False|…beh cosa aspetti?
Fenice|7|False|Prova a farmi piangere.
Fenice|8|False|Ti do un minuto!
Pepper|9|False|Cosa?!
Pepper|10|False|Farvi piangere?!
Pepper|11|False|Ma non sapevo che…
Pepper|12|False|E poi, solo un minuto?!
Pepper|13|True|Uh...
Pepper|14|True|OK!
Pepper|15|False|Vediamo.
Pepper|16|True|Mmh… Pensate alla fame nel mondo.
Pepper|17|True|Ehi, no, no, no!
Pepper|18|True|Ho qualcosa di meglio:
Pepper|19|False|Pensate ai cari che sono scomparsi.
Pepper|20|True|Ancora niente?
Pepper|21|True|E gli animali abbandonati?!
Pepper|22|False|È troppo triste, l'abbandono degli animali…
Fenice|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Allora?
Pepper|2|False|Non vi ha fatto piangere?
Fenice|3|False|MA SEI SERIA?!
Fenice|4|False|È TUTTO QUELLO CHE HAI?!
Fenice|5|True|Loro, almeno, hanno provato ad usare la poesia!
Fenice|6|True|La scrittura di tragedie!
Fenice|7|True|L’ARTE!
Fenice|8|False|IL TEATRO!
Fenice|9|True|E TU?!
Fenice|10|False|TU VIENI IMPREPARATA!
Fenice|11|True|SÌ, È COSÌ; SCIÒ!
Fenice|12|False|VATTENE E TORNA QUANDO AVRAI TROVATO DI MEGLIO!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Dai, Pepper! Trova una storia triste.
Pepper|2|True|Puoi farcela.
Pepper|3|False|Puoi farcela.
Pepper|4|True|No...
Pepper|5|False|Non puoi.
Venditore|6|False|Ehi! Oh! Vai via!
Pepper|7|False|!!
Venditore|8|False|Via le zampe dalla mia merce se non hai di che pagare, eh!
Pepper|9|True|Oh no…
Pepper|10|True|CARROT!
Pepper|11|False|Potresti aiutarmi invece di pensare al tuo stomaco!
Pepper|12|False|Oh?!
Pepper|13|True|Ma sì…
Pepper|14|False|Potrebbe funzionare.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fenice|1|True|Ah! Eccoti di nuovo qui.
Fenice|2|False|Così presto…
Fenice|3|False|Un’arma di metallo?!
Fenice|4|False|Seriamente?!
Fenice|5|False|Non sai che posso fondere ogni metallo e…
Suono|6|False|Plop
Suono|7|False|Plop
Suono|8|False|Plop
Fenice|9|True|OH NO!
Fenice|10|False|NON QUELLE!
Fenice|11|False|NON È VALIDO!
Pepper|12|True|Veloce Carrot!
Pepper|13|False|Raccogli più lacrime possibile!
Suono|14|False|Zac!
Suono|15|False|Zac!
Suono|16|False|Zac!
Titolo|17|False|- FINE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crediti|1|False|3 Agosto 2022 Disegni & sceneggiatura: David Revoy. Lettori della versione beta: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Traduzione in italiano: Matteo Bertini. Correzioni: Elisa Moretti. Basato sull'universo di Hereva Creato da: David Revoy. Amministratore: Craig Maloney. Autori: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correzioni: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Lo sapevi?
Pepper|3|False|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle donazioni dei lettori.
Pepper|4|False|Per questo episodio, un grazie va ai 1058 donatori!
Pepper|5|False|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|6|False|Siamo su Patreon, Tipeee, PayPal, Liberapay ...ed altri!
Pepper|7|False|Leggi su www.peppercarrot.com per maggiori informazioni!
Pepper|8|False|Grazie mille!
