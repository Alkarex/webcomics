# Transcript of Pepper&Carrot Episode 37 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episódio 37: As Lágrimas do Fênix

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ufa...
Pepper|2|False|É bom estar aqui!
Pepper|3|False|Ah, é dia de feira.
Pepper|4|False|Vamos pegar algo para comer antes de escalar o vulcão.
Pepper|5|False|Eu sabia que você ia gostar dessa ideia!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escrita|1|False|PROCURA-SE
Escrita|2|False|Torreya
Escrita|3|False|100 000Ko
Escrita|4|False|Shichimi
Escrita|5|False|250 000Ko
Escrita|6|False|Pepper
Escrita|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Parece que eles espalharam esses cartazes por todos os lados.
Pepper|2|False|Até mesmo nos cantos mais remotos.
Pepper|3|False|Venha, vamos sair daqui antes que peguem a gente.
Pepper|4|False|Nós temos problemas mais importantes agora...
Pepper|5|False|Essa luz...
Pepper|6|False|A gente deve estar perto do ninho dele.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fênix|1|False|Por que me perturbas, humana?
Pepper|2|False|Saudações, Ó grande Fênix!
Pepper|3|False|Meu nome é Pepper e eu sou uma bruxa de Chaosah.
Pepper|4|False|Recentemente eu recebi uma grande dose de Rea de Dragão e, desde então, esta coisa apareceu e segue crescendo a cada dia...
Pepper|5|False|Ela neutralizou todos os meus poderes e creio até que possa ser fatal.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fênix|1|True|Hmm...
Fênix|2|False|Entendo...
Fênix|3|False|... e você está aqui agora porque você quer lágrimas de Fênix para se curar, certo?
Pepper|4|False|Certo. É a minha única opção.
Fênix|5|False|*suspiro*
Fênix|6|False|...bem, o que você está esperando?
Fênix|7|False|Tente me fazer chorar.
Fênix|8|False|Eu te darei um minuto!
Pepper|9|False|O quê?!
Pepper|10|False|Fazer você chorar?!
Pepper|11|False|Mas eu não sabia disso...
Pepper|12|False|Quer dizer... Só um minuto?!
Pepper|13|True|Uhm...
Pepper|14|True|OK!
Pepper|15|False|Deixa eu ver.
Pepper|16|True|Hmm... Pense sobre a fome mundial.
Pepper|17|True|Um, peraí peraí peraí!
Pepper|18|True|Eu tenho algo melhor.
Pepper|19|False|Seus entes queridos que se foram.
Pepper|20|True|Nada ainda?
Pepper|21|True|Animais de estimação abandonados?!
Pepper|22|False|Mas abandono de animais de estimação é tão triste...
Fênix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Então, que tal?
Pepper|2|False|Isso não te faz chorar?
Fênix|3|False|ISSO É SÉRIO?!
Fênix|4|False|É O MELHOR QUE VOCÊ PODE FAZER?!
Fênix|5|True|Eles ao menos tentaram poesia!
Fênix|6|True|Escrever tragédias!
Fênix|7|True|ARTE!
Fênix|8|False|DRAMA!
Fênix|9|True|MAS VOCÊ?!
Fênix|10|False|VOCÊ VEM DESPREPARADA!
Fênix|11|True|É ISSO MESMO, CHISPA!
Fênix|12|False|VÁ PARA CASA E VOLTE SOMENTE QUANDO ESTIVER PRONTA!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Vamos, Pepper! Preciso de uma história triste.
Pepper|2|True|Você consegue.
Pepper|3|False|Você consegue.
Pepper|4|True|Não...
Pepper|5|False|Você não consegue.
Vendedor|6|False|Ei! Opa! Sai pra lá!
Pepper|7|False|!!
Vendedor|8|False|Se você não pode pagar, tire a suas patas das minhas coisas!
Pepper|9|True|Ah não, você não...
Pepper|10|True|CARROT!
Pepper|11|False|Você bem que podia me ajudar ao invés de pensar no seu estômago!
Pepper|12|False|Ah?!
Pepper|13|True|É claro...
Pepper|14|False|Isso pode funcionar.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fênix|1|True|Ah! Você voltou.
Fênix|2|False|Tão cedo...
Fênix|3|False|Uma arma de metal?!
Fênix|4|False|É sério?!
Fênix|5|False|Você sabe que eu posso derreter qualquer metal e...
Som|6|False|Plomp
Som|7|False|Plomp
Som|8|False|Plomp
Fênix|9|True|AH NÃO!
Fênix|10|False|ISSO NÃO!
Fênix|11|False|NÃO É JUSTO!
Pepper|12|True|Rápido Carrot!
Pepper|13|False|Pegue o máximo de lágrimas que você conseguir!
Som|14|False|Chop!
Som|15|False|Chop!
Som|16|False|Chop!
Título|17|False|- FIM -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|3 de agosto de 2022 Arte e roteiro: David Revoy. Leitores beta: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Tradução para Português: Alexandre E. Almeida, Estefania de Vasconcellos Guimaraes. Baseado no universo de Hereva Criador: David Revoy. Mantenedor principal: Craig Maloney. Escritores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Corretores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 no Fedora 36 KDE Spin. Licença: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Você sabia?
Pepper|3|False|Pepper&Carrot é totalmente livre, com código aberto e patrocinada graças a gentil contribuição dos seus leitores.
Pepper|4|False|Para este episódio, agradecemos a 1058 patronos!
Pepper|5|False|Você também pode se tornar um(a) patrono(a) de Pepper&Carrot e ter o seu nome aqui!
Pepper|6|False|Nós estamos no Patreon, Tipeee, PayPal, Liberapay... e mais!
Pepper|7|False|Acesse www.peppercarrot.com para mais informações!
Pepper|8|False|Obrigada!
