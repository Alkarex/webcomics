# Transcript of Pepper&Carrot Episode 37 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 37: Feniksove solze

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|No, tako.
Paprika|2|False|Končno sem prispela!
Paprika|3|False|O, tržni dan je.
Paprika|4|False|Poiščiva si kaj za pod zob, preden se povzpneva na vulkan.
Paprika|5|False|Sem vedela, da se boš strinjal!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|1|False|IŠČEJO SE
Napis|2|False|Toreja
Napis|3|False|100 000 ko.
Napis|4|False|Šičimi
Napis|5|False|250 000 ko.
Napis|6|False|Paprika
Napis|7|False|1 000 000 ko.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Vsepovsod so razobesili te plakate.
Paprika|2|False|Celo v najbolj oddaljene kotičke Hereve.
Paprika|3|False|Pridi, popihajva jo, preden naju prepoznajo.
Paprika|4|False|Že tako imava večje težave …
Paprika|5|False|Ta svetloba …
Paprika|6|False|Gotovo sva blizu gnezda.
Paprika|7|False|Terna!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feniks|1|False|Čemu me nadleguješ, človek?
Paprika|2|False|Pozdravljen, veliki feniks!
Paprika|3|False|Ime mi je Paprika in sem kaosaška čarovnica.
Paprika|4|False|Pred kratkim sem prejela veliko količino zmajevega rêsa. Od takrat imam to znamenje, ki se širi iz dneva v dan …
Paprika|5|False|Onesposobilo je moje moči in čez dalj časa me bo ubilo.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feniks|1|True|Hmm…
Feniks|2|False|Razumem.
Feniks|3|False|In zdaj iščeš feniksove solze, ki te bodo ozdravile, mar ne?
Paprika|4|False|Da, nimam druge izbire.
Feniks|5|False|*vzdih*
Feniks|6|False|No? Kaj se obiraš?
Feniks|7|False|Spravi me v jok.
Feniks|8|False|Na voljo imaš minuto!
Paprika|9|False|Kaj?!
Paprika|10|False|Spravim naj te v jok?!
Paprika|11|False|Ampak nisem vedela, da …
Paprika|12|False|In samo eno minuto imam?!
Paprika|13|True|Em…
Paprika|14|True|Prav!
Paprika|15|False|Pa poglejmo.
Paprika|16|True|Hmm… Predstavljaj si svetovno lakoto.
Paprika|17|True|Aaa, čakajčakajčakaj!
Paprika|18|True|Že vem, še bolje:
Paprika|19|False|Pomisli na bližnje, ki so te za vedno zapustili!
Paprika|20|True|Še vedno nič?
Paprika|21|True|Zapuščeni ljubljenčki?!
Paprika|22|False|Veš, kako so žalostni, ko jih zapustijo …
Feniks|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|No, kaj praviš?
Paprika|2|False|Imaš že solzne oči?
Feniks|3|False|SE DELAŠ NORCA IZ MENE?!
Feniks|4|False|JE BILO TO RES VSE?!
Feniks|5|True|Tile tu so se vsaj potrudili s poezijo!
Feniks|6|True|Pisali so žaloigre!
Feniks|7|True|UMETNOST!
Feniks|8|False|DRAMO!
Feniks|9|True|AMPAK TI?!
Feniks|10|False|TI PA SI PRIŠLA NEPRIPRAVLJENA!
Feniks|11|True|DA, TAKO JE PRAV! ŠC, ŠC!
Feniks|12|False|DOMOV POJDI IN SE VRNI, KO BOŠ MALO BOLJ PRIPRAVLJENA!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Grr! Dajmo, Paprika! Spomni se česa otožnega.
Paprika|2|True|Saj zmoreš.
Paprika|3|False|Saj zmoreš!
Paprika|4|True|Ne …
Paprika|5|False|Ne zmoreš.
Prodajalec|6|False|Oj! Ne! Proč!
Paprika|7|False|!!
Prodajalec|8|False|Tace stran od moje robe, če je ne misliš plačati!
Paprika|9|True|Pa ne zdaj še ti …
Paprika|10|True|KORENČEK!
Paprika|11|False|Lahko bi mi pomagal, namesto da se ukvarjaš s svojim želodcem!
Paprika|12|False|O?!
Paprika|13|True|Ooo, pač …
Paprika|14|False|To bi lahko vžgalo.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feniks|1|True|A! Si že nazaj.
Feniks|2|False|Nekam hitro …
Feniks|3|False|Kovinsko orožje?!
Feniks|4|False|Si resna?!
Feniks|5|False|Mar ne veš, da lahko stalim čisto vsako kovino …
Zvok|6|False|Plop
Zvok|7|False|Plop
Zvok|8|False|Plop
Feniks|9|True|O, NE!
Feniks|10|False|SAMO TEGA NE!
Feniks|11|False|TO NI POŠTENO!
Paprika|12|True|Hitro, Korenček!
Paprika|13|False|Naberi čim več solz!
Zvok|14|False|Sek!
Zvok|15|False|Sek!
Zvok|16|False|Sek!
Naslov|17|False|- KONEC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|3. avgust 2022 Piše in riše: David Revoy. Testni bralci: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron in Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nicolas Artance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 5.0.5 in Inkscape 1.2 na Fedora 36 KDE Spin. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
Paprika|2|False|Že veš?
Paprika|3|False|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 1058 podpornikov!
Paprika|5|False|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|6|False|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|7|False|Obišči www.peppercarrot.com za več informacij!
Paprika|8|False|Hvalaaaa!
