# Transcript of Pepper&Carrot Episode 37 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 37: The Tears of the Phoenix

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Phew...
Pepper|2|False|It's good to be here!
Pepper|3|False|Oh, it's market day.
Pepper|4|False|Let's grab a bite to eat before we climb the volcano.
Pepper|5|False|I knew you'd like that idea!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|WANTED
Writing|2|False|Torreya
Writing|3|False|100 000Ko
Writing|4|False|Shichimi
Writing|5|False|250 000Ko
Writing|6|False|Pepper
Writing|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|They've really posted this everywhere.
Pepper|2|False|Even in the remotest corners.
Pepper|3|False|Come on, let's get out of here before we're recognized.
Pepper|4|False|We have bigger problems right now...
Pepper|5|False|That light...
Pepper|6|False|We must be close to its nest.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|False|Why are you bothering me, human?
Pepper|2|False|Greetings, O great Phoenix!
Pepper|3|False|My name is Pepper, and I am a witch of Chaosah.
Pepper|4|False|I've recently received a huge dose of Dragon's Rea and since then, this has shown up and keeps growing every day...
Pepper|5|False|It neutralizes all my powers and may eventually kill me.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Hmm...
Phoenix|2|False|I see...
Phoenix|3|False|...and now you're here because you want a Phoenix's tears to heal, right?
Pepper|4|False|Yes, it's my only option.
Phoenix|5|False|*sigh*
Phoenix|6|False|...well, what are you waiting for?
Phoenix|7|False|Try to make me cry.
Phoenix|8|False|I'll give you a minute!
Pepper|9|False|What?!
Pepper|10|False|Make you cry?!
Pepper|11|False|But I didn't know that...
Pepper|12|False|I mean... Only one minute?!
Pepper|13|True|Uh...
Pepper|14|True|OK!
Pepper|15|False|Let's see.
Pepper|16|True|Hmm... Think about world hunger.
Pepper|17|True|Er, waitwaitwait!
Pepper|18|True|I've got something better:
Pepper|19|False|Those you've lost.
Pepper|20|True|Still nothing?
Pepper|21|True|Abandoned pets?!
Pepper|22|False|Pet abandonment is so sad...
Phoenix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|So, how about it?
Pepper|2|False|Doesn't it make you cry?
Phoenix|3|False|ARE YOU SERIOUS?!
Phoenix|4|False|IS THAT ALL YOU HAVE?!
Phoenix|5|True|They at least tried poetry!
Phoenix|6|True|Writing tragedies!
Phoenix|7|True|ART!
Phoenix|8|False|DRAMA!
Phoenix|9|True|BUT YOU?!
Phoenix|10|False|YOU COME UNPREPARED!
Phoenix|11|True|YES, THAT'S IT; SHOO!
Phoenix|12|False|GO HOME AND COME BACK WHEN YOU'RE READY!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Come on, Pepper! Find a sad story.
Pepper|2|True|You can do it.
Pepper|3|False|You can do it.
Pepper|4|True|No...
Pepper|5|False|You can't.
Vendor|6|False|Hey! Oh! Get away!
Pepper|7|False|!!
Vendor|8|False|Keep your paws off my stuff if you can't pay!
Pepper|9|True|Oh no, you don't...
Pepper|10|True|CARROT!
Pepper|11|False|You could help me instead of thinking about your stomach!
Pepper|12|False|Oh?!
Pepper|13|True|Oh, yeah...
Pepper|14|False|This could work.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Ah! You're back.
Phoenix|2|False|So soon...
Phoenix|3|False|A metal weapon?!
Phoenix|4|False|Seriously?!
Phoenix|5|False|Don't you know that I can melt any metal and...
Sound|6|False|Plop
Sound|7|False|Plop
Sound|8|False|Plop
Phoenix|9|True|OH NO!
Phoenix|10|False|NOT THAT!
Phoenix|11|False|THAT'S NOT FAIR!
Pepper|12|True|Quick Carrot!
Pepper|13|False|Catch as many tears as you can!
Sound|14|False|Chop!
Sound|15|False|Chop!
Sound|16|False|Chop!
Title|17|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|August 03, 2022 Art & scenario: David Revoy. Beta readers: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. English translation: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Estefania de Vasconcellos Guimaraes. Proofreading: Craig Maloney. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. License: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Did you know?
Pepper|3|False|Pepper&Carrot is entirely free (libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 1058 patrons!
Pepper|5|False|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|6|False|We are on Patreon, Tipeee, PayPal, Liberapay ... and more!
Pepper|7|False|Check www.peppercarrot.com for more info!
Pepper|8|False|Thank you!
