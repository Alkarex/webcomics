# Transcript of Pepper&Carrot Episode 34 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Episodio 34: La cerimonia di Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Quella stessa notte...
Hibiscus|2|False|...ed ora, in questa notte, ti diamo il benvenuto, Shichimi, come la più giovane tra i Cavalieri di Ah.
Coriandolo|3|False|Ancora nessuna traccia di Pepper?
Zafferano|4|True|No, non ancora.
Zafferano|5|False|Se non arriva in tempo si perderà il discorso di Shichimi
Shichimi|6|True|Grazie mille.
Shichimi|7|False|Io vorrei...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|1|False|FiuuMMMM
Suono|2|False|FiuuMMMMM
Pepper|3|True|Sto arrivando!
Pepper|4|True|Largooo!
Pepper|5|False|LAAAARGOOOO!!!
Suono|6|False|CRASH!
Pepper|7|False|Oooops!
Pepper|8|False|Tutti OK? Niente di rotto?
Shichimi|9|False|Pepper!
Pepper|10|True|Ciao Shichimi!
Pepper|11|True|Scusa per l'entrata teatrale ed il ritardo!
Pepper|12|False|Ho corso tutto il giorno, ma è una lunga storia.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Lei è una delle tue invitate?
Shichimi|2|False|Si, è la mia amica Pepper. È tutto a posto.
Pepper|3|True|Carrot, tutto OK?
Pepper|4|False|Scusa perl'atter-raggio, non ho ancora il perfetto controllo dell'iper-velocità.
Pepper|5|True|E mi scuso ancora con tutti per il disturbo,
Pepper|6|False|e di come sono vestita...
Shichimi|7|False|hiii hiii
Wasabi|8|True|Shichimi,
Wasabi|9|False|questa giovane strega che è appena arrivata, è davvero una tua amica?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Si, vostra Altezza.
Shichimi|2|False|Il suo nome è Pepper, della Scuola di Chaosah.
Wasabi|3|True|La sua presenza qui inquina la natura sacra della nostra scuola.
Wasabi|4|False|Allontanala dalla mia vista. Subito!!
Shichimi|5|True|Ma...
Shichimi|6|False|Maestra Wasabi...
Wasabi|7|True|Ma... cosa?
Wasabi|8|False|Preferisci essere espulsa dalla nostra scuola?
Shichimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Scusa Pepper, ma devi andartene.
Shichimi|2|False|Adesso.
Pepper|3|True|Hoh?
Pepper|4|False|Hei hei hei, aspetta un attimo! Sono sicura che qui ci sia un equivoco.
Shichimi|5|False|Per favore Pepper non mettermi in difficoltà.
Pepper|6|False|Hei tu! Lì sul trono, se hai dei problemi con me, vieni qui e dimmelo in faccia!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, hai dieci secondi...
Wasabi|9|True|nove...
Wasabi|10|True|otto...
Wasabi|11|False|sette...
Shichimi|12|False|BASTA, PEPPER! VATTENE VIA!!!
Suono|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|9|True|PAF!
Pepper|1|False|Shichimi, ti prego calmat...
Suono|2|False|BADuuM!
Shichimi|3|True|VAI... VIA!!!
Shichimi|4|True|VAI VIA!!!
Shichimi|5|False|VAI VIA!!!
Suono|6|False|CREEEEE!!!
Pepper|7|True|Haia!
Pepper|8|False|Hei! Questo... n-NON... Ouch... è carino!
Suono|10|True|POF!
Suono|11|True|POF!
Suono|12|False|PAF!
Coriandolo|13|False|SHICHIMI! PEPPER! FERMATEVI!
Zafferano|14|False|Aspetta.
Wasabi|15|False|Hmm!
Pepper|16|True|Grrr!
Pepper|17|False|OK, ve la siete cercata!
Suono|18|False|BRZOO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Suono|2|False|SHKLAK!
Pepper|3|False|Ouch!
Suono|4|False|PAF!!
Shichimi|5|False|Anche il tuo miglior incantesimo di cancellazione non ha alcun effetto su di me!
Shichimi|6|True|Rinuncia, Pepper, e vattene via!
Shichimi|7|False|Non obbligarmi a farti ancora più male!
Pepper|8|False|Oh, il mio incantesimo ha funzionato benissimo, solo non eri tu il mio obiettivo.
Shichimi|9|True|Ah?
Shichimi|10|False|Cosa vuoi dire?!
Suono|11|True|Pshh...
Suono|12|False|Pshh...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Lei... era il mio obiettivo!
Pepper|3|False|Ho appena cancellato il suo incantesimo Glamour che la fa sembrare giovane.
Pepper|4|True|Mi sono accorta dell'incantesimo non appena sono arrivata.
Pepper|5|False|Ecco, vi ho dato una piccola dimostrazione di quello che vi meritate per aver obbligato Shichimi a combattere contro di me!
Wasabi|6|True|INSOLENTE!
Wasabi|7|True|Come osi,
Wasabi|8|False|di fronte a tutta la mia scuola!
Pepper|9|True|Considerati fortunata!
Pepper|10|False|Se avessi avuto tutta la mia Rea, non mi sarei fermata qui.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|1|False|DRZOW!!
Wasabi|2|True|Vedo, hai raggiunto il livello di chi ti aveva preceduto molto prima di quanto avessi previsto...
Wasabi|3|False|Questo accelera i miei piani, ma è una buona notizia.
Pepper|4|True|I tuoi piani?
Pepper|5|True|Quindi mi stavi solo mettendo alla prova e questo non aveva niente a che fare con il mio ritardo?
Pepper|6|False|Tu sei davvero fuori!
Wasabi|7|True|sii...
Wasabi|8|False|sii.
Wasabi|9|True|COSA STATE LÌ IMPALATI?!!
Wasabi|10|False|MI HANNO APPENA ATTACCATA E VOI STATE LÌ COME DEI BACCALÀ?!! PRENDETELA!!!
Wasabi|11|False|LA VOGLIO VIVA!
Wasabi|12|False|PRENDETELA!!!
Pepper|13|False|Shichimi, ne dovremo parlare più tardi!
Pepper|14|True|Scusami Carrot, ma dobbiamo decollare ancora ad ipervelocità.
Pepper|15|False|Tieniti forte!
Suono|16|False|Tap!
Suono|17|False|Tap!
Suono|18|False|FiiuuMM!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|?!
Wasabi|2|False|PRENDETELA!!!
Pepper|3|False|Oh cielo!
Zafferano|4|False|Pepper, prendi la mia scopa!
Suono|5|False|Fizzz!
Pepper|6|True|Oh wow!
Pepper|7|False|Grazie mille Zafferano!
Suono|8|False|Toc!
Suono|9|False|fiuuuUMMM!
Narrator|10|False|CONTINUA...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crediti|1|False|Marzo 31, 2021 Disegni e sceneggiatura: David Revoy. Co-autori del testo: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versione in italiano Traduzione: Carlo Gandolfi. Correzioni: Antonio Parisi. Un ringraziamento speciale a Nartance per aver esplorarato il personaggio di Wasabi nei suoi racconti di fan-fiction. Il modo in cui l'ha immaginata ha avuto un grande impatto su come l'ho rappresentata in questo episodio. Basato sull'universo di Hereva Creato da: David Revoy. Amministratore: Craig Maloney. Autori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correzioni: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 su Kubuntu Linux 20.04. Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Lo sapevi?
Pepper|3|True|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle gentili donazioni dei lettori.
Pepper|4|False|Per questo episodio, grazie ai 1096 donatori!
Pepper|5|True|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|6|True|Siamo su Patreon, Tipeee, Paypal, Liberapay …ed altri!
Pepper|7|False|Leggi www.peppercarrot.com per ulteriori informazioni!
Pepper|8|False|Grazie mille!
