# Episode 34: The Knighting of Shichimi

![cover of episode 34](https://www.peppercarrot.com/0_sources/ep34_The-Knighting-of-Shichimi/low-res/Pepper-and-Carrot_by-David-Revoy_E34.jpg)

## Comments from the author

I wanted Shichimi's strong loyalty to her magic school to be felt more than explained. Shichimi has hard time to understand why someone young as Pepper can discuss the authority of her legendary master... Sorry if the tone was a bit too worrisome. This episode is more an exception than the future norm in terms of tone.

I was convinced the cruelty of Wasabi was enough expressed to avoid anyone of the audience to sympathise with her.

From [Author's blog of episode 34](https://www.davidrevoy.com/article830/episode-34-the-knighting-of-shichimi/show#comments)
