# Transcript of Pepper&Carrot Episode 23 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 23: Grijp je kans

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|Markt van Komona, één week later
Geschrift|2|False|Beroemdheid
Geschrift|3|False|50 000 Ko
Geschrift|4|False|De overwinning van Saffraan
Geschrift|5|False|Mode
Geschrift|6|False|Het fenomeen Saffraan
Geschrift|7|False|Chic
Geschrift|8|False|Special over Saffraan
Geluid|9|False|BAF!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Dank je, Carrot …
Pepper|2|False|Weet je, ik werk hard om een goede heks te zijn …
Pepper|3|True|…met respect voor traditie, voor de regels …
Pepper|4|False|…het is niet eerlijk dat Saffraan zo populair wordt met zo'n soort praktijken …
Pepper|5|False|…Echt niet.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Bonk!
Geest van Success|2|True|Goedendag!
Geest van Success|3|False|Staat u mij toe mij voor te stellen:
Geest van Success|4|False|ik ben de Succesgeest, immer tot uw dienst, vierentwintig uur per dag, zeven dagen per week!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geest van Success|1|False|*Uitgezonderd weekends en feestdagen; niet combineerbaar met andere aanbiedingen; zolang de voorraad strekt.
Geest van Success|2|False|Wereldwijde advertentie-campagne!
Geest van Success|3|False|Internationale faam!
Geest van Success|4|False|Stylisten en coiffeurs inbegrepen!
Geest van Success|5|False|Gegarandeerd en onmiddellijk resultaat!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geest van Success|1|False|Eén handtekeningetje onderaan dit document, en de poorten der glorie zullen zich voor u openen!
Pepper|2|False|Uhm, minuutje.
Pepper|4|True|Wat is dat hier voor een Deus ex machina?!
Pepper|3|False|Net nu het niet meer gaat, val jij uit de lucht met de wonderbaarlijke oplossing voor mijn probleem?!
Pepper|5|False|Het lijkt me héél toevallig!
Pepper|6|True|Kom, Carrot.
Pepper|7|False|Misschien zouden we daar vroeger ingetrapt zijn, maar nu zijn we niet meer zo dom om in zo'n val te lopen!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vogel|1|True|Roe!
Vogel|2|True|Koe!
Vogel|3|True|Roe!
Vogel|4|False|Koe!
Pepper|5|False|Kijk, Carrot, ik denk dat dit hoort bij volwassen worden.
Pepper|6|False|Goedkope trucjes doorzien, weer moeite leren doen …
Pepper|7|False|…want met jaloers zijn op anderen kom je nergens!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|1|False|Beroemdheid
Geschrift|2|False|50 000 Ko
Geschrift|3|False|De overwinning van Saffraan
Geschrift|4|False|Mode
Geschrift|5|False|Het fenomeen Saffraan
Pepper|8|False|Het lot zal óns uiteindelijk ook wel toelachen!
Geschrift|6|False|Chic
Geschrift|7|False|Special over Saffraan
Geluid|9|False|Bziooo!
Geschrift|10|False|Beroemdheid
Geschrift|11|False|Het sensationele leven van Mevrouw Duif
Geschrift|12|False|Mode
Geschrift|13|False|Het fenomeen Mevrouw Duif
Geschrift|14|False|Chic
Geschrift|15|False|Special over Mevrouw Duif
Verteller|16|False|- EINDE -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|08/2017 - www.peppercarrot.com - Tekeningen en verhaal: David Revoy - Vertaling: Midgard, Willem Sonke en Marno van der Maas.
Aftiteling|2|False|Proeflezing en dialoogverbeteringen: Alex Gryson, Calimeroteknik, Nicolas Artance en Valvin.
Aftiteling|4|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand en Alex Gryson.
Aftiteling|5|False|Software: Krita 3.1.4, Inkscape 0.92dev op Linux Mint 18.2 Cinnamon
Aftiteling|6|False|Licentie: Creative Commons Naamsvermelding 4.0
Aftiteling|8|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 879 patronen:
Aftiteling|7|False|Jij kan ook een patroon van Pepper&Carrot worden op www.patreon.com/davidrevoy
Aftiteling|3|False|Hulp bij verhaal en mise-en-scène: Calimeroteknik en Craig Maloney.
