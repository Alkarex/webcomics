# Transcript of Pepper&Carrot Episode 38 [vi]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Tập 38: Phù thủy trị thương

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Phù...
Vinya|2|False|Cuối cùng chúng ta đã hạ được nó!
Fritz|3|False|Ừ, thật khó khăn. Giờ chúng ta đã hiểu tại sao nhiều người kẹt ở đây.
Fritz|4|False|Brasic sao rồi?
Fritz|5|False|Brasic? Brasic!
Fritz|6|False|Trời! Tôi không để ý cậu thương nặng vậy!
Vinya|7|False|Phù thủy!
Vinya|8|False|Bọn tôi cần cô, NHANH LÊN!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Hãy tránh ra...
Sound|2|False|Tinh
Brasic|3|False|Vết thương đã biến mất.
Brasic|4|False|Lần nào cũng như một phép màu vậy.
Brasic|5|False|Cô quả là thiên tài, Phù thủy!
Brasic|6|False|Còn chần chừ gì nữa, kho báu đang chờ ta!
Fritz|7|False|Rõ rồi!
Fritz|8|False|Nhờ có cô, chúng ta bất bại!
Fritz|9|False|Hãy tìm quái vật tiếp theo nào!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Để tôi!
Vinya|2|False|Không! Để tôi!
Hạt Tiêu|3|False|?
Cây Yến Nhi|4|False|Con đang nghĩ gì vậy?
Cây Yến Nhi|5|False|Bọn ta đã tìm con hàng tháng trời.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Không phải quá rõ sao?
Hạt Tiêu|2|False|Con đã từ bỏ và quyết định đổi nghề.
Hạt Tiêu|3|False|Từ giờ, Con là Phù thủy trị thương vĩ đại Peph'Ra, như cô đã thấy.
Cây Yến Nhi|4|False|...
Hạt Tiêu|5|False|Cô hỏi tại sao ư ?!
Hạt Tiêu|6|False|Đơn giản là
Hạt Tiêu|7|False|CON CHỊU ĐỦ RỒI!
Hạt Tiêu|8|False|Con phát mệt vì bị săn đuổi, vì bị cười nhạo và hơn hết, không thể tìm được việc làm sau bao công sức học tập như vậy!
Hạt Tiêu|9|False|Ít nhất ở đây con được quý trọng vì kỹ năng của mình!
Brasic|10|False|Phù thủy!
Brasic|11|False|Đừng đi quá xa!
Brasic|12|False|Vinya vừa bị trúng đòn.
Sound|13|False|Tinh

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Thế thôi.
Hạt Tiêu|2|False|Có còn gì không ạ?
Cây Yến Nhi|3|False|Có.
Cây Yến Nhi|4|False|Tên mình bị ghi lên lệnh truy nã là điều bình thường với một phù thủy Chaosah.
Cây Yến Nhi|5|False|Thậm chí, có thể coi đó là việc đáng nể .
Cây Yến Nhi|6|False|Hãy mặc miệng lưỡi thiên hạ, và bắt đầu sự nghiệp của chính mình thay vì phó mặc cho người khác.
Cây Yến Nhi|7|False|Và hơn hết: con không biết trị thương.
Cây Yến Nhi|8|False|Sớm muộn gì họ cũng biết thôi, khi con hết sạch chỗ Nước mắt Phượng hoàng mà con mang theo để giả vờ mình biết trị thương đó .
Hạt Tiêu|9|False|Cái... ?
Hạt Tiêu|10|False|Làm sao cô ...
Hạt Tiêu|11|False|...đoán được.
Cây Yến Nhi|12|False|Xì!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Grrr!!!
Hạt Tiêu|2|False|Khi nào hết, con sẽ lấy thêm!
Hạt Tiêu|3|False|Bởi vì nó giúp được con!
Hạt Tiêu|4|False|Không như phép thuật Chaosah quái gở.
Hạt Tiêu|5|False|GRRR...!
Brasic|6|False|Coi chừng!
Brasic|7|False|Con quái vật đang biến hình!
Monster|8|False|CReeeeeeeee
Monster|9|False|eeeeeeeeeeeeeee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|CReeeeeeeeeeeeeeeeee
Monster|2|False|eeeeeeeeeeeeeeeee
Monster|3|False|eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
Sound|4|False|Rắc! ! !|nowhitespace
Sound|5|False|Rắc ! ! !|nowhitespace
Sound|6|False|Rắc ! ! !|nowhitespace
Sound|7|False|Xoàng ! ! !|nowhitespace
Sound|8|False|Xoàng ! ! !|nowhitespace
Sound|9|False|Xoàng! ! !|nowhitespace
Sound|10|False|Xoàng ! ! !|nowhitespace
Sound|11|False|Xoàng! ! !|nowhitespace
Sound|12|False|Xoảng! ! !|nowhitespace
Monster|13|False|eeeeeeee...
Sound|14|False|Chíu !!|nowhitespace
Vinya|15|False|Trời, thật phiền phức!
Brasic|16|False|Hãy yểm trợ. Tôi sẽ làm cho nó im hẳn luôn!
Brasic|17|False|Nghe không, quái vật?!
Brasic|18|False|Phù thủy, hãy chuẩn bị đề phòng...
Brasic|19|False|Phù thủy?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|ỐI!
Hạt Tiêu|2|False|không, không!
Brasic|3|False|Hô hô hô!
Brasic|4|False|Nhìn kìa! Mới nghe con quái vật gầm mà cô ta đã vãi ra rồi .
Brasic|5|False|Ha ha ha!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Không những vậy lại còn xanh lè!
Vinya|8|False|HAHA HA HA!
Vinya|9|False|Không biết có trị thương được không?
Fritz|10|False|Eo, tởm quá!
Fritz|11|False|HAHA HA HA!
Brasic|12|False|HA HA HA!
Hạt Tiêu|13|False|...
Fritz|14|False|Ê!
Brasic|15|False|Chờ đã ! Chúng tôi chỉ đùa thôi!
Vinya|16|False|Phù thủy!
Cây Yến Nhi|17|False|Chào mừng trở lại.
Title|18|False|- HẾT -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|26 tháng Tư, 2023 Tranh vẽ: David Revoy. Người đọc thử: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Bản dịch tiếng Việt: Ngô Ngọc Đức Huy. Soát lỗi: (trống). Dựa trên vũ trụ Hereva Tác giả: David Revoy. Trưởng nhóm duy trì: Craig Maloney. Biên tập: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Hiệu đính: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Phần mềm: Krita 5.0.5, Inkscape 1.2 trên Fedora 36 KDE Spin. Giấy phép: Creative Commons Attribution 4.0. www.peppercarrot.com
Hạt Tiêu|2|False|Bạn có biết?
Hạt Tiêu|3|False|Pepper&Carrot là bộ truyện hoàn toàn miễn phí, tự do, mã nguồn mở và được tài trợ bởi độc giả.
Hạt Tiêu|4|False|Tập này xin cảm ơn 1084 người tài trợ !
Hạt Tiêu|5|False|Bạn cũng có thể tài trợ Pepper&Carrot và được ghi tên ở đây!
Hạt Tiêu|6|False|Chúng tôi nhận ủng hộ từ Patreon, Tipeee, PayPal, Liberapay ...!
Hạt Tiêu|7|False|Xem www.peppercarrot.com để biết thêm chi tiết!
Hạt Tiêu|8|False|Chân thành cảm ơn!
