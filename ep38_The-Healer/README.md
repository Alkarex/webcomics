# Episode 38: The Healer

![cover of episode 38](https://www.peppercarrot.com/0_sources/ep38_The-Healer/low-res/Pepper-and-Carrot_by-David-Revoy_E38.jpg)

## Comments from the author

I had too many "open doors" to close on the Pepper&Carrot series: Pepper's quest to find work, her "wanted" poster, her humiliations with spells not working as she wanted... She wasn't really taking decisions in regard to all of that. When at the end of the last episode Pepper got a big supply of 'Phoenix's tears' I thought it was the perfect opportunity for her to attempt a radical change.

This is something I also feel on my social bubble: the desire to leave everything and start anew. So I thought it could be cathartic to show a story where Pepper actually does that, without any compromise. It was also fun to show Pepper resisting Cayenne and underline again how rebellious she is against authority. It was also a secret pleasure to draw Cayenne actually caring for Pepper even though she hides it.

This episode is also about the impostor syndrome, It's about new groups of "friends" welcoming you based on interest, and it's about family always reminding you of your roots. It's also announcing the way back home for Pepper after many episodes 'in the wild'. I personally really needed to hear this final "Welcome back" from Cayenne at the end.

From [Beta-reading Forum of episode 38](https://framagit.org/peppercarrot/webcomics/-/issues/238)
