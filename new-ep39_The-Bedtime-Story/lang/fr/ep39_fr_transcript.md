# Transcript of Pepper&Carrot Episode 39 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Episode 39: Une histoire avant d'aller au lit.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Merci d'avoir accepté de faire cette petite halte par mon abris.
Pepper|2|False|J'avais sérieusement besoin de me changer et de prendre une douche.
Cayenne|3|False|De toutes manières, il aurait été trop difficile de rejoindre Qualicity directement.
Cayenne|4|False|Nous partirons demain à l'aube.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Thyme et Cumin ont bien reçu mon signal, et sont en route à présent.
Cayenne|2|False|Elles auront certainement un peu d'avance sur nous.
Cayenne|3|False|A Qualicity, ton amie la reine Coriandre te parlera de ses progrès en matière de diplomatie.
Cayenne|4|False|Elle pense pouvoir faire cesser l'avis de recherche sur ta tête et celle de tes amies.
Cayenne|5|False|... ?
Cayenne|6|False|On peut savoir qu'est-ce-qui te fait sourire bêtement comme ça ?
Pepper|7|False|Oh, c'est juste que... Enfin, ta voix... Quand j'étais petite, tu ne m'as jamais raconter d'histoire avant d'aller au lit. Alors je m'imaginais un peu ce que ça aurait pu faire.
Cayenne|8|False|...
Cayenne|9|False|Il ...
Cayenne|10|False|Il était une fois ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|... trois sorcières.
Cayenne|2|False|C'était les trois uniques sorcières du chaos pour être plus précise.
Cayenne|3|False|Elles étaient reconnues, respectées et craintes dans tout le royaume.
Cayenne|4|False|Les puissants, les stratèges... Tout le monde faisait appel à leur puissance et demandaient leur conseils.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Mais oui bien sure...
Pepper|2|False|Trois sorcières du Chaos! ... Comme par hazard !
Carrot|3|False|Shh !
Pepper|4|False|OK, OK, j'ai rien dit ! Continue.
Cayenne|5|False|Jusqu'au jour donc où une guerre éclatat.
Cayenne|6|False|Une guerre entre sorcières.
Cayenne|7|False|Au fil des combats, les sorcières de l'eau, la lumière et la nature formérent une aliance contre la mort, le feu et le chaos.
Cayenne|8|False|Nos trois sorcières, aussi fortes qu'elle se pensaient, ne purent cependant pas mettre un terme a ce conflit.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Car une sorcière de la lumière, un prodige elevée par les dragons blancs domina clairement chaque bataille.
Cayenne|2|False|Ennivrée de victoire et de puissance, cette dernière franchie cependant un tabou dans un ultime combat :
Cayenne|3|False|Et tua nos trois sorcières, éradiquant ainsi toute l'école du Chaos.
Cayenne|4|False|Ce macabre évènement glaça le sang de tous les béligérents et la guerre pris fin.
Cayenne|5|False|La lumière domina alors les autres magies.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Mais ...
Carrot|2|False|Shh !
Cayenne|3|False|Cependant, n'ayant plus aucun maître, le chaos allait alors s'acharner sauvagement sur le monde.
Cayenne|4|False|Une ville vit un de ses arbre au milieu de sa place centrale grandir monstrueusement et s'envoler, arrachant la ville entièrement du sol.
Cayenne|5|False|Une diversité chaotique d'être vivants et de dragons naquirent cette année là.
Cayenne|6|False|La magie elle-même se mit également à devenir aléatoire, capricieuse, et imprévisible ...
Cayenne|7|False|...entrainant famines, accidents et instabilités politiques sur tout Hereva.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Cette période de désordre profita cependant à la fille des dragons.
Cayenne|2|False|Elle utilisa sa magie pour faire l'illusion qu'elle était en contacte avec des entité supérieures. Les foules entrèrent en adoration pour son culte et cela calma les esprits un temps.
Cayenne|3|False|Sa magie de la lumière "Lumiah" devint seulement "Ah", la magie des revenants et des arrières mondes.
Cayenne|4|False|Étant totalement dominé, aucune autre école ne dénonca cette manipulation.
Cayenne|5|False|C'est ainsi qu'un culte mystique commença autour de sa personne.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Cependant les désastres de grande ampleur continuaient et le chaos se déchainant partout.
Cayenne|2|False|Et c'est après le géant tremblement de terre de Qualicity qui isola une partie de la ville au milieu d'un desert que la magie des morts décida d'une solution radicale:
Cayenne|3|False|... faire revenir de parmis les mort nos trois sorcières du Chaos pour stabiliser le monde et qu'elles transmettent leurs enseignements à une nouvelle génération.
Cayenne|4|False|L'opération nécessita la création du Crystal de Réa: un artefact puissant qui permettait d'entretenir ce retour à la vie.
Cayenne|5|False|Mais la création d'un tel objet avait un coût sans précédent.
Cayenne|6|False|Toutes les plus puissantes sorcières alors en vie y sacrifièrent une partie de leur Réa, les affaiblissants à vie.
Cayenne|7|False|L'opération fût cependant un succès et le Chaos se calmat, les magies réapprirent par la même occasion à collaborer et le monde se stabilisa.

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Cependant, nos trois sorcières ne trouvèrent qu'une seule enfant capable de canaliser le Chaos dans ce nouveau monde.
Cayenne|2|False|On leur donna donc l'ordre de mettre leurs enseignements par écris et de s'appliquer à son éducation.
Cayenne|3|False|Mais elles étaient perdues car elles évoluaient dans un monde qu'elles ne reconaissaient plus.
Cayenne|4|False|Un monde ou elles ne pouvaient plus se venger ou dominer.
Cayenne|5|False|Un monde ou leur survie ne dépendais que d'un crystal, quelquepart dans les sous-sols de Qualicity.
Cayenne|6|False|Un monde ou le processus de zombification les avait fait viellir et blanchie leur yeux...
Cayenne|7|False|Fin de l'histoire.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Quoi ?!
Pepper|2|False|Mortes ?! Enfin, j'veux dire, plus en vie ?!
Pepper|3|False|Mais... Mais... ...c'est affreux !
Pepper|4|False|POURQUOI JE N'AI JAMAIS ÉTÉ MISE AU COURANT DE TOUT ÇA ?!
Cayenne|5|False|Tsss ! Comme si tu nous aurais écouté ...
Cayenne|6|False|En tout cas, maintenant c'est fait.
Cayenne|7|False|Essaie de te reposer pour être en forme demain; Qualicity ce n'est pas la porte d'a côté.
Pepper|8|False|J'ai ...
Pepper|9|False|... J'ai été élevé par des ZOMBIES.
Pepper|10|False|Et comment je vais pouvoir dormir après ÇA ...?!
Peppper|11|False|C'est juste la pire histoire avant d'aller au lit DE TOUS LES TEMPS !
Titre|12|False|- FIN -

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Le █ █████ ███ Art & scénario : David Revoy. Lecteurs de la version bêta : ███ ████, ███ ████, ███ ████, ███ ████, ███ ████. Version française originale Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 5.2.0-rc1appimage, Inkscape 1.3 sur Fedora Linux 38 (KDE Plasma) Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Le saviez-vous ?
Pepper|3|False|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de %%%% mécènes !
Pepper|5|False|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|False|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
