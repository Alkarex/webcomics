# Transcript of Pepper&Carrot Episode 39 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Episode 39: The Bedtime Story.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Thank you for agreeing to make this little stop at my shelter.
Pepper|2|False|I seriously needed to change and shower.
Cayenne|3|False|In any case, it would have been too difficult to join Qualicity directly.
Cayenne|4|False|We'll be leaving tomorrow at dawn.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Thyme and Cumin have received received my signal, and are now on their way.
Cayenne|2|False|They will certainly be a little ahead of us.
Cayenne|3|False|In Qualicity, your friend Queen Coriander will tell you about her progress in diplomacy.
Cayenne|4|False|She thinks that she can get the wanted notice away from you and away from your friends.
Cayenne|5|False|... ?
Cayenne|6|False|Can I find out what is the cause of your goofy smile?
Pepper|7|False|Oh, it's just that... I mean, your voice... When I was little, you never told me a story before I went to bed. So I imagined what it would have been like.
Cayenne|8|False|...
Cayenne|9|False|Once ...
Cayenne|10|False|Once upon a time ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|...three witches.
Cayenne|2|False|They were the only three witches of Chaos, to be more precise..
Cayenne|3|False|They had recognition, respect, and fear throughout the kingdom.
Cayenne|4|False|The powerful, the strategists... Everyone appealed to their power and sought their advice.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Of course of course...
Pepper|2|False|Three witches of Chaos! ... What a coincidence!
Carrot|3|False|Shh !
Pepper|4|False|OK, OK, I didn't say anything! Go ahead.
Cayenne|5|False|Until, that is, war broke out.
Cayenne|6|False|A war between witches.
Cayenne|7|False|Through battles, the witches of water, light, and nature had formed an alliance against death, fire, and chaos.
Cayenne|8|False|But our three witches, as strong as they thought they were, were unable to put an end to the conflict.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Because a witch of the Light, a prodigy raised by the white dragons, clearly dominated every battle.
Cayenne|2|False|In the euphoria of victory and power, this one nevertheless broke a taboo in a final battle:
Cayenne|3|False|And killed our three witches, eradicating the entire School of Chaos.
Cayenne|4|False|This macabre event chilled the blood of all belligerents, and the war came to an end.
Cayenne|5|False|Light then dominated all other magic.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|But...
Carrot|2|False|Shh !
Cayenne|3|False|However, with no master left, Chaos would then savagely descend upon the world.
Cayenne|4|False|A city saw one of its trees in the middle of its central square grow monstrously and fly away, tearing the whole town from the ground.
Cayenne|5|False|In that year, a chaotic variety of living creatures and dragons were born into existence.
Cayenne|6|False|Magic itself also began to become random, capricious and unpredictable...
Cayenne|7|False|...leading to famine, accidents and political instability throughout Hereva.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|However, this period of confusion benefited the daughter of dragons.
Cayenne|2|False|She used her magic to create the illusion that she was in contact with higher entities. Crowds worshipped her cult and this calmed people down for a while.
Cayenne|3|False|Her light magic "Lumiah" became only "Ah", the magic of ghosts and alternate worlds.
Cayenne|4|False|Being totally dominated, no other school denounced this manipulation.
Cayenne|5|False|This is how a mystical cult began around her person.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|However, large-scale disasters continued, and chaos broke out everywhere.
Cayenne|2|False|And it was after the giant Qualicity earthquake, which isolated part of a city in the middle of the desert, that the magic of the dead decided on a radical solution:
Cayenne|3|False|... bring our three witches of Chaos back from the dead to stabilize the world and pass on their teachings to a new generation.
Cayenne|4|False|The operation required the creation of the Crystal of Réa: a powerful artifact that could sustain this return to life.
Cayenne|5|False|But creating such an object came at an unprecedented cost.
Cayenne|6|False|All the most powerful witches then alive sacrificed part of their Rea, weakening them for life.
Cayenne|7|False|The operation was a success, however, and Chaos calmed down, magics began to work together again and the world stabilized.

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|However, our three witches found only one child capable of channeling Chaos in this new world.
Cayenne|2|False|So they were instructed to write down their teachings and apply themselves to her education.
Cayenne|3|False|But they were lost in a world they no longer recognized.
Cayenne|4|False|A world where they could no longer take revenge or dominate.
Cayenne|5|False|A world where their survival depended on a single crystal, somewhere in the basement of Qualicity.
Cayenne|6|False|A world where the process of zombification had aged them and whitened their eyes...
Cayenne|7|False|End of the story.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|What?!
Pepper|2|False|Dead?! Well, I mean, no longer alive?!
Pepper|3|False|But... But... ...it's awful!
Pepper|4|False|WHY WAS I NEVER TOLD ABOUT ALL THIS?!
Cayenne|5|False|Tsss! As if you you would have listened to us ...
Cayenne|6|False|In any case, now it's done.
Cayenne|7|False|Try to get some rest so you're fit tomorrow; Qualicity isn't next door.
Pepper|8|False|I was ...
Pepper|9|False|... I was raised by ZOMBIES.
Pepper|10|False|And how am I going to sleep after THAT...?!
Peppper|11|False|This is just the worst bedtime story EVER!
Titre|12|False|- FIN -

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Le █ █████ ███ Art & scénario : David Revoy. Lecteurs de la version bêta : ███ ████, ███ ████, ███ ████, ███ ████, ███ ████. Version française originale Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 5.2.0-rc1appimage, Inkscape 1.3 sur Fedora Linux 38 (KDE Plasma) Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Le saviez-vous ?
Pepper|3|False|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de %%%% mécènes !
Pepper|5|False|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|False|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
