# Transcript of Pepper&Carrot Episode 10 [ga]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 10 : Especiau estiu

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -
Crèdits|2|False|08/2015 - Dessenh & Scenari : David Revoy

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot qu'ei completament liure, open source, e esponsorizat mercés au mecenat deus sons lectors. Entad aqueste episòdi, mercé aus 422 mecènas :
Crèdits|3|False|https://www.patreon.com/davidrevoy
Crèdits|2|True|Vos tanben, vadetz mecèna de Pepper&Carrot entà l'episòdi vienent sus
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logiciaus : aqueste episòdi qu'estó dessenhat a 100% dab logiciaus liures Krita 2.9.6, Inkscape 0.91 sus Linux Mint 17
