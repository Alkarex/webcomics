# Transcript of Pepper&Carrot Episode 26 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 26: Bücher sind klasse

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Bücher sind klasse!
Pepper|3|False|Es ist Gold wert!
Pepper|2|True|Dieses seltene Buch enthält die Notizen eines erfahrenen Abenteuers und beschreibt alles über diese Burg.
Pepper|4|False|Zum Beispiel: Dieses Auge des Bösen am Eingang schießt tödliche Feuerbälle, um unerwünschte Besucher abzuschrecken.
Pepper|5|False|Aber wenn man sich ihm vorsichtig von der Seite nähert, außer Sichtweite....

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...und dann das Auge mit einem Hut bedeckt...
Pepper|3|False|...sollte sich die Tür von selbst öffnen!
Pepper|4|True|Es wird sogar empfohlen, den Hut dazulassen, um die Flucht zu vereinfachen!
Pepper|5|False|Bücher sind so toll!
Schrift|6|False|„Springen Sie direkt über den ersten Balkon.“
Pepper|7|False|OK!
Geräusch|9|False|BOING
Geräusch|10|False|BOING
Schrift|8|False|„Es könnte sich ein Fallgefühl einstellen. Das ist normal. Dies ist eine Abkürzung.“
Schrift|11|False|„Genießen Sie die bequemen Leuchtnetze völlig risikofrei: Diese Spinnen sind photosynthetisch und ernähren sich vom reflektierten Licht.“
Schrift|12|False|„Ich empfehle ein kleines Nickerchen, um wieder zu Kräften zu kommen.“
Pepper|13|False|Oh, ich liebe Bücher!
Geräusch|2|False|Ploff!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Ich weiß ja, wie ungern Katzen nass werden.
Pepper|3|True|Keine Sorge!
Pepper|4|False|Auch dafür sind Bücher gut!
Pepper|5|True|In weniger als zehn Minuten nachdem ich die Burg betreten habe, sitze ich in der Schatz-kammer und pflücke die Blätter des seltenen Wasserbaums.
Pepper|6|False|Bücher sind magisch!
Pepper|1|True|Oh, Carrot...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|3|False|„Da Sie die Abkürzung genommen haben, sollten Sie nun zum Wächter gelangen.“
Pepper|4|True|Hmm...
Pepper|5|True|der End-gegner?!?
Pepper|6|False|Wo denn?
Pepper|7|True|AH-HA...
Pepper|8|False|Gefunden!
Pepper|11|True|Pfff...!
Pepper|12|False|Zu einfach, wenn man die Schwachstelle kennt!
Schrift|1|False|„Auf dem Weg eine Feder aufheben.“
Pepper|2|False|An Federn mangelt es hier nicht!
Geräusch|9|True|kitzel
Geräusch|10|False|kitzel

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Schkrrkrr!
Pepper|2|False|Aha! Da ist der Ausgang!
Geräusch|3|False|Wuuup!
Pepper|5|True|Iiieh...
Pepper|6|False|Bestimmt steht hier was dazu, wie man dieses „Ding“ entfernt...
Pepper|7|False|Carrot... Kannst du es lesen?
Pepper|8|True|Mm... natürlich nicht...
Pepper|9|False|Ach...
Pepper|4|False|?!
Pepper|12|False|Grrr!!!
Pepper|10|True|ICH WUSSTE ES!!!
Pepper|11|True|Es war zu gut, um wahrzusein!
Pepper|13|False|Vielleicht können Bücher doch nicht jede Lage meistern...
Geräusch|14|False|Paff!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...es sei denn
Pepper|2|False|HIAAAAAAAA!!!
Geräusch|3|False|Bäm!
Pepper|5|False|...Bücher sind klasse!
Pepper|4|True|Doch, es stimmt; in jeder Situation...
Erzähler|6|False|- ENDE -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Lizenz: Creative Commons Namensnennung 4.0. Software: Krita 4.1, Inkscape 0.92.3 auf Kubuntu 17.10. Illustration & Handlung: David Revoy. Script Doctor: Craig Maloney. Beta-Leser: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Deutsche Version Übersetzer: Ret Samys. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28. Juli 2018 www.peppercarrot.com
Impressum|3|False|Auch du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
Impressum|2|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Folge geht der Dank an 1098 Förderer:
