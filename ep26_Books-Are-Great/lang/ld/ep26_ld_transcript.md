# Transcript of Pepper&Carrot Episode 26 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 26: Methalethíle Áabe

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Bíi methalethíle áabe wi!
Loyud|3|False|Thalethúle hi!
Loyud|2|True|Thi wobalin woháabe hi úthú eril thod wothad wohimá, i di be abesh mathethuth wáa.
Loyud|4|False|Bóodi láad ne: Bíi duholob worathal wohoyi hi áathesha óowababenan úwanú menáthed rathóo wáa.
Loyud|5|False|Izh bere sháad beye lóolonal bedim ib, beyeth raláadeshub...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Míi le! Denedi thodá úthú yeneth benemeshub úwanú aril thad nasháad radozhenal wa!
Loyud|3|False|...id rumad oyith yenenan...
Loyud|4|True|...ébere nahu áath beyóoth wáa!
Loyud|5|False|Methalethúul áabe wi!
Thod|6|False|“Bóodi oób ne ralithenal thibáhimu rayil.”
Loyud|7|False|Em wáa!
Zho|9|False|ODEZHO
Zho|10|False|ODEZHO
Thod|8|False|“Bíidi rilrili loláad ne háda nehé wa. Bóo lhith ra ne wi. Bíidi bohí weth wi.”
Thod|11|False|“Bóo láadethéle ne she dathimethu zhubethoth yomenal wa: Bíidi meyod dathimezhub hi itheth dalanal wi.”
Thod|12|False|“Bóo dibóo le wohahí woháanath úwanú dul ne.”
Loyud|13|False|Bíi athíle le áabe wa!!
Zho|2|False|Rayahanesh!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|2|False|Bíi lothel le wa. Menéde menalili ra rul wi.
Loyud|3|True|Bóo lhith ra ne!
Loyud|4|False|Bíi methal áabe hiwan!
Loyud|5|True|Bíi eril meshóo áal thab, id ril wod le worashud woshodeha, i bel le mith Iliyáaninede wa.
Loyud|6|False|Meyahanesh áabe wa!
Loyud|1|True|Bíida shud Ámed wa;

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thod|3|False|“Bíidi beróo eril sháad ne wobohí wowethemu, ril nosháad shodeha Lúul bethu wa.”
Loyud|4|True|Mmm...
Loyud|5|True|worahíya wohu wáa?!?
Loyud|6|False|Izh bebáaha?
Loyud|7|True|AÁ...
Loyud|8|False|Redeb le neth!
Loyud|11|True|Lhlhlh...!
Loyud|12|False|Bere lothel beye radowudeth, ébere hith dódozheshub wa!!
Thod|1|False|“Bóo héedá ne yulisheth wethemu.”
Loyud|2|False|Bíi meham yulish menedebe nuha wa!
Zho|9|True|adama
Zho|10|False|adama

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zho|1|False|Rrrruuum!
Loyud|2|False|Methad mesháad lezh nude wáa!!
Zho|3|False|Wuuub!
Loyud|5|True|Báa dósháad le "dal" hith bebáanal? Bíi edeláad le úthú di áabe...
Loyud|6|False|Ámed... Báa thad wéedan ne beth?
Loyud|7|False|Mm... thad ra ne wi...
Loyud|8|True|Lhlh...
Loyud|9|False|?!
Loyud|4|False|Lhlhlh!!!
Loyud|12|False|ERIL LOTHEL LE WA!!!
Loyud|10|True|Eril ralorolo be wa!
Loyud|11|True|Rilrili methad medóthal ra áabe abesheth hadihad wa...
Loyud|13|False|Heb!
Zho|14|False|Ílhilh...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|...íizha
Loyud|2|False|YAAAAAAAA!!!
Zho|3|False|SHUM!
Loyud|5|False|...methalethíle áabe wa!
Loyud|4|True|Ra. Bíi dóon be wa; hadihad, ...
Dedidehá|6|False|- NODI -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|1|False|Hudi: Creative Commons Attribution 4.0. Bodibod: Krita 4.1, Inkscape 0.92.3 Kubuntu 17.10 benal. Alehala i woban: David Revoy. Dórawulúd dedidewan: Craig Maloney. Wodide wowéedaná: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Héedan Láadanedim: Yuli i Álo. Bíi nosháad dedide thera Shebedoni bethude wa Elá: David Revoy. Wohun wonayahá: Craig Maloney. Dedidethodá: Craig Maloney, Nartance, Scribblemaniac, Valvin. Dóoná: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 2018, Ahum thabeshin i nibeya www.peppercarrot.com
Dohiná|3|False|Bíi thad naden ne Loyud i Ámed beth www.patreon.com/davidrevoy beha wa
Dohiná|2|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 1098 hi:
