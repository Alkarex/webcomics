# Transcript of Pepper&Carrot Episode 29 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 29: Verdsøydeleggjaren

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|2|True|Endeleg!
Monster|3|False|Ei interdimensjonal rivne!
Monster|4|False|Truleg resultatet av ei kosmisk storhending!
Monster|5|True|Veks! Veks, vesle rivne!
Monster|6|False|Og vis meg ei ny verd som eg kan TRÆLBINDA og HERSKA OVER!
Monster|7|True|Oi sann ...
Monster|8|False|Dette var interessant.
Monster|9|False|I dag er visst lukkedagen min ...
Forteljar|1|False|Samtidig, i ein annan dimensjon ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|Ein dimensjon der det bur intelligente vesen!
Monster|2|False|Kom igjen, vesle rivne!
Monster|3|False|Veks! Muhaha-hahaha!
Pepar|4|True|Puh!
Pepar|5|False|Det var jammen eit kjekt selskap, kjære vener!
Koriander|6|False|Forresten, Safran og eg innsåg nettopp at me ikkje har peiling på korleis kaosah-trolldommen din verkar.
Safran|7|False|Ja, det er eit skikkeleg mysterium. Kan du gjera oss litt klokare?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Ha-ha, det er litt vrient å forklara.
Pepar|2|True|Men du kan på ein måte seia at trolldommen byggjer på det å skjøna lovene som styrer kaotiske system ...
Pepar|3|False|... dei minste til dei største.
Koriander|4|False|Ja, det var jo veldig klårgjerande ...
Pepar|7|False|Vent litt, så kan eg kanskje finna eit enkelt døme.
Pepar|8|False|Berre ein augneblink, slik at den gode, gamle kaosah-visdommen får verka litt ...
Pepar|9|False|Der ja!
Lyd|5|True|KLØ
Lyd|6|False|KLØ
Pepar|10|True|Ser de denne tann-pirkaren som låg mellom brusteinane?
Pepar|11|False|Ved å plukka han opp har eg heilt sikkert forhindra nokon å tråkka på han.
Pepar|12|True|Ei lita, positiv endring i livsens store kaotiske system kan altså ha enorme konsekvensar.
Pepar|13|False|Det er dette kaosah handlar om!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Æsj!
Koriander|2|False|Uææh! Nokon har hatt han i munnen!
Safran|3|False|Ha-ha! Du imponerer som alltid, Pepar!
Koriander|4|False|Fint, Pepar. Takk for denne ... «forklaringa».
Koriander|5|True|Men no er det vel på tide å krypa til køys?
Koriander|6|False|Og så bør nok nokon vaska hendene sine.
Pepar|7|False|Hei sann! Vent på meg!
Lyd|8|False|Knips!
Lyd|9|True|Tapp!
Lyd|10|False|Tapp!
Lyd|11|False|SMEKK!
Gulrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Ssss!
Lyd|2|False|Paff!
Lyd|3|False|Tapp!
Lyd|4|False|KNUS!
Lyd|5|False|Sjrroof...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|7|False|TIL ÅTAK!!!
Monster|6|True|MuhahaHAHA! Endeleg!
Monster|9|False|!?
Skrift|1|True|LAGERROM
Skrift|2|False|FYRVERKERI
Lyd|3|False|Sjrroof!!
Lyd|4|False|Sjrr!!
Lyd|5|False|BANG!
Lyd|8|False|Fisss!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|3|False|SMELL!
Lyd|2|False|BANG!
Lyd|1|False|KRASJ!
Lyd|4|False|Fusss!
Lyd|5|False|PoFF!
Pepar|6|True|Ta det med ro, Gulrot.
Pepar|7|False|Det er nok berre nokon som ikkje har avslutta feiringa heilt enno ...
Pepar|8|False|God natt, litle ven!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|False|...
Karve|1|False|Verkeleg? Klarte ho å utløysa ein så enorm kjedereaksjon utan å vera klar over det sjølv?!
Kajenne|2|False|Utan tvil.
Timian|3|False|Mine damer, no trur eg vår kjære Pepar endeleg er klar!
Lyd|6|False|Fvip!
Lyd|4|False|Fsjjjj!
Forteljar|7|False|– SISTE DEL AV TRILOGIEN OM KRONINGA AV KORIANDER –

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|25. april 2019 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance og Valvin. Omsetjing til engelsk: CalimeroTeknik, Craig Maloney, Martin Disch og Midgard. Omsetjing til nynorsk: Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. Programvare: Krita 4.1.5~appimage og Inkscape 0.92.3 på Kubuntu 18.04.1. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 960 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
