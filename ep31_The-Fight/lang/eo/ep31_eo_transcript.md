# Transcript of Pepper&Carrot Episode 31 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 31a : La Batalo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rakontanto|1|False|Tenebrumo, sankta monteto de Ĥaosaho.
Sono|2|False|VRuŭum!
Pipro|3|False|Tsk!
Sono|4|False|VRuŭŭŭm
Sono|5|False|Brzuŭŭ!
Pipro|6|False|Prenu tion!
Sono|8|False|Sĉh!
Kajeno|9|False|Amatore!
Pipro|10|False|!!
Sono|11|False|SLAM!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|False|Drzzuŭŭv!
Sono|2|False|KRAK!!!
Sono|3|False|KRAAK!!!
Pipro|4|False|Ne tiel rapide!
Pipro|5|False|GRAVITATIONAS ŜILDUS!
Sono|6|False|Vuŭŭŭŝ!!
Sono|7|False|Tĉkŝkk!!
Sono|8|False|Tĉkŝkk!!
Sono|9|False|Tok!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|KAROĈJO!
Pipro|2|False|Plano 7a-B!
Pipro|4|False|JPEGUS QUALITIS!
Sono|5|False|Brzuŭŭ !
Sono|3|False|Zuŭŭm
Kajeno|6|False|?!!
Kajeno|10|False|Aaŭ!!
Sono|7|True|G
Sono|8|True|Z|nowhitespace
Sono|9|False|Z|nowhitespace
Pipro|11|False|QUALITIS MINIMALIS!
Kajeno|12|False|!!
Kajeno|13|False|Grr...
Skribaĵo|14|False|2019-12-20-E31P03_V15-final.jpg
Skribaĵo|15|False|Eraro de ŝargo
Sono|16|False|KRAŜ !!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajeno|1|False|PYRO BOMBA ATOMICUS!
Sono|2|False|Frrcuŭŭv!
Sono|3|True|K
Sono|4|True|A|nowhitespace
Sono|5|True|B|nowhitespace
Sono|6|True|U|nowhitespace
Sono|7|True|M|nowhitespace
Sono|8|True|!|nowhitespace
Sono|9|False|!|nowhitespace
Sono|10|True|BRRR
Sono|11|False|BRRR
Sono|12|False|Pŝhh...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajeno|1|False|Pfft...
Kajeno|2|True|Fuĝi al dimensieto en via aĝo?
Kajeno|3|False|Kia mizera malvenko...
Pipro|4|True|Erare!
Pipro|5|False|Vermotruo!
Sono|6|False|Bcc!!
Pipro|7|False|Dankon por la eliro, Karoĉjo!
Pipro|8|False|Ŝakmato, majstro Kajeno!
Pipro|9|False|GURGES...
Pipro|10|False|...ATER!
Sono|12|False|SSvvvvvvijjjjjjjpppp!!!
Sono|11|False|VRUŬM!!!
Timiano|13|False|ĈESU!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timiano|1|True|Mi diris ĈESU!
Timiano|2|False|Tio sufiĉas!
Sono|3|False|Klak!
Sono|4|False|PUŬŬF!
Sono|5|False|TĈK!
Timiano|6|True|Ambaŭ vi!
Timiano|7|True|ĈI TIEN!
Timiano|8|False|Tuj!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|False|Tok!
Sono|2|False|Tok!
Timiano|3|True|Estas jam tri jaroj ekde kiam, vi diris al ni "nur lastminuta testo"...
Timiano|4|False|...ni ne restos la tutan nokton ĉi tie!
Timiano|5|False|Do...?
Pipro|6|False|...
Kajeno|7|True|BONE...
Kajeno|8|True|...ŝi povas havi sian diplomon...
Kajeno|9|False|...sed kun "akceptebla" nivelo.
Skribaĵo|10|True|Diplomo
Skribaĵo|11|True|de
Skribaĵo|12|False|Ĥaosaho
Skribaĵo|14|False|Kumino
Skribaĵo|13|False|Kajeno
Skribaĵo|15|False|Timiano
Skribaĵo|16|False|~ al Pipro ~
Skribaĵo|17|False|Oficiala Sorĉisto
Rakontanto|18|False|- FINO -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon ĉi tie!
Pipro|3|True|Pepper&Carrot estas tute libera, malfermitkoda kaj subtenita danke al la mecenateco de siaj legantoj.
Pipro|4|False|Pri ĉi tiu rakonto, dankon al la 971 mecenantoj!
Pipro|7|True|Vidu www.peppercarrot.com por pli da informo!
Pipro|6|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ...kaj en multaj pli!
Pipro|8|False|Dankon!
Pipro|2|True|Ĉu vi sciis?
Atribuintaro|1|False|20a de decembro de 2019 Arto kaj scenaro: David Revoy. Beta-legantoj: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Esperanta versio Traduko: Jorge Maldonado Ventura. Fasono: Navi, Tirifto. Bazita sur la universo de Hereva Kreinto: David Revoy. Ĉefa fleganto: Craig Maloney. Verkistoj: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korektistoj: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programaro: Krita 4.2.6appimage, Inkscape 0.92.3 sur Kubuntu 18.04-LTS. Licenco: Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
