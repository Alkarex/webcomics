# Transcript of Pepper&Carrot Episode 31 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 31: Spopad

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Dremota, sveti kaosaški grič.
Zvok|2|False|VRummm!
Paprika|3|False|Pha!
Zvok|4|False|VRuummm
Zvok|5|False|Bruzzz!
Paprika|6|False|Izvoli tole!
Zvok|8|False|Šhh!
Kajenka|9|False|Amaterka!
Paprika|10|False|!!
Zvok|11|False|POK!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Drzzaaf!
Zvok|2|False|TRESK!!!
Zvok|3|False|TREESK!!!
Paprika|4|False|Ne tako hitro!
Paprika|5|False|ŠČITUS PADALIS!
Zvok|6|False|Vuuuuš!!
Zvok|7|False|Tčkškk!!
Zvok|8|False|Tčkškk!!
Zvok|9|False|Tok!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|KORENČEK!
Paprika|2|False|Načrt 7-B!
Paprika|4|False|KODIRUS JPEGUS!
Zvok|5|False|Brzamm!
Zvok|3|False|Švirr
Kajenka|6|False|?!!
Kajenka|10|False|Aaah!!
Zvok|7|True|G
Zvok|8|True|Z|nowhitespace
Zvok|9|False|Z|nowhitespace
Paprika|11|False|KVALITETIS KATASTROFUS!
Kajenka|12|False|!!
Kajenka|13|False|Grrr…
Napis|14|False|2019-12-20-E31P03_V15-final.jpg
Napis|15|False|Napaka med nalaganjem
Zvok|16|False|ŠKRC !!!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|False|PIROTEHNIKUS ATOMIKUS!
Zvok|2|False|Frrcuuuf!
Zvok|3|True|K
Zvok|4|True|A|nowhitespace
Zvok|5|True|B|nowhitespace
Zvok|6|True|U|nowhitespace
Zvok|7|True|M|nowhitespace
Zvok|8|True|M|nowhitespace
Zvok|9|True|!|nowhitespace
Zvok|10|False|!|nowhitespace
Zvok|11|True|BRRR
Zvok|12|False|BRRR
Zvok|13|False|Pššš...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|False|Pfff…
Kajenka|2|True|Pobeg v mikrodimenzijo? Pri tvoji starosti?
Kajenka|3|False|Kakšen sramoten poraz...
Paprika|4|True|Narobe!
Paprika|5|False|Črvina!
Zvok|6|False|Bzz!!
Paprika|7|False|Hvala za izhod, Korenček!
Paprika|8|False|Šah-mat, mojstrica Kajenka!
Paprika|9|False|GURGES …
Paprika|10|False|…ATER!
Zvok|12|False|Švvvuuuuuuppppp!!!
Zvok|11|False|VUŠ!!!
Timijana|13|False|STOJ!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timijana|1|True|STOJ, sem rekla!
Timijana|2|False|Dovolj je!
Zvok|3|False|Tlesk!
Zvok|4|False|PUFFF!
Zvok|5|False|SVIŠ!
Timijana|6|True|Obe!
Timijana|7|True|SEM!
Timijana|8|False|Takoj!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Tip!
Zvok|2|False|Tap!
Timijana|3|True|Že zadnja tri leta siliš s tem „zadnjim preizkusom“!
Timijana|4|False|Ne bomo celo noč ždele tukaj!
Timijana|5|False|No?
Paprika|6|False|...
Kajenka|7|True|Naj bo.
Kajenka|8|True|Lahko dobi spričevalo.
Kajenka|9|False|Ampak samo z oceno „zadostno“.
Napis|10|True|Spričevalo
Napis|11|True|Kaosaha
Napis|12|False|-
Napis|14|False|Kumina
Napis|13|False|Kajenka
Napis|15|False|Timijana
Napis|16|False|~ za Papriko ~
Napis|17|False|Uradna čarovnica
Pripovedovalec|18|False|- KONEC -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 971 podpornikov!
Paprika|7|True|Obišči www.peppercarrot.com za več informacij!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|8|False|Hvalaaaa!
Paprika|2|True|Že veš?
Zasluge|1|False|20. december 2019 Piše in riše: David Revoy. Testni bralci: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance in Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 4.2.6appimage in Inkscape 0.92.3 na Kubuntu 18.04-LTS. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
