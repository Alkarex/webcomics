# Transcript of Pepper&Carrot Episode 31 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 31 mo'o lisri le nu damba

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|bu'u la .tenebrum. noi du le censa cmana po la kalsa
Sound|2|False|.lacp.lacp.
la .piper.|3|False|.penc.
Sound|4|False|.lacp.lacp.
Sound|5|False|.gunr.
la .piper.|6|False|ko lifri tu'a ti
Sound|8|False|.savr.
la .kaien.|9|False|nintadni ga'i
la .piper.|10|False|.ue
Sound|11|False|.darx.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|.lacp.
Sound|2|False|.fenr.
Sound|3|False|.gusk
la .piper.|4|False|.e'e bu'o cu'i
la .piper.|5|False|ca'e grava santa
Sound|6|False|.cfar.
Sound|7|False|.tikp.
Sound|8|False|.tikp.
Sound|9|False|.tikp.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|doi .karot.
la .piper.|2|False|ze by. moi se platu
la .piper.|4|False|me la .jypeg. va'e lo ka zabna
Sound|5|False|.punl.
Sound|3|False|.lenj.
la .kaien.|6|False|.ua nai
la .kaien.|10|False|.oi
Sound|7|True|G
Sound|8|True|Z|nowhitespace
Sound|9|False|Z|nowhitespace
la .piper.|11|False|no va'e lo ka zabna
la .kaien.|12|False|mo
la .kaien.|13|False|le'o
Writing|14|False|2019-12-20-E31P03_V15-romoi.jpg
Writing|15|False|na ka'e viska
Sound|16|False|.janli.|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .kaien.|1|False|fagri jbama co kantu
Sound|2|False|.lacp.
Sound|3|True|.j
Sound|4|True|b|nowhitespace
Sound|5|True|a|nowhitespace
Sound|6|False|m.|nowhitespace
Sound|7|True|.danm.
Sound|8|False|.danm.
Sound|9|False|.bumr.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .kaien.|1|False|ga'i
la .kaien.|2|True|rivbi fi lo ka klama lo sivni se cimde
la .kaien.|3|False|.i tai mabla bradi fa do
la .piper.|4|True|jitfa
la .piper.|5|False|.i ti vorme fo lo grava
Sound|6|False|.ui
la .piper.|7|False|.i ckire do lo ka klama le bartu doi .karot.
la .piper.|8|False|.i mi jinga doi .kaien. voi nobli
la .piper.|9|False|.i ca'e vlipa
la .piper.|10|False|ve farlu
Sound|12|False|.gunr.gunr.gunr.gunr.
Sound|11|False|.carn.
la .timian.|13|False|ko co'u

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .timian.|1|True|co'u ke'u
la .timian.|2|False|.i raunzu
Sound|3|False|.bikl.
Sound|4|False|.canc.
Sound|5|False|.tunt.
la .timian.|6|True|doi re do
la .timian.|7|True|fe ti
la .timian.|8|False|sutra klama

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|.darx.
Sound|2|False|.darx.
la .timian.|3|True|.i do cusku lu sa'u romoi cipra li'u ze'a le nanca be li ci
la .timian.|4|False|.i .ai mi'o na zvati ti ze'e le nicte
la .timian.|5|False|ja'e mo
la .piper.|6|False|.y.
la .kaien.|7|True|.i'a
la .kaien.|8|True|pajni le du'u ra certu .y.
la .kaien.|9|False|sa du'u ra su'e rau va'e lo ka certu
Writing|10|True|jaspu
Writing|11|True|fi
Writing|12|False|la kalsa
Writing|14|False|.kumin.
Writing|13|False|.kaien.
Writing|15|False|.timian.
Writing|16|False|fe la .piper.
Writing|17|False|lo ka ca'i te makfa
Narrator|18|False|.i fanmo

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|5|True|la'e la .piper. joi la .karot. cu fingubni gi'e se sarji le tcidu
la .piper.|3|True|.i sarji le nu finti le dei lisri kei fa le 971 da poi liste fa di'e
la .piper.|4|False|xu do djuno le du'u
la .piper.|7|True|.i do ji'a ka'e sidju .i je ba'e le cmene be do ba se ciska fi le tai papri
la .piper.|6|True|.i ko tcidu fi la'o .url. www.peppercarrot.com .url. ja'e le nu do djuno fi le tcila
la .piper.|8|False|.i mi'a pilno la'o gy.Patreon.gy. e la'o gy.Tipeee.gy. e la'o gy.PayPal.gy. e la'o gy.Liberapay.gy. e la'o gy. e lo drata
la .piper.|2|True|ki'e do
Credits|1|False|.i de'i li nanca bu 2019 masti bu 12 djedi bu 20 co'a gubni .i le pixra .e le lisri cu se finti la'o fy.David Revoy.fy. .i pu cipra tcidu lu'i le lisri poi pu nu'o bredi fa la'o gy.Arlo James Barnes.gy. e la'o gy.Craig Maloney.gy. e la'o gy.Martin Disch.gy. e la'o gy.Midgard.gy. e la'o gy.Nicolas Artance.gy. e la'o gy.Valvin.gy. .i le lojbo xe fanva zo'u fanva fa la gleki .i skicu la .erevas. noi munje zi'e noi finti fa la'o.fy.David Revoy.fy. .i ralju sidju fa la'o gy.Craig Maloney.gy. .i finti lei lisri be la .erevas. fa la'o gy.Craig Maloney.gy. e la'o gy.Nartance.gy. e la'o gy.Scribblemaniac.gy. e la'o gy.Valvin.gy. .i cikre le xe fanva fa la'o gy.Willem Sonke.gy. e la'o gy.Moini.gy. e la'o gy.Hali.gy. e la'o gy.CGand.gy. e la'o gy.Alex Gryson.gy. .i pilno le proga poi du la'o py. Krita 4.1.5~appimage.py. jo'u la'o py. Inkscape 0.92.3.py. jo'u la'o py.Kubuntu 18.04.1.py. i finkrali javni fa la'o jy. Creative Commons Attribution 4.0 .jy .i zoi .urli. www.peppercarrot.com .urli. judri
