﻿# Episode 19: Pollution

![cover of episode 19](https://www.peppercarrot.com/0_sources/ep19_Pollution/low-res/Pepper-and-Carrot_by-David-Revoy_E19.jpg)

## Comments from the author

Episode 19 is an episode about pollution, and also about the fighting of Pepper against the tradition. It's an answer to a previous episode the episode 12 'Autumn Clearout' (with the black hole of potions) and also an answer to the bad behavior of the witches of Chaosah "to bury everything".
I wanted to take an unusual point-of-view on this topic: about the embarassing 'history log' the pollution can leave in the layer of sediment soil. Also, in this episode, it's the first time Pepper really 'wins' against the three witches of Chaosah. But she doesn't win because of her effort themself (to embarass the witches) but because of the new situation.

A technical note: This episode was painted fully 'digital' with Krita. For inking, I used the preset 'Pencil 2B' (from my "8.1" brushkit) at size 7px, the style was made to match the episode 12; colorful with a focus on the precision of drawing and inking, and not so much on painting this time. It was longer, out of my comfort zone but I learned a lot.

From [Author's blog of episode 19](https://www.davidrevoy.com/article583/episode-19-pollution/show#comments)
