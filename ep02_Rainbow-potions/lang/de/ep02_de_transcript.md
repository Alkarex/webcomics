# Transcript of Pepper&Carrot Episode 02 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 2: Regenbogentränke

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|5|True|Gluck
Geräusch|6|True|Gluck
Geräusch|7|False|Gluck
Schrift|1|True|ACHTUNG
Schrift|3|False|GRUNDSTÜCK
Schrift|2|True|HEXEN
Schrift|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|7|False|klack|nowhitespace
Schrift|1|False|FEUER
Schrift|2|False|TIEFOZEAN
Schrift|3|False|VIOLE(N)T
Schrift|4|False|ULTRA BLAU
Schrift|5|False|PIN
Schrift|6|False|MESIN
Schrift|8|False|NATUR
Schrift|9|False|GELB
Schrift|10|False|ORANGE TOP
Schrift|11|False|FEUERTANZ
Schrift|12|False|TIEFOZEAN
Schrift|13|False|VIOLE(N)T
Schrift|14|False|ULTRA BLAU
Schrift|15|False|META PINK
Schrift|16|False|MAGENTA X
Schrift|17|False|KARMESIN

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|True|Blup
Geräusch|2|True|Blup
Geräusch|3|False|Blup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|True|Gluck
Geräusch|2|False|Gluck
Geräusch|3|True|Gluck
Geräusch|4|False|Gluck
Geräusch|5|True|Gluck
Geräusch|6|False|Gluck
Geräusch|20|False|m|nowhitespace
Geräusch|19|True|m|nowhitespace
Geräusch|18|True|M|nowhitespace
Geräusch|7|True|B|nowhitespace
Geräusch|8|True|u|nowhitespace
Geräusch|9|True|ä|nowhitespace
Geräusch|10|False|rgh!|nowhitespace
Geräusch|11|True|P|nowhitespace
Geräusch|12|True|l|nowhitespace
Geräusch|13|True|a|nowhitespace
Geräusch|14|True|t|nowhitespace
Geräusch|15|True|s|nowhitespace
Geräusch|16|True|c|nowhitespace
Geräusch|17|False|h|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|3|False|Blup
Geräusch|1|True|B
Geräusch|2|True|lup|nowhitespace
Geräusch|6|True|sch|nowhitespace
Geräusch|5|True|at|nowhitespace
Geräusch|4|True|p
Geräusch|9|True|sch|nowhitespace
Geräusch|8|True|at|nowhitespace
Geräusch|7|True|p
Geräusch|12|False|sch|nowhitespace
Geräusch|11|True|at|nowhitespace
Geräusch|10|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|True|Dieser Webcomic ist Open Source und wurde von 21 Förderern unterstützt via
Impressum|2|False|www.patreon.com/davidrevoy
Impressum|3|False|Vielen Dank an
Impressum|4|False|erstellt mit Krita auf GNU/Linux
