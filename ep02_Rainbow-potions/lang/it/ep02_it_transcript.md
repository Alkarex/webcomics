# Transcript of Pepper&Carrot Episode 02 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Episodio 2: Pozioni arcobaleno

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|5|True|Glup
Suono|6|True|Glup
Suono|7|False|Glup
Writing|1|True|WARNING
Writing|3|False|PROPERTY
Writing|2|True|WITCH
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|7|False|tok|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|1|True|Glup
Suono|2|True|Glup
Suono|3|False|Glup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|1|True|Gulp
Suono|2|False|Gulp
Suono|3|True|Gulp
Suono|4|False|Gulp
Suono|5|True|Gulp
Suono|6|False|Gulp
Suono|20|False|m|nowhitespace
Suono|19|True|m|nowhitespace
Suono|18|True|M|nowhitespace
Suono|7|True|S|nowhitespace
Suono|8|True|P|nowhitespace
Suono|9|True|l|nowhitespace
Suono|10|False|urp !|nowhitespace
Suono|11|True|S|nowhitespace
Suono|12|True|S|nowhitespace
Suono|13|True|S|nowhitespace
Suono|14|True|P|nowhitespace
Suono|15|True|l|nowhitespace
Suono|16|True|a|nowhitespace
Suono|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|1|True|B
Suono|2|True|lup|nowhitespace
Suono|3|True|B
Suono|4|False|lup|nowhitespace
Suono|7|True|up|nowhitespace
Suono|6|True|pl|nowhitespace
Suono|5|True|s
Suono|10|True|up|nowhitespace
Suono|9|True|pl|nowhitespace
Suono|8|True|s
Suono|13|False|up|nowhitespace
Suono|12|True|pl|nowhitespace
Suono|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crediti|1|True|Questo fumetto è open-source e questo episodio è stato finanziato dai miei 21 donatori onnatoriatori
Crediti|2|False|www.patreon.com/davidrevoy
Crediti|3|False|Un ringraziamento a
Crediti|4|False|creato con Krita su GNU/Linux
