# Transcript of Pepper&Carrot Episode 02 [mx]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 2: Pociones de Arcoíris

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|5|True|Glu
Sonido|6|True|Glu
Sonido|7|False|Glu
Writing|3|False|EMBRUJADA
Writing|2|True|PROPIEDAD
Writing|1|True|¡PELIGRO!
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|7|False|tok|nowhitespace
Writing|1|False|DANZA
Writing|2|False|OCÉANO PROFUNDO
Writing|3|False|VIOLE(N)TA
Writing|4|False|ULTRA AZUL
Writing|5|False|ROS
Writing|6|False|ARMESÍ
Writing|8|False|NATURALEZA
Writing|9|False|AMARILLO
Writing|10|False|NARANJA SUPERIOR
Writing|11|False|DANZA DE FUEGO
Writing|12|False|OCÉANO PROFUNDO
Writing|13|False|VIOLE(N)TA
Writing|14|False|ULTRA AZUL
Writing|15|False|META ROSA
Writing|16|False|MAGENTA X
Writing|17|False|CARMESÍ

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|Glu
Sonido|2|True|Glu
Sonido|3|False|Glu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|Gulp
Sonido|2|False|Gulp
Sonido|3|True|Gulp
Sonido|4|False|Gulp
Sonido|5|True|Gulp
Sonido|6|False|Gulp
Sonido|20|False|m|nowhitespace
Sonido|19|True|m|nowhitespace
Sonido|18|True|¡M|nowhitespace
Sonido|7|True|¡B|nowhitespace
Sonido|8|True|l|nowhitespace
Sonido|9|True|a|nowhitespace
Sonido|10|False|rgg !|nowhitespace
Sonido|11|True|S|nowhitespace
Sonido|12|True|S|nowhitespace
Sonido|13|True|S|nowhitespace
Sonido|14|True|P|nowhitespace
Sonido|15|True|l|nowhitespace
Sonido|16|True|o|nowhitespace
Sonido|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|G
Sonido|2|True|lub|nowhitespace
Sonido|3|True|G
Sonido|4|False|lub|nowhitespace
Sonido|7|True|plof
Sonido|6|True|lof|nowhitespace
Sonido|5|True|p
Sonido|10|False|lof|nowhitespace
Sonido|9|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|True|Esta tira cómica es de código libre. Este episodio ha tenido 21 contribuyentes en
Créditos|2|False|www.patreon.com/davidrevoy
Créditos|3|False|Gracias a :
Créditos|4|False|hecho con Krita en GNU/Linux-traducido con Inkscape en Archlinux
Créditos|5|False|Traducción al castellano : TheFaico adaptado para México por RJ Quiralta
