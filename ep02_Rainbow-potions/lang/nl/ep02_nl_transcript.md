# Transcript of Pepper&Carrot Episode 02 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 2: Regenboogdrankjes

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|5|True|Gloep
Geluid|6|True|Gloep
Geluid|7|False|Gloep
Geschrift|1|True|WAARSCHUWING
Geschrift|3|False|PRIVÉ-EIGENDOM
Geschrift|2|True|HEKS
Geschrift|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|7|False|tok|nowhitespace
Geschrift|1|False|VUUR
Geschrift|2|False|OCEAAN
Geschrift|3|False|VIOLET
Geschrift|4|False|INDIGO
Geschrift|5|False|RO
Geschrift|6|False|OBIJN
Geschrift|8|False|NATURE
Geschrift|9|False|YELLOW
Geschrift|10|False|ORANJE TOP
Geschrift|11|False|VUUR DANS
Geschrift|12|False|OCEAAN
Geschrift|13|False|VIOLET
Geschrift|14|False|INDIGO
Geschrift|15|False|META ROZE
Geschrift|16|False|MAGENTA X
Geschrift|17|False|ROBIJN

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|True|Gloep
Geluid|2|True|Gloep
Geluid|3|False|Gloep

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|True|Gulp
Geluid|2|False|Gulp
Geluid|3|True|Gulp
Geluid|4|False|Gulp
Geluid|5|True|Gulp
Geluid|6|False|Gulp
Geluid|21|False|...!|nowhitespace
Geluid|20|True|egh|nowhitespace
Geluid|19|True|w|nowhitespace
Geluid|18|True|B
Geluid|7|True|S
Geluid|8|True|P|nowhitespace
Geluid|9|True|l|nowhitespace
Geluid|10|False|urp!|nowhitespace
Geluid|11|True|S
Geluid|12|True|S|nowhitespace
Geluid|13|True|S|nowhitespace
Geluid|14|True|P|nowhitespace
Geluid|15|True|l|nowhitespace
Geluid|16|True|a|nowhitespace
Geluid|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|True|B
Geluid|2|True|lub|nowhitespace
Geluid|3|True|B
Geluid|4|False|lub|nowhitespace
Geluid|7|True|ut|nowhitespace
Geluid|6|True|pl|nowhitespace
Geluid|5|True|s
Geluid|10|True|ut|nowhitespace
Geluid|9|True|pl|nowhitespace
Geluid|8|True|s
Geluid|13|False|ut|nowhitespace
Geluid|12|True|pl|nowhitespace
Geluid|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|True|Deze webstrip is open-bron, en deze aflevering is gesponsord door 21 patronen
Aftiteling|2|False|www.patreon.com/davidrevoy
Aftiteling|3|False|Veel dank aan
Aftiteling|4|False|gemaakt met Krita op GNU/Linux
