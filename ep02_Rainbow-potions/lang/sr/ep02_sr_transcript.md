# Transcript of Pepper&Carrot Episode 02 [sr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Епизода 2: Дугини напици
<hidden>|2|False|пПП
<hidden>|3|False|Папричица
<hidden>|4|False|Шаргарепица

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Кло
Sound|6|True|Кло
Sound|7|False|Кло
Writing|1|True|УПОЗОРЕЊЕ
Writing|3|False|ВЛАСНИШТВО
Writing|2|True|ВЕШТИЧЈЕ
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|куц|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Кло
Sound|2|True|Кло
Sound|3|False|Кло

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|КЛо
Sound|2|False|Кло
Sound|3|True|Кло
Sound|4|False|Кло
Sound|5|True|Кло
Sound|6|False|Кло
Sound|20|False|м|nowhitespace
Sound|19|True|м|nowhitespace
Sound|18|True|М
Sound|7|True|П
Sound|8|True|љ|nowhitespace
Sound|9|True|у|nowhitespace
Sound|10|False|ц !|nowhitespace
Sound|11|True|Б
Sound|12|True|љ|nowhitespace
Sound|13|True|у|nowhitespace
Sound|14|False|ц|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|К
Sound|2|True|ло|nowhitespace
Sound|3|True|К
Sound|4|False|ло|nowhitespace
Sound|7|True|ап|nowhitespace
Sound|6|True|љ|nowhitespace
Sound|5|True|ш
Sound|10|True|ап|nowhitespace
Sound|9|True|љ|nowhitespace
Sound|8|True|ш
Sound|13|False|ап|nowhitespace
Sound|12|True|љ|nowhitespace
Sound|11|True|ш

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Oвај веб-стрип је отвореног кода и ову епизоду је финансирао 21 покровитељ на
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Захваљујем се
Credits|4|False|направлено са Krita-ом на GNU/Linux
