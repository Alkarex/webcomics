# Transcript of Pepper&Carrot Episode 02 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 2: Mavrični napoji

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|5|True|Glu
Zvok|6|True|Glu
Zvok|7|False|Glu
Napis|1|True|POZOR
Napis|3|False|ČAROVNICE
Napis|2|True|LASTNINA
Napis|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|7|False|tok|nowhitespace
Napis|1|False|OGNJ
Napis|2|False|PODVODNA
Napis|3|False|VIJOLIČNA
Napis|4|False|VELEMODRA
Napis|5|False|ROZ
Napis|6|False|LATNA
Napis|8|False|NARAVNA
Napis|9|False|SONČNA
Napis|10|False|POMARANČNA
Napis|11|False|OGNJENA
Napis|12|False|PODVODNA
Napis|13|False|VIJOLIČNA
Napis|14|False|VELEMODRA
Napis|15|False|META ROZA
Napis|16|False|MAGENTA X
Napis|17|False|ŠKRLATNA

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|True|Glu
Zvok|2|True|Glu
Zvok|3|False|Glu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|True|Glu
Zvok|2|False|Glu
Zvok|3|True|Glu
Zvok|4|False|Glu
Zvok|5|True|Glu
Zvok|6|False|Glu
Zvok|20|False|m|nowhitespace
Zvok|19|True|m|nowhitespace
Zvok|18|True|M|nowhitespace
Zvok|7|True|P|nowhitespace
Zvok|8|True|f|nowhitespace
Zvok|9|True|u|nowhitespace
Zvok|10|False|j !|nowhitespace
Zvok|11|True|B|nowhitespace
Zvok|12|True|l|nowhitespace
Zvok|13|True|j|nowhitespace
Zvok|14|True|a|nowhitespace
Zvok|15|True|a|nowhitespace
Zvok|16|True|a|nowhitespace
Zvok|17|False|k|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|True|B
Zvok|2|True|lup|nowhitespace
Zvok|3|True|B
Zvok|4|False|lup|nowhitespace
Zvok|7|True|op|nowhitespace
Zvok|6|True|l|nowhitespace
Zvok|5|True|š
Zvok|10|True|op|nowhitespace
Zvok|9|True|l|nowhitespace
Zvok|8|True|š
Zvok|13|False|op|nowhitespace
Zvok|12|True|l|nowhitespace
Zvok|11|True|š

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|True|Ta spletni strip je odprtokoden. To epizodo je financiralo 21 podpornikov prek
Zasluge|2|False|www.patreon.com/davidrevoy
Zasluge|3|False|Najlepša hvala,
Zasluge|4|False|Ustvarjeno s Krito na operacijskem sistemu GNU/Linux
