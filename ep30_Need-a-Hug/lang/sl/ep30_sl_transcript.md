# Transcript of Pepper&Carrot Episode 30 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 30: Iskanje pozornosti

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|- KONEC -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 973 podpornikov!
Paprika|7|True|Obišči www.peppercarrot.com za več informacij!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|8|False|Hvalaaaa!
Paprika|2|True|Že veš?
Zasluge|1|False|03. september 2019 Piše in riše: David Revoy. Testni bralci: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance in Valvin. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nicolas Artance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita/4.2~git branch in Inkscape 0.92.3 na Kubuntu 18.04.2. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
