# Transcript of Pepper&Carrot Episode 30 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 30a : Bezonas Brakumon

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rakontanto|1|False|- FINO -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon ĉi tie!
Pipro|3|True|Pepper&Carrot estas tute libera, malfermitkoda kaj subtenita danke al la mecenateco de siaj legantoj.
Pipro|4|False|Pri ĉi tiu rakonto, dankon al la 973 mecenantoj!
Pipro|7|True|Vidu www.peppercarrot.com por pli da informo!
Pipro|6|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ...kaj en multaj pli!
Pipro|8|False|Dankon!
Pipro|2|True|Ĉu vi sciis?
Atribuintaro|1|False|3a de septembro de 2019 Arto kaj scenaro: David Revoy. Beta-legantoj kaj skizantoj: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Esperanta versio Traduko: Jorge Maldonado Ventura. Fasono: Navi, Tirifto. Bazita sur la universo de Hereva Kreinto: David Revoy. Ĉefa fleganto: Craig Maloney. Verkistoj: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korektistoj: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programaro: Krita/4.2~git branch, Inkscape 0.92.3 en Kubuntu 18.04.2. Licenco: Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
