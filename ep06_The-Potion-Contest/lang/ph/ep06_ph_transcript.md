# Transcript of Pepper&Carrot Episode 06 [ph]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 6: Ang Potions Contest

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|[Hikab hikab] Nakatulog ako ng bukas ang bintana... nanaman...
Pepper|2|True|... ang ginaw ...
Pepper|3|False|... at bakit kita ko ang Komona mula dito sa bintana?
Pepper|4|False|KOMONA!
Pepper|5|False|Yung Potions Contest!
Pepper|6|True|PATAY! Nakatulugan ko yung ginagawa ko kagabi!
Pepper|9|True|... pero?
Pepper|10|False|anung nangyayari dito ?!?
Bird|12|False|cK?|nowhitespace
Bird|11|True|qua|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|True|Carrot!
Pepper|5|True|Oooo-
Pepper|7|True|Naisip mo din magdala ng potion, ang damit ko, ang balanggot ko...
Pepper|8|False|... tignan nga natin kung anung potion ang dinala mo...
Pepper|6|False|kaayY!
Pepper|4|False|Ang sweet naman ng pusa kong makulit!
Pepper|3|True|Awww!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|huwaaaaaAT ?!!
Mayor of Komona|3|False|Bilang Mayor ng Komona, Dinideklara ko... na ang Patimpalak ng Salamangka ... ay nagsimula na!
Mayor of Komona|4|False|Nagagalak ang Baryong ito na i-welcome ang ating apat na kalahok sa ating unang edisyon nitong munting patimpalak
Mayor of Komona|5|True|Please,
Mayor of Komona|7|True|Masigabong
Writing|2|False|Komona Potions Contest
Mayor of Komona|8|False|palakpakan :
Mayor of Komona|6|True|magbigay po tayo ng

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|All the way mula sa sikat na Technologist's Union, Galak nating i-welcome ang charming at creative na
Mayor of Komona|3|True|... wag nating kalimutan ang sarili nating witch dito sa Komona,
Mayor of Komona|5|True|... ang pangatlo nating kalahok na nanggaling sa himbingan ng mga buwan,
Mayor of Komona|7|True|... at panghuli, ang ating last participant, Mula sa gubat ng Squirrel's End,
Mayor of Komona|2|False|si Coriander !
Mayor of Komona|4|False|si Saffron !
Mayor of Komona|6|False|si Shichimi !
Mayor of Komona|8|False|si Pepper !
Mayor of Komona|9|True|Simulan na natin ang palaro!
Mayor of Komona|10|False|Ang mga boto ay dadaanin sa Kabugan ng Palakpak
Mayor of Komona|11|False|ang pang-una, ang demonstration ni Coriander
Coriander|13|False|Kamatayan? ... hindi na kailangan pang mangamba ... salamat sa aking
Coriander|14|True|... Potion of
Coriander|15|False|ZOMBIFICATION !
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|Ladies & Gentlemen...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|KAMANGHA MANGHA !
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|True|Heto na ang Potion
Saffron|17|True|... please, Wag muna kayong pumalakpak madlang people ng Komona !
Saffron|21|True|... ang Kamatayan
Saffron|19|False|KO
Saffron|25|False|ALTASOSIODAD !
Saffron|24|True|... Potion de
Saffron|23|False|Maglalaho ang buhok ng mga inggitera... isang patak lang ng aking ...
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|False|Clap
Mayor of Komona|42|False|Mukang yayaman na lahat ang lola niyo!
Mayor of Komona|41|True|Sossy! Bongga!
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Mayor of Komona|2|False|binuhay ni Coriander ang patay! ito na ata ang potion ng mga himala!
Saffron|20|True|Ang potion na hinihintay ninyong lahat, ang potion na magdudulot ng kamatayan sa mga tsismosang kapitbahay ...
Mayor of Komona|43|False|Hindi nagkakamali ang lakas ang mga palakpak. si Coriander ay talbog na!
Saffron|22|False|sa inggit!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|2|True|sa demonstration na iyon! Mukang mahihirapan ng
Shichimi|6|True|WAG!
Shichimi|7|True|Hindi pw... Masyadong pong delikado
Shichimi|8|False|SORRY PO!
Mayor of Komona|5|False|... ikaw na Shichimi, hinihintay ka naming lahat
Mayor of Komona|9|False|Ladies & Gentlemen mukhang si Shichimi ay umuurong na s-...
Saffron|10|False|akin na yan!
Saffron|11|False|... tumigil ka na sa pa-effect mo jan, you're spoiling the show
Saffron|12|True|Alam nating lahat na
Shichimi|15|False|!!!
Sound|16|False|BZZZIIOO
Shichimi|19|False|HiGANTENG BAKULAW
Shichimi|4|False|uh... hindi ko alam na kailanga palang magbigay ng demo
Shichimi|17|True|Naku po!!!
Shichimi|18|True|Ang potion ng
Mayor of Komona|1|True|Wala na sigurong tatalo pa
Mayor of Komona|3|False|kabugin ito ni Shichimi!
Saffron|20|False|Aaahh!!!!
Saffron|14|False|sa contest SO, kahit anu pang gawin ng potion mo ...
Saffron|13|True|ako na, ang nagwagi

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-Caaaaaaaawwww
Sound|2|False|BAM!
Pepper|3|True|... ooOh!! cool!
Pepper|5|False|... matatawa po siguro kayo sa potion ko kasi ...
Pepper|4|False|so, ako na po ba?
Mayor of Komona|6|True|Takbo na, bata!
Mayor of Komona|7|True|Tapos na
Mayor of Komona|9|False|takas na!
Mayor of Komona|8|True|ang palaro!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|True|... as usual,
Pepper|1|True|There you go ...
Pepper|4|True|At least alam ko na kung anung gamit ng "potion" mo Carrot,
Pepper|5|False|... pagkatapos nito umuwi na tayo ...
Pepper|7|True|IkAW!
Pepper|10|True|Gusto mong i-try yung potion ko? ...
Pepper|11|False|... hindi kamo?
Pepper|6|False|HOY !
Sound|9|False|CRACK !
Pepper|3|False|kapag kami na, aalisan na sila
Pepper|8|False|DAMBUHALANG-SUSIAL-NA-ZOMBING-TIKLING!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Yeah, basahin mo yung label, basahin mo...
Pepper|2|False|... hindi ko pagiisipan bago ko ibuhos to lahat sayo kung hindi ka lalayas sa Komona ngayun din !
Mayor of Komona|3|True|Dahil sa pagliligtas mo sa aming baryo mula sa tiyak na panganib
Mayor of Komona|4|False|Pinaparangalan namin ng Panalong tagumpay, si Pepper para sa kanyang Potion ng ... ??!!
Pepper|7|False|... ahm... ang totoo, hindi to potion ; wiwi-sample to ng pusa ko nung nagpa-checkup siya sa vet!
Pepper|6|True|... Haha! yep ...
Pepper|8|False|... magde-demo pa ba kami ?...
Narrator|9|False|Episode 6 : The Potion Contest
Narrator|10|False|WAKs.
Writing|5|False|50,000 Ko
Credits|11|False|March 2015 - Artwork and story by David Revoy - Translation by Paolo Abes

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free, open-source and sponsored thanks to the kind patronage of readers. For this episode, thank you to the 245 Patrons :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|You too can become a patron of Pepper&Carrot for the next episode :
Credits|7|False|Tools : This episode was 100% drawn with Free/Libre software Krita on Linux Mint
Credits|6|False|Open-source : all source files with layers and fonts, are available on the official site
Credits|5|False|License : Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
