# Transcript of Pepper&Carrot Episode 06 [sv]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 6: Elixrtävlingen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Å tusan, jag somnade med fönstret öppet, igen...
Pepper|2|True|...det är så blåsigt...
Pepper|3|False|... och varför kan jag inte se Komona i fönstret?
Pepper|4|False|KOMONA!
Pepper|5|False|Elixrtävlingen!
Pepper|6|True|Jag måste... måste somnat av misstag!
Pepper|9|True|... men?
Pepper|10|False|Var är jag ?!?
Bird|12|False|cK?|nowhitespace
Bird|11|True|qua|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Så söt du är som tar mig till tävlingen
Pepper|3|False|Fan-tas-tiskt !
Pepper|4|True|Du tog även med mitt elixir, mina kläder och min hatt...
Pepper|5|False|... nu ska vi se vilket elixir to tog med...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|VA ?!!
Mayor of Komona|3|False|Som Borgmästarer i Komona, förklarar jag elixirtävlen ... Öppnad!
Mayor of Komona|4|False|Vårt stad välkommnar inte färre än fyra häxor till denna, den första tävlingen.
Mayor of Komona|5|True|Snälla, en
Mayor of Komona|6|True|stor
Writing|2|False|Komona Potion Contest
Mayor of Komona|7|False|applåd :

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Klapp
Mayor of Komona|1|True|Hela vägen från det fantastiska tecknologgillet, det är en ära att välkomna den vackra och smarta
Mayor of Komona|3|True|... inte att förglömma, Komonas egna häxa,
Mayor of Komona|5|True|... Vår tredje deltagare kommer från landet där alla månar går ner,
Mayor of Komona|7|True|... och till sist, vår sista deltagare, från skogen vid Ekorrboda,
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|Låt tävlingen börja!
Mayor of Komona|10|False|Tävlingen kommer att avgöras med applådmätaren
Mayor of Komona|11|False|Först ut, Corianders demonstration
Coriander|13|False|... var inte rädd för döden mer, tack vare mitt ...
Coriander|15|False|ZOMBIFIERINGS ELIXIR !
Audience|16|True|Klapp
Audience|17|True|Klapp
Audience|18|True|Klapp
Audience|19|True|Klapp
Audience|20|True|Klapp
Audience|21|True|Klapp
Audience|22|True|Klapp
Audience|23|True|Klapp
Audience|24|True|Klapp
Audience|25|True|Klapp
Audience|26|True|Klapp
Audience|27|True|Klapp
Audience|28|True|Klapp
Coriander|12|False|Mina damer och herrar...
Coriander|14|True|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTISKT !
Audience|3|True|Klapp
Audience|4|True|Klapp
Audience|5|True|Klapp
Audience|6|True|Klapp
Audience|7|True|Klapp
Audience|8|True|Klapp
Audience|9|True|Klapp
Audience|10|True|Klapp
Audience|11|True|Klapp
Audience|12|True|Klapp
Audience|13|True|Klapp
Audience|14|True|Klapp
Audience|15|True|Klapp
Audience|16|False|Klapp
Saffron|18|True|för här är
Saffron|17|True|... men snälla, Komonas folk, spara era applåder !
Saffron|22|False|... göra dem avunsjuka!
Saffron|19|True|MITT
Saffron|25|False|GLAMOURELIXIR !
Saffron|24|True|...
Saffron|23|False|... allt detta är möjligt genom att använda en droppe av mitt ...
Audience|26|True|Klapp
Audience|27|True|Klapp
Audience|28|True|Klapp
Audience|29|True|Klapp
Audience|30|True|Klapp
Audience|31|True|Klapp
Audience|32|True|Klapp
Audience|33|True|Klapp
Audience|34|True|Klapp
Audience|35|True|Klapp
Audience|36|True|Klapp
Audience|37|True|Klapp
Audience|38|True|Klapp
Audience|39|True|Klapp
Audience|40|False|Klapp
Mayor of Komona|42|False|Detta elixir kan göra alla i Komona rika!
Mayor of Komona|41|True|Fantastiskt! Otroligt !
Audience|44|True|Klapp
Audience|45|True|Klapp
Audience|46|True|Klapp
Audience|47|True|Klapp
Audience|48|True|Klapp
Audience|49|True|Klapp
Audience|50|True|Klapp
Audience|51|True|Klapp
Audience|52|True|Klapp
Audience|53|True|Klapp
Audience|54|True|Klapp
Audience|55|True|Klapp
Audience|56|True|Klapp
Audience|57|True|Klapp
Audience|58|True|Klapp
Audience|59|True|Klapp
Audience|60|False|Klapp
Mayor of Komona|2|False|Coriander trotsar döden med detta mi-ra-ku-lösa elixir!
Saffron|21|True|Elixiret vi all har väntat på: det som kommer att förvåna era grannar ...
Mayor of Komona|43|False|Era applåder kan inte vara fel. Coriander har redan blivit utslagen.
Saffron|20|False|elexir

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Demonstrationen vi precis såg verkar vara svår för Shichimi att slå !
Shichimi|4|True|NEJ!
Shichimi|5|True|Jag kan inte, det är för farligt
Shichimi|6|False|TYVÄRR !
Mayor of Komona|3|False|... kom igen Shichimi, alla väntar på dig
Mayor of Komona|7|False|Det verkar, mina Damer och Herrar, som om Shichimi ger upp...
Saffron|8|False|Ge mig flaskan !
Saffron|9|False|... och sluta låtsas vara blyg, du förstör hela tävlingen
Saffron|10|False|Alla vet redan att jag vunnit tävlingen oavsett vad din dryck gör ...
Shichimi|11|False|!!!
Sound|12|False|BZZZIIOO
Shichimi|15|False|STORA MONSTER !
Shichimi|2|False|Jag... jag trodde inte vi skulle göra någon demonstration.
Shichimi|13|True|AKTA!!!
Shichimi|14|True|Det är ett elixir för

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-Caaaaaaaawwww
Sound|2|False|BAM!
Pepper|3|True|... heh, coolt !
Pepper|5|False|... mitt elixir kommer i alla fall att vara värt att par skratt för ....
Pepper|4|False|så är det min tur nu ?
Mayor of Komona|6|True|Spring !
Mayor of Komona|7|False|Tävlingen är över ! ... rädda dig själv!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... som vanligt, alla sticker när det är vår tur
Pepper|1|True|där ser du ...
Pepper|4|True|Jag har i allafall en aning om vad vårt elixir kan göra, Carrot
Pepper|5|False|...återställa allt och sen sticker vi hem!
Pepper|7|True|Du
Pepper|8|False|Övervuxna-glamour-zombie-pippi!
Pepper|10|False|Vill du prova ett sista elixir? ...
Pepper|11|False|... inte?
Pepper|6|False|HEY !
Sound|9|False|CRACK !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ja, läs etiketten, ordentligt ...
Pepper|2|False|... Jag kommer inte att tveka innan jag häller det över dig om du inte sticker från Komona på direkten !
Mayor of Komona|3|True|Eftersom hon räddade vår stad då den var i fara
Mayor of Komona|4|False|Belönar vi Pepper för hennes elixir ... ??!!
Pepper|7|False|... det var inte ett elixir, det var min katts urinprov från senaste besöket hos vetrinären!
Pepper|6|True|... Haha! jepp ...
Pepper|8|False|... inget demo då ?...
Narrator|9|False|Episode 6 : Elixirtävlingen
Narrator|10|False|FIN
Writing|5|False|50,000 Ko
Credits|11|False|March 2015 - Artwork and story by David Revoy - Translation by Mikael Olofsson

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free, open-source and sponsored thanks to the kind patronage of readers. For this episode, thank you to the 245 Patrons :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|You too can become a patron of Pepper&Carrot for the next episode :
Credits|7|False|Tools : This episode was 100% drawn with Free/Libre software Krita on Linux Mint
Credits|6|False|Open-source : all source files with layers and fonts, are available on the official site
Credits|5|False|License : Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
