# Transcript of Pepper&Carrot Episode 06 [kr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
제목|1|False|제6 화: 포션 대회

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|True|에이 씨, 또 창문을 열어놓고 잠들었나봐...
후추|2|True|...너무 바람이 많이 부네...
후추|3|False|...근데 창문 사이로 왜 코모나가 보이는 거지?
후추|4|False|코모나!
후추|5|False|포션 대회!
후추|6|False|내가 실수로... 실수로 잠들었나 봐!
후추|9|True|...근데...
후추|10|False|여기가 어디야?!?
새|12|False|꽤액?|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|False|!!!
후추|2|False|당근아! 이런 귀여운 것~ 나를 대회 장소까지 데려다주는 거야?
후추|3|False|좋-았-어 !
후추|4|True|내 옷이랑, 모자랑, 포션까지 가지고 왔네...
후추|5|False|... 어떤 포션을 가져왔는지 볼까...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|False|이게 뭐야 ?!!
코모나 시장|3|False|코모나 시장으로서, 포션 대회의 개최를... 선언합니다!
코모나 시장|4|False|첫째 포션 대회의 참가자로 4명의 마녀를 초대한 것을 영광스럽게 생각합니다
코모나 시장|5|True|큰
글자|2|False|코모나 포션 대회
코모나 시장|7|False|부탁드립니다!
코모나 시장|6|True|박수

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
코모나 시장|1|True|위대한 기술자 연합에서 온, 매혹적이고 천재적인
코모나 시장|3|True|... 우리 코모나 시의 마녀도 잊지 않았겠죠,
코모나 시장|5|True|... 세번째 참가자는 달이 지는 땅에서 온
코모나 시장|7|True|... 그리고 마지막 참가자, 다람쥐 꼬리 숲에서 온
코모나 시장|2|False|고수 !
코모나 시장|4|False|사프란 !
코모나 시장|6|False|칠미 !
코모나 시장|8|False|후추!
코모나 시장|9|True|이제 대회를 시작하겠습니다!
코모나 시장|10|False|박수 갈채를 가장 많이 받은 참가자가 우승합니다
코모나 시장|11|False|먼저, 고수 씨의 시범입니다.
고수|13|True|... 죽음이여 물렀거라, 소개합니다...
고수|15|False|좀비의 포션!
관중|16|True|짝짝
관중|17|True|짝짝
관중|18|True|짝짝
관중|19|True|짝짝
관중|20|True|짝짝
관중|21|True|짝짝
고수|12|False|신사 숙녀 여러분...
관중|22|True|짝짝
관중|23|True|짝짝
관중|24|True|짝짝
관중|25|True|짝짝
관중|26|True|짝짝
관중|27|True|짝짝
관중|28|True|짝짝
관중|29|False|짝짝
고수|14|True|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
코모나 시장|1|True|환상적입니다!
관중|12|True|짝짝
사프란|4|True|왜냐하면 여기
사프란|3|True|... 하지만 코모나 시민 여러분, 박수는 아껴두시죠 !
사프란|8|False|... 질투하게 만들 바로 그 포션!
사프란|11|False|귀족의 포션!
사프란|10|True|...
사프란|9|True|... 이 모든 것은 이 포션 한 방울만 있으면 됩니다. 소개합니다 ...
관중|13|True|짝짝
코모나 시장|41|False|이 포션으로 모든 코모나의 시민이 부자가 될 수 있겠군요!
코모나 시장|40|True|환상적이고 놀랍습니다!
관중|14|True|짝짝
관중|15|True|짝짝
관중|16|True|짝짝
관중|17|True|짝짝
코모나 시장|2|False|고수 씨는 이 놀-라-운 포션으로 죽음을 무찔렀군요!
사프란|7|True|여러분들이 기다려 왔던 진짜 포션입니다: 바로 당신의 이웃들을 놀라게 하고...
코모나 시장|42|False|여러분의 박수 소리가 증명해주는군요. 고수 씨는 벌써 탈락되었습니다
관중|18|True|짝짝
관중|19|True|짝짝
관중|20|True|짝짝
관중|21|True|짝짝
관중|22|True|짝짝
관중|23|True|짝짝
관중|24|True|짝짝
관중|25|True|짝짝
관중|26|True|짝짝
관중|27|True|짝짝
관중|28|True|짝짝
관중|33|True|짝짝
관중|34|False|짝짝
사프란|5|True|제
관중|43|True|짝짝
관중|44|True|짝짝
관중|45|True|짝짝
관중|46|True|짝짝
관중|47|True|짝짝
관중|48|True|짝짝
관중|49|True|짝짝
관중|50|True|짝짝
관중|51|True|짝짝
관중|52|True|짝짝
관중|53|True|짝짝
관중|54|True|짝짝
관중|55|True|짝짝
관중|56|True|짝짝
관중|57|True|짝짝
관중|58|True|짝짝
관중|59|True|짝짝
관중|60|True|짝짝
관중|61|True|짝짝
관중|62|True|짝짝
관중|63|True|짝짝
관중|64|True|짝짝
관중|65|True|짝짝
관중|66|True|짝짝
관중|67|True|짝짝
관중|68|True|짝짝
관중|69|True|짝짝
관중|70|True|짝짝
관중|71|False|짝짝
사프란|6|False|포션은

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
코모나 시장|1|False|방금 전 시범이 칠미 씨에겐 꽤 부담이 될 듯 합니다 !
칠미|4|True|안 돼요!
칠미|5|True|너무 위험해서 할 수 없어요
칠미|6|False|죄송합니다!
코모나 시장|3|False|... 칠미 씨, 어서 진행하시죠
코모나 시장|7|False|신사 숙녀 여러분, 칠미 씨가 기권을...
사프란|8|False|이리 내 봐요 !
사프란|9|False|... 그리고 수줍은 척 그만하시죠, 분위기가 흐려지잖아요
사프란|10|False|당신네 포션이 뭐가 됐든 이미 제가 승자라는 걸 모두가 알고 있거든요 ...
칠미|11|False|!!!
소리|12|False|부우우우우웅
칠미|15|False|거대한 괴물의 포션이에요!
칠미|2|False|시범 ... 시범을 보여야 할 거라고는 예상 못했는데...
칠미|13|True|조심해요!!!
칠미|14|True|이 포션은

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
새|1|False|까악-까악-까아아아아아아
소리|2|False|쾅!
후추|3|True|... 우와, 죽이네 !
후추|5|False|... 제 포션은 그래도 웃음거리 정도는 될 것 같네요. 왜 그러냐면 ...
후추|4|False|그럼 이제 제 차례인 건가요?
코모나 시장|6|True|그냥 도망가요!
코모나 시장|7|False|대회는 이제 끝났어요! ... 목숨부터 부지하세요!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|2|False|... 이번에도 우리 차례에 다들 떠나잖아
후추|1|True|이것 봐...
후추|4|True|그래도 네가 들고 온 "포션"을 어떻게 쓸 수 있을지는 알 것 같아, 당근아
후추|5|False|... 어서 이 사태를 해결하고 집으로 가자!
후추|10|True|내 마지막 포션 한 번 먹어 볼래? ...
후추|11|False|... 그래, 싫지 ?
후추|6|False|야!
소리|9|False|쩌저적!
후추|8|False|초대형-귀족-좀비-카나리아야!
후추|7|True|이

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|True|그래, 라벨을 한 번 읽어 봐, 꼼꼼히...
후추|2|False|... 지금 코모나를 당장 뜨지 않으면 지금 이걸 너한테 뿌려버릴거야!
코모나 시장|3|True|위험에 처한 저희 도시를 구한 공로로
코모나 시장|4|False|후추 씨에게 대상을 수여합니다. 무슨 포션을 쓴 거죠 ??!!
후추|7|False|... 흠... 사실, 포션은 아니에요; 저번에 제 고양이가 동물병원에 갔을 때 채취한 소변 시료에요!
후추|6|True|... 하하! 예 ...
후추|8|False|... 그럼 시범은 보일 필요 없겠죠 ?...
해설|9|False|제 6화 : 포션 대회
해설|10|False|끝
글자|5|False|50,000 콤
크레딧|11|False|2015년 3월 - 그림 및 이야기 : David Revoy - 한글 번역 : Shikamaru Yamamoto, Jihoon Kim

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
크레딧|1|False|후추와 당근은 완전히 자유 오픈소스이고 독자들의 소중한 후원을 받아 운영하고 있습니다. 이번 화에 도움을 주신 245명의 후원자분께 감사의 말씀 드립니다:
크레딧|4|False|https://www.patreon.com/davidrevoy
크레딧|3|False|여러분도 후추와 당근의 다음 화의 후원자가 될 수 있습니다 :
크레딧|7|False|도구 : 이번 화는 100% 무료/자유 소프트웨어로 제작되었습니다 리눅스 민트에서 크리타 사용
크레딧|6|False|오픈 소스 : 모든 레이어 소스 파일과 폰트는 공식 홈페이지에서 다운로드 가능
크레딧|5|False|라이선스 : 크리에이티브 커먼즈 저작자표시 수정, 재배포, 판매 등 가능
크레딧|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
