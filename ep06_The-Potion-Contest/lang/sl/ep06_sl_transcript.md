# Transcript of Pepper&Carrot Episode 06 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 6: Tekmovanje v napojih

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Ah, šment! Spet sem zaspala pri odprtem oknu.
Paprika|2|True|Kakšen prepih.
Paprika|3|False|In kako to, da vidim Komono skozi okno?
Paprika|4|False|KOMONA!
Paprika|5|False|Tekmovanje v napojih!
Paprika|6|False|Po nesreči sem zaspala!
Paprika|7|True|Ampak …
Paprika|8|False|Kje pa sem?!?
Ptica|10|False|ga?|nowhitespace
Ptica|9|True|ga|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|!!!
Paprika|2|False|Korenček! Kako lepo, da si me pripeljal na tekmovanje!
Paprika|3|False|En-krat-no!
Paprika|4|True|Spomnil si se celo na napoj, moja oblačila in klobuk!
Paprika|5|False|Da vidimo, kateri napoj si našel …

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|KAJ?!!
Župan Komone|3|False|Kot župan Komone razglašam, da se tekmovanje v napojih … začenja!
Župan Komone|4|False|Našemu mestu je v veliko čast, da so na prvo prireditev te vrste k nam z vseh vetrov prišle kar štiri čarovnice!
Župan Komone|5|True|En
Župan Komone|6|True|velik
Napis|2|False|Komonsko tekmovanje v napojih
Župan Komone|7|False|aplavz za naše tekmovalke!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Občinstvo|29|False|Plosk
Župan Komone|1|True|Pripotovala je iz daljne Tehnološke zaveze. V čast nam je gostiti osupljivo in veleumno
Župan Komone|3|True|Ne pozabimo naše domače komonske čarovnice,
Župan Komone|5|True|Naša tretja tekmovalka prihaja iz dežele zahajajočih lun,
Župan Komone|7|True|In nazadnje še tekmovalka iz gozda pri Veveričjem kotu,
Župan Komone|2|False|Koriandriko!
Župan Komone|4|False|Žafranke!
Župan Komone|6|False|Šičimi!
Župan Komone|8|False|Paprika!
Župan Komone|9|True|Naj se tekmovanje prične!
Župan Komone|10|False|Uspešnost napoja bomo merili z aplavzomerom.
Župan Komone|11|False|Prva bo svoj napoj predstavila Koriandrika.
Koriandrika|13|False|ne bojte se več smrti, saj vam predstavljam
Koriandrika|14|True|svoj inovativni napoj
Koriandrika|15|False|ZOMBIFIKACIJE!
Občinstvo|16|True|Plosk
Občinstvo|17|True|Plosk
Občinstvo|18|True|Plosk
Občinstvo|19|True|Plosk
Občinstvo|20|True|Plosk
Občinstvo|21|True|Plosk
Občinstvo|22|True|Plosk
Občinstvo|23|True|Plosk
Občinstvo|24|True|Plosk
Občinstvo|25|True|Plosk
Občinstvo|26|True|Plosk
Občinstvo|27|True|Plosk
Občinstvo|28|True|Plosk
Koriandrika|12|False|Dame in gospodje,

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|1|False|IZJEMNO!
Občinstvo|3|True|Plosk
Občinstvo|4|True|Plosk
Občinstvo|5|True|Plosk
Občinstvo|6|True|Plosk
Občinstvo|7|True|Plosk
Občinstvo|8|True|Plosk
Občinstvo|9|True|Plosk
Občinstvo|10|True|Plosk
Občinstvo|11|True|Plosk
Občinstvo|12|True|Plosk
Občinstvo|13|True|Plosk
Občinstvo|14|True|Plosk
Občinstvo|15|True|Plosk
Občinstvo|16|False|Plosk
Žafranka|18|True|Ker tukaj je
Žafranka|17|True|Prosim, no, prihranite še aplavz, Komonci!
Žafranka|22|False|… in jih napolnil z zavistjo!
Žafranka|19|True|MOJ
Žafranka|25|False|RAZKOŠJA!
Žafranka|24|True|…napoja
Žafranka|23|False|Potrebujete le kapljico mojega …
Občinstvo|26|True|Plosk
Občinstvo|27|True|Plosk
Občinstvo|28|True|Plosk
Občinstvo|29|True|Plosk
Občinstvo|30|True|Plosk
Občinstvo|31|True|Plosk
Občinstvo|32|True|Plosk
Občinstvo|33|True|Plosk
Občinstvo|34|True|Plosk
Občinstvo|35|True|Plosk
Občinstvo|36|True|Plosk
Občinstvo|37|True|Plosk
Občinstvo|38|True|Plosk
Občinstvo|39|True|Plosk
Občinstvo|40|False|Plosk
Župan Komone|42|False|Ta napoj bi lahko prinesel Komoni neverjetno bogastvo!
Župan Komone|41|True|Imenitno! Enkratno!
Občinstvo|44|True|Plosk
Občinstvo|45|True|Plosk
Občinstvo|46|True|Plosk
Občinstvo|47|True|Plosk
Občinstvo|48|True|Plosk
Občinstvo|49|True|Plosk
Občinstvo|50|True|Plosk
Občinstvo|51|True|Plosk
Občinstvo|52|True|Plosk
Občinstvo|53|True|Plosk
Občinstvo|54|True|Plosk
Občinstvo|55|True|Plosk
Občinstvo|56|True|Plosk
Občinstvo|57|True|Plosk
Občinstvo|58|True|Plosk
Občinstvo|59|True|Plosk
Občinstvo|60|False|Plosk
Župan Komone|2|False|Koriandrika kljubuje smrti sami s tem ču-do-del-nim napojem!
Žafranka|21|True|To je napoj, na katerega ste čakali. Napoj, ki bo osupnil vaše sosede …
Župan Komone|43|False|Vaš aplavz se ne moti. Koriandrika je že izločena.
Žafranka|20|False|napoj!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|1|False|Šičimi bo stežka premagala predhodnico!
Šičimi|4|True|NE!
Šičimi|5|True|Ne morem, prenevarno je.
Šičimi|6|False|OPROSTITE!
Župan Komone|3|False|Dajmo, Šičimi, vsi čakajo.
Župan Komone|7|False|Kaže, dame in gospodje, da Šičimi odstopa …
Žafranka|8|False|Daj sem!
Žafranka|9|False|In nehaj se delati plašno, samo zabavo kvariš.
Žafranka|10|False|Tako ali tako sem že zmagala, pa naj tvoj napoj dela, kar hoče.
Šičimi|11|False|!!!
Zvok|12|False|ZZZUUUP
Šičimi|15|False|ORJAŠKE POŠASTI!
Šičimi|2|False|N-nisem vedela, da jih moramo demonstrirati …
Šičimi|13|True|PAZI!!!
Šičimi|14|True|Napoj ustvari

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ptica|1|False|KRA-KRA-Kraaaaaaaaaaa
Zvok|2|False|BAM!
Paprika|3|True|Ha, fino!
Paprika|5|False|Moj napoj vas bo vsaj pošteno nasmejal, ker…
Paprika|4|False|Sem zdaj jaz na vrsti?
Župan Komone|6|True|Teci, butara!
Župan Komone|7|False|Tekmovanja je konec!Reši se, kdor se more!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|2|False|Kot vedno vsi izginejo ravno, ko sva midva na vrsti.
Paprika|1|True|Vidiš to!
Paprika|4|True|Zdaj vsaj vem, kaj bova storila s tvojim „napojem“, Korenček.
Paprika|5|False|Vse bova spravila v red in šla domov!
Paprika|7|True|Ti
Paprika|8|False|ogromna-gizdalinska-zombi-kanarčica!
Paprika|10|False|Bi rada poskusila še zadnji napoj?
Paprika|11|False|Ti ni do tega, kaj?
Paprika|6|False|HEJ!
Zvok|9|False|KRAAK!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Tako, tako, pozorno preberi etiketo!
Paprika|2|False|Če se takoj ne pobereš iz Komone, bom tole brez pomisleka polila po tebi!
Župan Komone|3|True|Ker je rešila naše mesto pred nevarnostjo,
Župan Komone|4|False|pripada prvo mesto Papriki za njen napoj …??!!
Paprika|7|False|Em… V bistvu ni zares napoj. To je vzorec urina mojega mačka z zadnjega obiska pri veterinarju!
Paprika|6|True|Haha! Aha.
Paprika|8|False|Ni treba demonstrirati, kajne?
Pripovedovalec|9|False|Epizoda 6: Tekmovanje v napojih
Pripovedovalec|10|False|KONEC
Napis|5|False|50 000 ko.
Zasluge|11|False|Marec 2015 - Napisal in narisal David Revoy - Prevedla Andrej Ficko in Gorazd Gorup

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 245 bralcev:
Zasluge|4|False|https://www.patreon.com/davidrevoy
Zasluge|3|False|Tudi ti lahko postaneš denarni/a podpornik/ca za naslednjo epizodo stripa:
Zasluge|7|False|Orodja: Ta epizoda je bila narisana s prostim programom Krita na operacijskem sistemu Linux Mint
Zasluge|6|False|Prost dostop: na domačem spletišču so na voljo vse izvorne datoteke grafik in pisav
Zasluge|5|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Dovoljeno spreminjanje, deljenje, prodajanje itd.
Zasluge|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
