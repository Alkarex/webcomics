# Transcript of Pepper&Carrot Episode 06 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 6: Eliksirkonkurrencen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Pokkers! Jeg har igen lagt mig til at sove uden at lukke vinduet…
Pepper|2|True|…men sikke en vind!
Pepper|3|False|… og hvorfor kan jeg se Komona ud af vinduet?
Pepper|4|False|KOMONA!
Pepper|5|False|Eliksir-konkurrencen!
Pepper|6|True|Åh nej… Jeg faldt vist i søvn!
Pepper|8|False|... men?
Pepper|11|False|Hvor er jeg ?!?
Fugl|10|False|P?|nowhitespace
Fugl|9|True|ra|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Hvor er du sød at have tænkt på at tage mig hen til konkurrencen!
Pepper|3|False|Fan-tas-tisk!
Pepper|4|True|Du har endda tænkt på at tage en eliksir, mit tøj, og min hat...
Pepper|5|False|... lad os se hvilken eliksir du har valgt...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|HVAD?!!
Komonas Borgmester|3|False|Som Komonas borgmester, erklærer jeg hermed eliksirkonkurrencen for åben!
Komonas Borgmester|4|False|Vores by er meget stolt af at byde velkommen til ikke mindre end fire hekse ved denne premiere.
Komonas Borgmester|5|True|Lad os give dem en
Komonas Borgmester|6|True|stor
Skrift|2|False|Komonas eliksirkonkurrence
Komonas Borgmester|7|False|hånd:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|29|False|Klap
Komonas Borgmester|1|True|Helt fra det fantastiske Teknologland, er det en ære at byde velkommen til den charmerende og geniale
Komonas Borgmester|3|True|Uden at glemme vores lokale pige, Komonas egen heks,
Komonas Borgmester|5|True|Vores tredje deltager kommer fra Månenedgangenes land:
Komonas Borgmester|7|True|Og endelig, vores sidste deltager, som kommer fra skoven ved Egerns Ende:
Komonas Borgmester|2|False|Koriander!
Komonas Borgmester|4|False|Safran!
Komonas Borgmester|6|False|Shichimi!
Komonas Borgmester|8|False|Pepper!
Komonas Borgmester|9|True|Lad konkurrencen begynde!
Komonas Borgmester|10|False|Stemmerne vil bliver talt op af klap-o-metereret
Komonas Borgmester|11|False|Først skal vi se Korianders demonstration
Koriander|12|False|Mine damer og herrer...
Koriander|13|True|... frygt ikke længere døden takket være...
Koriander|14|True|... min
Koriander|15|True|ZOMBIFICERINGS-
Publikum|17|True|Klap
Publikum|18|True|Klap
Publikum|19|True|Klap
Publikum|20|True|Klap
Publikum|21|True|Klap
Publikum|22|True|Klap
Publikum|23|True|Klap
Publikum|24|True|Klap
Publikum|25|True|Klap
Publikum|26|True|Klap
Publikum|27|True|Klap
Publikum|28|True|Klap
Publikum|30|False|Klap
Koriander|16|False|eliksir!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|1|False|FANTASTISK!
Publikum|3|True|Koriander trodser selve døden med denne mi-ra-ku-lø-se eliksir!
Publikum|4|True|Klap
Publikum|5|True|Klap
Publikum|6|True|Klap
Publikum|7|True|Klap
Publikum|8|True|Klap
Publikum|9|True|Klap
Publikum|10|True|Klap
Publikum|11|True|Klap
Publikum|12|True|Klap
Publikum|13|True|Klap
Publikum|14|True|Klap
Publikum|15|True|Klap
Publikum|16|False|Klap
Safran|18|True|Klap
Safran|17|True|For her er
Safran|22|False|... men spar på jeres klapsalver, Komonas folk!
Safran|19|True|Den eliksir I alle har ventet på: én, der kan imponere alle jeres naboer...
Safran|25|False|... gøre dem jaloux!
Safran|24|True|MIN
Safran|23|False|OVERKLASSES-ELIKSIR!
Publikum|26|True|Alt dette er nu muligt ved at anvende en enkelt dråbe af min...
Publikum|27|True|Klap
Publikum|28|True|Klap
Publikum|29|True|Klap
Publikum|30|True|Klap
Publikum|31|True|Klap
Publikum|32|True|Klap
Publikum|33|True|Klap
Publikum|34|True|Klap
Publikum|35|True|Klap
Publikum|36|True|Klap
Publikum|37|True|Klap
Publikum|38|True|Klap
Publikum|39|True|Klap
Publikum|40|False|Klap
Komonas Borgmester|42|False|Klap
Komonas Borgmester|41|True|Denne eliksir kan gøre hele Komona rig!
Publikum|44|True|Fantastisk! Utroligt!
Publikum|45|True|Klap
Publikum|46|True|Klap
Publikum|47|True|Klap
Publikum|48|True|Klap
Publikum|49|True|Klap
Publikum|50|True|Klap
Publikum|51|True|Klap
Publikum|52|True|Klap
Publikum|53|True|Klap
Publikum|54|True|Klap
Publikum|55|True|Klap
Publikum|56|True|Klap
Publikum|57|True|Klap
Publikum|58|True|Klap
Publikum|59|True|Klap
Publikum|60|False|Klap
Komonas Borgmester|2|False|Klap
Safran|21|True|Jeres klapsalver lyver ikke; Koriander er allerede udgået.
Komonas Borgmester|43|False|eliksir

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|1|False|Det bliver svært for Shichimi at slå!
Shichimi|4|True|NEJ!
Shichimi|5|True|Det kan jeg ikke, for farligt
Shichimi|6|False|UNDSKYLD!
Komonas Borgmester|3|False|... Kom nu Shichimi, alle venter på dig
Komonas Borgmester|7|False|Det ser ud til, mine damer og herrer, at Shichimi trækker sig...
Safran|8|False|Giv mig det dér!
Safran|9|False|... og stop med at være så genert og ødelægge showet
Safran|10|False|Alle ved allerede, at jeg har vundet konkurrencen, så uanset hvad din eliksir kan...
Shichimi|11|False|!!!
Lyd|12|False|BZZZIIOO
Shichimi|15|False|KÆMPEMONSTRE!
Shichimi|2|False|Jeg... Jeg vidste ikke, at vi skulle lave en demonstration
Shichimi|13|True|PAS PÅ!!!
Shichimi|14|True|Eliksiren skaber

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fugl|1|False|Kra-Kra-Kraaaaaaaaaa
Lyd|2|False|BUM!
Pepper|3|True|... Øhh, cool!
Pepper|5|False|... Min eliksir bør i det mindste få jer til at grine, fordi...
Pepper|4|False|Så er det min tur, ikke?
Komonas Borgmester|6|True|Forsvind!
Komonas Borgmester|7|False|Konkurencen er slut! ... løb for livet!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... som altid forsvinder alle lige når det er vores tur
Pepper|1|True|Se nu dér:
Pepper|4|True|Nå men jeg tror, jeg har en idé til, hvad vi skal bruge din ”eliksir” til, Carrot
Pepper|5|False|... lad os rydde op her og så gå hjem!
Pepper|7|True|Din
Pepper|8|False|Kæmpe-snob-zombie-pippip!
Pepper|10|False|Kunne du tænke dig at prøve den sidste eliksir?...
Pepper|11|False|... ikke rigtig, hva'?
Pepper|6|False|HEY!
Lyd|9|False|KRAKK!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ja, læs du etiketten grundigt...
Pepper|2|False|... Jeg vil ikke tøve med at hælde den på dig, hvis du ikke forlader Komona med det samme!
Komonas Borgmester|3|True|Fordi hun reddede vores by fra en katastrofe,
Komonas Borgmester|4|False|giver vi førstepladsen til Pepper og hendes eliksir som kan... ??!!
Pepper|7|False|... øh... faktisk er det ikke rigtig en eliksir; det er min kats urinprøve til dens tjek hos dyrlægen!
Pepper|6|True|... Haha! ja...
Pepper|8|False|... ikke nogen demo, OK?...
Fortæller|9|False|Episode 6: Eliksirkonkurrencen
Fortæller|10|False|Slut
Skrift|5|False|50 000 Ko
Credits|11|False|March 2015 - Design og manuskript: David Revoy; Rettelser: Aurélien Gâteau; Oversættelse: Alexandre og Emmiline Alapetite

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 245 tilhængere:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Støt næste episode af Pepper&Carrot; hver en krone gør en forskel:
Credits|7|False|Værktøj: Denne episode er 100% designet med fri software: Krita og Inkscape på Linux Mint
Credits|6|False|Open source: al kildematerialet, skrifttyper, og lagdelte filer kan downloades i høj opløsning fra den officielle hjemmeside
Credits|5|False|Licens: Creative Commons Kreditering Du kan dele og tilpasse materialet til alle formål, også kommercielle
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
