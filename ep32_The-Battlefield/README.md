# Episode 32: The Battlefield

![cover of episode 32](https://www.peppercarrot.com/0_sources/ep32_The-Battlefield/low-res/Pepper-and-Carrot_by-David-Revoy_E32.jpg)

## Comments from the author

For the general theme, it's a new arc about Pepper trying to get new a professional career with her new diploma. This episode is about getting the right appearance for a work position and how other evaluate our skill and experience based on our appearance.

From [Beta-reading Forum of episode 32](https://framagit.org/peppercarrot/webcomics/-/issues/147)
