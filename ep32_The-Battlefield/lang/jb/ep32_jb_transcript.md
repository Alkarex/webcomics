# Transcript of Pepper&Carrot Episode 32 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 32 mo'o stuzi le nu jamna
<hidden>|2|False|la .piper.
<hidden>|3|False|la .karot.
<hidden>|4|False|joi

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|doi vipsi
King|2|False|xu gau do su'o te makfa co'a cmima le se bende be fi mi
Officer|3|True|jetnu doi jatna
Officer|4|False|.i le te makfa cu sanli ne'a do
King|5|False|.y.
la .piper.|6|True|coi
la .piper.|7|False|mi'e .piper. i
King|8|False|mo
King|9|True|bebna
King|10|False|.i ki'u ma gau do ti noi verba cu cmima le se bende .i .ei su'o jetnu sidju fi lo ka jamna

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|ko fraxu mi
la .piper.|2|True|.i mi je'a me le te makfa pe la kalsa
la .piper.|3|False|.i ji'a mi se jaspu ti fo .y.
Writing|4|True|jaspu
Writing|5|True|fi
Writing|6|False|la kalsa
Writing|8|True|.kaien.
Writing|9|False|.kumin.
Writing|7|True|.timian.
Writing|10|False|fe la .piper.
King|11|False|smaji
King|12|True|.i no da prali mi lo nu lo verba cu bilni
King|13|False|.i ko di'a zvati le zdani be do gi'e kelci le danlu
Sound|14|False|.bikl.
Army|15|True|.u'i .u'i .u'i .u'i .u'i
Army|16|True|.u'i .u'i .u'i .u'i .u'i
Army|17|True|.u'i .u'i .u'i .u'i .u'i
Army|18|False|.u'i .u'i .u'i .u'i

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|.ia nai
la .piper.|2|False|mi tadni ze'a le nanca be li so'i .i ku'i no da junri tu'a mi ki'u
la .piper.|3|False|le nu mi simlu lo ka na'e certu vau va'o nu catlu mi
Sound|4|False|.canc.
la .piper.|5|False|.karot.|nowhitespace
Sound|6|False|.darx.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|be'u nai ro'o pei doi la .karot.
la .piper.|2|True|.i .a'o pu kukte
la .piper.|3|False|.i do simlu lo ka jai se terpa
la .piper.|4|False|.i do ba'e simlu
la .piper.|5|True|.i le li'a jvinu
la .piper.|6|False|.porp.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|ju'i do
la .piper.|2|False|.i ti'e do pu sisku lo ka jetnu te makfa

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|.i zu'u nai .ei na pilno ra
King|2|False|.i la'a ti cu jai se jdima lo du'enzu
Writing|3|False|to le lisri nu'o fanmo toi

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|5|True|la'e la .piper. joi la .karot. cu fingubni gi'e se sarji le tcidu
la .piper.|3|True|.i sarji le nu finti le dei lisri kei fa le 1121 da poi liste fa di'e
la .piper.|4|False|xu do djuno le du'u
la .piper.|7|True|.i do ji'a ka'e sidju .i je ba'e le cmene be do ba se ciska fi le tai papri
la .piper.|6|True|.i ko tcidu fi la'o .url. www.peppercarrot.com .url. ja'e le nu do djuno fi le tcila
la .piper.|8|False|.i mi'a pilno la'o gy.Patreon.gy. e la'o gy.Tipeee.gy. e la'o gy.PayPal.gy. e la'o gy.Liberapay.gy. e la'o gy. e lo drata
la .piper.|2|True|ki'e do
Credits|1|False|.i de'i li nanca bu 2020 masti bu 3 djedi bu 31 co'a gubni .i le pixra .e le lisri cu se finti la'o fy.David Revoy.fy. .i pu cipra tcidu lu'i le lisri poi pu nu'o bredi fa la'o gy.Arlo James Barnes.gy. e la'o gy.Craig Maloney.gy. e la'o gy.Martin Disch.gy. e la'o gy.Midgard.gy. e la'o gy.Nicolas Artance.gy. e la'o gy.Valvin.gy. e la'o gy.Parnikkapore.gy. e la'o gy.Stephen Paul Weber.gy.e la'o gy.Vejvej.gy. .i le lojbo xe fanva zo'u fanva fa la gleki .i skicu la .erevas. noi munje zi'e noi finti fa la'o.fy.David Revoy.fy. .i ralju sidju fa la'o gy.Craig Maloney.gy. .i finti lei lisri be la .erevas. fa la'o gy.Craig Maloney.gy. e la'o gy.Nartance.gy. e la'o gy.Scribblemaniac.gy. e la'o gy.Valvin.gy. .i cikre le xe fanva fa la'o gy.Willem Sonke.gy. e la'o gy.Moini.gy. e la'o gy.Hali.gy. e la'o gy.CGand.gy. e la'o gy.Alex Gryson.gy. .i pilno le proga poi du la'o py. Krita 4.2.9-beta.py. jo'u la'o py. Inkscape 0.92.3.py. jo'u la'o py.Kubuntu 19.10.py. i finkrali javni fa la'o jy. Creative Commons Attribution 4.0 .jy .i zoi .urli. www.peppercarrot.com .urli. judri
