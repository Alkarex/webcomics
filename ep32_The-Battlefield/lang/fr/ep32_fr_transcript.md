# Transcript of Pepper&Carrot Episode 32 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 32 : Le champ de bataille

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|True|Officier !
Roi|2|False|Avez-vous engagé une sorcière, comme je l'ai demandé ?
Officier|3|True|Oui, mon roi !
Officier|4|False|Elle est juste là, à côté de vous.
Roi|5|False|... ?
Pepper|6|True|Salut !
Pepper|7|False|Je m'appelle Pepp...
Roi|8|False|?!!
Roi|9|True|IMBÉCILE !!!
Roi|10|True|Pourquoi avez-vous engagé cette gamine ?!
Roi|11|False|J'ai besoin d'une vraie sorcière de combat !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Euh, pardon ?!
Pepper|2|True|Je suis une vraie Sorcière de Chaosah.
Pepper|3|False|J'ai même un diplôme qui...
Écriture|4|True|Diplôme
Écriture|5|True|de
Écriture|6|False|Chaosah
Écriture|9|False|Cayenne
Écriture|8|True|Cumin
Écriture|7|True|Thym
Écriture|10|False|~ pour Pepper ~
Roi|11|False|SILENCE !
Roi|12|True|Je n'ai que faire d'une gamine dans cette armée.
Roi|13|False|Rentre chez toi jouer avec tes poupées.
Son|14|False|Slap !
Armée|15|True|HAHA HA HA !
Armée|16|True|HAHA HA HA !
Armée|17|True|HAHA HA HA !
Armée|18|False|HA HA HAHA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|J'y crois pas !
Pepper|2|False|Toutes ces années d'études et personne ne me prend au sérieux sous prétexte que...
Pepper|3|False|...je n'ai pas assez d'expérience !
Son|4|False|POUF !!
Pepper|5|False|CARROT !
Son|6|False|PAF !!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sérieusement, Carrot ?
Pepper|2|True|J'espère que ce repas en valait la peine.
Pepper|3|False|T'as l'air hideux...
Pepper|4|False|... T'as l'air...
Pepper|5|True|Les apparences !
Pepper|6|False|Bien sûr !
Son|7|False|Crac !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HÉ !
Pepper|2|False|J'ai entendu dire que vous cherchiez une VRAIE SORCIÈRE ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|True|Tout compte fait, rappelez la gamine.
Roi|2|False|Celle-ci doit coûter bien trop cher.
Écriture|3|False|À SUIVRE...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 1121 mécènes !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|8|False|Merci !
Pepper|2|True|Le saviez-vous ?
Crédits|1|False|Le 31 mars 2020 Art & scénario : David Revoy. Lecteurs de la version bêta : Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Traduction française: Nicolas Artance, David Revoy, Valvin, Quetzal2, Alkarex. Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 4.2.9-beta, Inkscape 0.92.3 sur Kubuntu 19.10. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
