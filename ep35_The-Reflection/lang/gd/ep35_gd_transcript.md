# Transcript of Pepper&Carrot Episode 35 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 35: An t-ath-shoillseachadh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Ò, dhùisg thu!
Peabar|2|False|’Eil thu ceart gu leòr? Nach eil thu ro fhuar?
Peabar|3|True|Tha mi duilich ach tha an Rèatha gann orm
Peabar|4|False|agus m’ aura ro lag airson ar dìon on fhuachd shuas san adhar.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Tha mi gu bhith call m’ fhar-luaiths
Peabar|2|True|agus tha am paidhleat ’s a dràgon às ar dèidh fad uairean mòra fhathast.
Peabar|3|False|Glacaidh iad gun trioblaid sinn nuair a thig maille oirnn.
Peabar|4|True|Abair òinseach a bh’ annam!
Peabar|5|False|Bha mi cinnteach nach fhada gus an leigeadh iad romhpa!
Peabar|6|False|Gum faighinn cuidhteas dhiubh.
Peabar|7|True|Iochd! Feumaidh sinn an cur dhinn!
Peabar|8|False|Mus glac iad sinn!
Peabar|9|True|Ach ’s ann gu bheil mi…
Peabar|10|True|… glè …
Peabar|11|False|… sgìth …
Peabar|12|True|h-OPAG!
Peabar|13|False|Thoir an aire, a Pheabar!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cnò-mheannt|1|False|Chunnaic thu siud, nach fhaca?
Arra|2|True|Chunnaic. Teannaidh sinn orra a dh’aithghearr.
Arra|3|False|Chan fhada gus am bi a cumhachd claoidhte.
Cnò-mheannt|4|False|Bheir sinn oirre cho luath ’s a bhios i caithte.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|BEuM!
Peabar|2|False|GU DÈ?!
Peabar|3|False|Na can ruim gun do bhris iad am balla-fuaime!
Peabar|4|False|!!!
Fuaim|5|False|STRÀÀC!!!
Peabar|6|True|Grrr!
Peabar|7|False|Dèan grèim-bàis, a Churrain!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|DÒLAS!
Peabar|2|False|DÈ A-NIS?!
Peabar|3|False|Ò!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Siud uamh Loch nan Adharc!
Peabar|2|False|A Churrain, siud ar cothrom!
Peabar|3|False|Tha mi eòlach air an uamh ud.
Peabar|4|True|Tha cumhang fada lùbach innte a ruigeas ceann a thuit ’na bhroinn.
Peabar|5|True|Tha cruach de dh’aol-chluigein ’na balla spìceach san roinn ud thall a thuit. ’S e ribe marbhtach a th’ innte!
Peabar|6|True|Tha beulag ri a taobh a tha leathann gu leòr ach am faigh sinne troimhpe
Peabar|7|False|ach cha stad a’ bhana-dràgon ri àm ’s bualaidh i ris a’ bhalla air astar mòr! Mùhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Foirfe! Tha iad a’ leantainn oirnn!
Peabar|2|False|Gheibh sinn cuidhteas dhiubh gun dàil.
Peabar|3|True|Tha mi air bhioran!
Peabar|4|False|Mùhahaha!
Peabar|5|False|!!!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Fuirich ort! Dè ghabh mi ris?
Peabar|2|True|Murt?
Peabar|3|False|’Nam bhana-bhuidseach olc?
Peabar|4|False|!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|4|False|FRUuuis!
Peabar|1|True|CHAN ANN!
Peabar|2|False|Cha bhithinn mar sin!
Peabar|3|False|STADAIBH!!!
Peabar|5|False|GÈILLIDH MI!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cnò-mheannt|1|False|Tha fhios a’d nach dèid gu math leat ma ghèilleas tu ri Maighstir Raidis, nach eil?
Peabar|2|False|Tha fhios a’m ach tha mi fhathast a’ creidsinn gun lorg mi dòigh sa bhruidhninn rithe – ach an tuigeadh i.
Peabar|3|True|Aig deireadh na sgeòil, chan eil rud fon ghrèin nach gabhadh a chàradh.
Peabar|4|False|Dh’ionnsaich mi sin o dheagh charaid.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Mòran taing gun dug thu cead do Churran aodach glan fhaighinn dhomh.
Cnò-mheannt|2|True|Tha tu làn di-beathte.
Cnò-mheannt|3|False|Rinn thu ar n-obair nas fhasa dhuinn agus chan eil adhbhar nach bithinn bàidheil.
Cnò-mheannt|4|True|Ach fhios a’d, chuir thu Raidis mun cuairt.
Cnò-mheannt|5|True|Tha i ag iarraidh a h-uile càil foirfe. Mhill thu sin le do shalachar, d’ aodach robach is tuiteam às an speur.
Cnò-mheannt|6|False|Agus thug thu ionnsaigh oirre! Bidh i trom ort.
Peabar|7|False|Tha fhios a’m.
Cnò-mheannt|8|True|Co-dhiù no co-dheth, tha mi feumach air norrag.
Cnò-mheannt|9|True|Falbhaidh sinn nuair a dhùisgeas mi.
Cnò-mheannt|10|False|Dèan na thogras tu ach fuirich faisg oirnn…
Fuaim|11|False|Spliut
Cnò-mheannt|12|False|Abair bana-bhuidseach annasach.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Nise, ge b’ e dè an staing no cunnart,
Peabar|2|True|mairidh mi dìleas dha mo nàdar.
Peabar|3|False|’Nam gheall!
Neach-aithris|4|False|RI LEANTAINN…

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|18mh dhen Ògmhios 2021 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Bathar-bog: Krita 4.4.2, Inkscape 1.1 air Kubuntu Linux 20.04 Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
Peabar|3|True|An robh fios agad?
Peabar|4|False|Tha Peabar ⁊ Curran saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|2|True|Mòran taing dhan 1054 pàtran a thug taic dhan eapasod seo!
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Peabar ⁊ Curran cuideachd is chì thu d’ àinm an-seo!
Peabar|7|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|6|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|8|False|Mòran taing!
