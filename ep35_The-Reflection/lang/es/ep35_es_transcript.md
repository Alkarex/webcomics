# Transcript of Pepper&Carrot Episode 35 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 35: El reflejo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Oh, te has despertado!
Pimienta|2|False|¿Estás bien? ¿Tienes frío?
Pimienta|3|True|Lo siento, me voy a quedar sin Rea muy pronto...
Pimienta|4|False|y mi aura es demasiado débil para protegernos del frío a esta altitud.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|Estoy a punto de perder mi hipervelocidad
Pimienta|2|True|y este dragón y su piloto nos pisan los talones desde hace horas.
Pimienta|3|False|No tendrán ningún problema para atraparnos en cuanto ralenticemos.
Pimienta|4|True|¡Qué idiota he sido!
Pimienta|5|False|¡Estaba convencida de que se rendirían hace horas!
Pimienta|6|False|Si pudiera al menos despistarlos.
Pimienta|7|True|NO. ¡Lo que tengo que hacer es deshacerme de ellos!
Pimienta|8|False|¡Antes de que nos atrapen!
Pimienta|9|True|Pero estoy...
Pimienta|10|True|...tan...
Pimienta|11|False|...cansada...
Pimienta|12|True|¡UPS!
Pimienta|13|False|¡Vuelve en ti, Pimienta!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|¿Has visto eso?
Arra|2|True|Sí. Esta persecución acabará pronto.
Arra|3|False|Sus poderes se agotarán en poco tiempo.
Torreya|4|False|Aguantaremos para atacar en ese momento.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡BoOM!
Pimienta|2|False|¡¿QUÉ?!
Pimienta|3|False|¡¿Han roto la barrera del sonido?!
Pimienta|4|False|!!!
Sonido|5|False|¡¡¡WOousH!!!
Pimienta|6|True|¡Grrr!
Pimienta|7|False|¡Agárrate, Zanahoria!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡JOLÍN!
Pimienta|2|False|¡¿QUÉ PUEDO HACER?!
Pimienta|3|False|¡Oh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡La cueva del Lago de los Cuernos!
Pimienta|2|False|¡Zanahoria, es nuestra oportunidad!
Pimienta|3|False|Conozco esta gruta.
Pimienta|4|True|Es un largo pasadizo que acaba en una cavidad creada por un terreno que ha cedido.
Pimienta|5|True|Las estalactitas de ese terreno hundido forman un muro de pinchos. ¡Es una trampa mortal!
Pimienta|6|True|Hay una pequeña abertura lo suficientemente grande para que podamos escapar,
Pimienta|7|False|pero este dragón no podrá parar a tiempo y chocará contra el muro ¡a toda velocidad! ¡Muajajaja!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Perfecto! ¡Nos están siguiendo!
Pimienta|2|False|¡Vamos a deshacernos de ellos ahora mismo!
Pimienta|3|True|¡Estoy ansiosa por verlos!
Pimienta|4|False|¡Muajajaja!
Pimienta|5|False|!!!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Espera! ¿En qué me he convertido?
Pimienta|2|True|¿En una asesina?
Pimienta|3|False|¿Una bruja malvada?
Pimienta|4|False|!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|4|False|¡Suooush!
Pimienta|1|True|¡NO!
Pimienta|2|False|¡Yo no soy así!
Pimienta|3|False|¡¡¡STOOOOP!!!
Pimienta|5|False|¡ME RINDO!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|¿Eres consciente de que entregarte a la Maestra Wasabi no va a terminar bien para ti?
Pimienta|2|False|Lo sé, pero siento que aún debe de haber una forma de entendernos, de hacerle comprender.
Pimienta|3|True|Después de todo, nada es irreparable.
Pimienta|4|False|Aprendí eso de una buena amiga.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Gracias por haber dejado a Zanahoria ir a buscarme una muda de ropa limpia.
Torreya|2|True|De nada.
Torreya|3|False|Tú me has facilitado el trabajo, es lo mínimo que podía hacer por ti.
Torreya|4|True|Pero sabes, realmente has hecho enfadar a Wasabi.
Torreya|5|True|Ella exige la perfección. La has alterado con tu desorden, tu ropa y tu forma de llegar.
Torreya|6|False|¡Además, la has atacado! No te hará concesiones.
Pimienta|7|False|Lo sé.
Torreya|8|True|Bueno, necesito echar una siestecita.
Torreya|9|True|Partiremos cuando despierte.
Torreya|10|False|Haz lo que tú quieras siempre que no te vayas muy lejos...
Sonido|11|False|Splush
Torreya|12|False|¡Qué bruja tan singular!

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|A partir de ahora, no importan los riesgos o los peligros,
Pimienta|2|True|seré fiel a mí misma, lo que soy.
Pimienta|3|False|¡Lo prometo!
Narrador|4|False|CONTINUARÁ...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|18 de junio de 2021 Dibujo & Guion: David Revoy. Lectores de la versión beta: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versión en castellano Traducción: TheFaico Revisión: Li Chong Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.2, Inkscape 1.1 en Kubuntu Linux 20.04 Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
Pimienta|3|True|¿Sabías qué?
Pimienta|4|False|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|2|True|¡Este episodio ha recibido el apoyo de 1054 mecenas!
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|7|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|6|True|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|8|False|¡Gracias!
