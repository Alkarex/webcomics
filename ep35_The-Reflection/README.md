# Episode 35: The Reflection

![cover of episode 35](https://www.peppercarrot.com/0_sources/ep35_The-Reflection/low-res/Pepper-and-Carrot_by-David-Revoy_E35.jpg)

## Comments from the author

So, in this episode, Pepper starts tired and Rea exhausted. But she is still chased by a Pilot (Torreya) and Dragon (Arra) of Ah. This situation puts her into a big stress. Pepper will invent a plan to get rid of the Dragon, expressing the darkest side of her personality. This confrontation with herself will remind her who she really is, and decide a rather anticlimactic move... The episode has an interesting curve for Pepper, and recenter the story around the "good witch" VS "mean witch" struggle for Pepper growing as a Chaosah witch.

From [Beta-reading Forum of episode 35](https://framagit.org/peppercarrot/webcomics/-/issues/187)
