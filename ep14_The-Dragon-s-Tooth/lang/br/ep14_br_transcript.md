# Transcript of Pepper&Carrot Episode 14 [br]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Rann 14: An Dant Aerouant

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|5|True|Ma, ma, ma! Echu ar vakañsoù.
Cumin|6|False|Stagomp gant ur gentel war an drammoù hen hag an aozennoù diazez anezhe.
Cumin|7|True|Mmm...Chaous! N'eus ket ken a boultr dant aerouant.
Cumin|8|False|Ne vo ket staget gant ar gentel-mañ hep an aozenn-se...
Writing|9|False|Dant Aerouant
Writing|4|False|33
Writing|3|False|DOMANI PREVEZ
Writing|2|True|SORSEREZ
Writing|1|True|DIWALLIT

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Kayen! Tin! Mat e vefe deoc'h mont da gerc'hat din dant aerouant?
Cayenne|2|False|Gant ar yenien?! N'in ket 'vat!
Thyme|3|False|Yay... Memes tra...
Pepper|4|False|Kudenn ebet me zo 'vont!
Pepper|6|False|Ne vin ket pell!
Pepper|7|True|"Ar yenien", "ar yenien"!
Pepper|8|False|Nag ur vandennad yer beliet!
Pepper|9|False|Un durkez vat, un nebeud gwarezioù ha dimp eo an dent erevent!
Cumin|5|False|Pepper gortoz!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Aha!
Pepper|2|False|Chaous! Un dister dra luzietoc'h e vo eget na soñje din!
Pepper|3|True|Pfui!
Pepper|4|False|...ha soñjal a rae din e vije aesoc'h gant un aerouant aer!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot!...
Pepper|2|True|Deus en-dro...
Pepper|3|True|...Brudet eo aerouant ar geunioù evit bezañ kuñvoc'h koulskoude.
Pepper|4|False|N'eo ket me a lâre...
Pepper|5|True|Mmm... Evit an aerouant-kurun...
Pepper|6|False|...n'on ket sur e jaojfe an durkez-mañ.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ok.
Pepper|2|True|Re ziaes eo...
Pepper|3|False|... diskregiñ a ran.
Pepper|5|False|Pepper ne ziskrog morse!
Pepper|4|True|Naren!
Narrator|6|False|An deiz war-lerc'h
Cumin|8|False|!!
Bird|7|False|KOTOKOKOOoooog! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|- ECHU -
Credits|11|False|01/2016 - Tresadennoù & Senario : David Revoy
Writing|4|True|Dentour
Writing|5|False|Erevent
Writing|6|False|Digoust!
Sound|3|False|pop !
Pepper|1|True|Ha neuze, estlammet, n'oc'h ket?
Pepper|2|False|Bez' em eus ur c'hant bennak a zent erevent!
Cumin|7|True|...Arsa Pepper, an dant aerouant...
Cumin|8|False|...zo ur blantenn!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot zo digoust, mammenn-digor, ha sponsoret a-drugarez da vesenerezh al lennerien. Evit ar rann-mañ, trugarez d'an 671 Mesen:
Credits|4|False|Lañvaz: Creative Commons Attribution 4.0 Mammennoù: e www.peppercarrot.com Meziantoù: treset eo bet ar rann-mañ 100% gant meziantoù frank Krita 2.9.10, Inkscape 0.91 war Linux Mint 17
Credits|2|True|C'hwi ivez, deuit da vezañ mesen Pepper&Carrot evit ar rann a zeu war
Credits|3|False|https://www.patreon.com/davidrevoy
