# Transcript of Pepper&Carrot Episode 14 [ca]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodi 14: La Dent de Drac

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|5|True|Molt rebé! S'han acabat les vacances!
Cumin|6|False|Començarem per un curs sobre pocions antigues i el seus ingredients principals!
Cumin|7|True|Vatua l'olla! No ens queda pols de dent de drac!
Cumin|8|False|No podem començar el curs sense aquest ingredient...
Writing|9|False|Dent de Drac
Writing|4|False|33
Writing|3|False|PROPIETAT PRIVADA
Writing|2|True|BRUIXA
Writing|1|True|PERILL

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Cayenne! Thym! Que podríeu anar a buscar-me dents de drac?
Cayenne|2|False|Amb aquest fred?! No gràcies!
Thyme|3|False|Pel que fa a mi... Dic el mateix...
Pepper|4|False|Cap problema, jo me n'ocupo!
Pepper|6|False|Torno de seguida!
Pepper|7|True|“El fred”, “el fred”!
Pepper|8|False|Però quina colla de figa-flors!
Pepper|9|False|Unes bones tenalles, les proteccions i ja podem anar a per les dents de drac!
Cumin|5|False|Pepper espera!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah! Ah!
Pepper|2|False|Vatua! Això costarà una mica més del que havia previst!
Pepper|3|True|Pfff!
Pepper|4|False|...i jo que pensava que seria una mica més fàcil amb un drac dels aires!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot!...
Pepper|2|True|Torna...
Pepper|3|True|...els dracs dels fangars tenen fama de ser més dòcils. I això...
Pepper|4|False|és el que jo pensava...
Pepper|5|True|Mmm... potser aquestes tenalles...
Pepper|6|False|...no estan fetes pels dracs dels llamps.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|D'acord.
Pepper|2|True|És massa difícil...
Pepper|3|False|...plego.
Pepper|5|False|La Pepper no abandona MAI!
Pepper|4|True|No!
Narrator|6|False|L'endemà...
Cumin|8|False|!!
Bird|7|False|Quiquiriquiiii ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|- FI-
Credits|11|False|01/2016 - Dibuix i guió : David Revoy. Traducció al català: Juan José Segura
Writing|4|True|Dentista
Writing|5|True|per
Writing|6|False|Dracs
Writing|7|False|Gratis!
Sound|3|False|pop !
Pepper|1|True|Què et sembla? Impressionada, eh?
Pepper|2|False|En tinc ben bé un bon centenar, de dents de drac!
Cumin|8|True|...però Pepper, la dent de drac...
Cumin|9|False|...és una planta!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot és completament gratuït, open-source i finançat gràcies al mecenatge dels lectors. Volem agraïr per aquest episodi a 671 mecenes:
Credits|4|False|Llicència : Creative Commons Attribution 4.0 Fitxers : disponibles a www.peppercarrot.com Software : aquest episodi ha sigut 100% fet amb libre software Krita 2.9.6, Inkscape 0.91 a Linux Mint 17
Credits|2|True|Tu també pots esdevenir un mecenes de Pepper&Carrot pel proper capítol:
Credits|3|False|https://www.patreon.com/davidrevoy
