# Transcript of Pepper&Carrot Episode 14 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 14: El diente de dragón

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumino|5|True|¡Bueno, bueno! ¡Se acabaron las vacaciones!
Cumino|6|False|Hoy vamos a empezar un curso sobre pociones antiguas y sus ingredientes principales.
Cumino|7|True|Mmm... ¡vaya! No queda polvo de diente de dragón.
Cumino|8|False|No podemos comenzar el curso sin ese ingrediente...
Escritura|9|False|Diente de dragón
Escritura|4|False|33
Escritura|3|False|PELIGROSA
Escritura|2|True|BRUJA
Escritura|1|True|CUIDADO

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumino|1|False|¡Cayena! ¡Tomillo! ¿Podéis ir a buscarme algún diente de dragón?
Cayena|2|False|¡¿Con este frío?! ¡No, gracias!
Tomillo|3|False|Sip... Lo mismo digo...
Pimienta|4|False|No hay problema, ¡yo me encargo!
Pimienta|6|False|¡Vuelvo en un periquete!
Pimienta|7|True|¡"El frío", "el frío"!
Pimienta|8|False|¡Vaya panda de cobardicas!
Pimienta|9|False|¡Unas buenas tenazas, alguna protección y ya son nuestros los dientes de dragón!
Cumino|5|False|Pimienta, ¡espera!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Jajaja!
Pimienta|2|False|¡Fiuu! ¡Esto va a ser un poquito más complicado de lo que había pensado!
Pimienta|3|True|¡Mpff!
Pimienta|4|False|...y yo que creía que sería más fácil con un Dragón del Aire...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡¡Zanahoria!!
Pimienta|2|True|Vuelve...
Pimienta|3|True|...¡el Dragón del Pantano es conocido por su carácter dócil!
Pimienta|4|False|...o eso me habían dicho...
Pimienta|5|True|Mmm... para el Dragón Relámpago...
Pimienta|6|False|...no estoy segura de que esta sea la herramienta adecuada.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|Ok.
Pimienta|2|True|Esto es demasiado difícil...
Pimienta|3|False|... me rindo.
Pimienta|5|False|¡Pimienta no se rinde jamás!
Pimienta|4|True|¡No!
Ave|7|False|¡ ¡ ¡ KIKIRIKKIIIIII ! ! !|nowhitespace
Narrador|6|False|Al día siguiente
Cumino|8|False|¡¡!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|10|False|- FIN -
Créditos|11|False|Enero 2016 - Dibujo & Guión: David Revoy - Traducción: TheFaico
Escritura|4|True|Dentista
Escritura|5|True|de
Escritura|6|False|Dragones
Escritura|7|False|¡Servicio gratuito!
Sonido|3|False|¡pop!
Pimienta|1|True|¿Qué te parece? Impresionada, ¿verdad?
Pimienta|2|False|¡He conseguido por lo menos cien dientes de dragón!
Cumino|8|True|...pero, Pimienta, el diente de dragón...
Cumino|9|False|...¡es una planta!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 671 mecenas:
Créditos|2|True|Licencia: Creative Commons Attribution 4.0 Ficheros originales disponibles en www.peppercarrot.com Herramientas: Este episodio ha sido creado al 100% con software libre Krita 2.9.10, Inkscape 0.91 en Linux Mint 17
Créditos|3|False|Tú también puedes ser mecenas de Pepper&Carrot para el próximo episodio
Créditos|4|False|https://www.patreon.com/davidrevoy
