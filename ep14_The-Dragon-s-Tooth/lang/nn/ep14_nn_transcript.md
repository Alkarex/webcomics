# Transcript of Pepper&Carrot Episode 14 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 14: Draketanna

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Karve|5|False|Jajamensann! Då var ferien over!
Karve|6|False|La oss byrja med eit kurs om eldgamle heksebrygg og hovudingrediensane deira.
Karve|7|True|Hm ... Søren! Tomt for draketann-pulver.
Karve|8|False|Kan ikkje byrja kurset utan det ...
Skrift|9|False|Draketann
Skrift|4|False|33
Skrift|3|False|PRIVAT OMRÅDE
Skrift|2|False|HEKS
Skrift|1|False|FARE

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Karve|1|False|Kajenne! Timian! Kan de finna draketann til meg?
Kajenne|2|False|I denne kulda?! Elles takk!
Timian|3|False|Same her!
Pepar|4|False|Ikkje noko problem. Eg fiksar det!
Pepar|6|False|Er snart tilbake!
Pepar|7|True|«Kulda», «kulda»!
Pepar|8|False|For nokre pyser!
Pepar|9|False|Ei god tong og litt vern på kroppen, så er draketennene snart våre.
Karve|5|False|Vent, Pepar!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Ha, ha!
Pepar|2|False|Oi! Det vert visst ørlite vanskelegare enn eg hadde rekna med.
Pepar|3|True|Pfff!
Pepar|4|False|... og eg som trudde det ville vera lettare med ein luftdrake!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Gulrot!
Pepar|2|True|Kom tilbake ...
Pepar|3|False|Sumpdrakar er kjende for å vera fredelege!
Pepar|4|False|... trudde eg.
Pepar|5|True|Hmmm... Lyndraken ...
Pepar|6|False|Her er nok ikkje dette rett verktøy for jobben.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|OK.
Pepar|2|True|Det er for vanskeleg ...
Pepar|3|False|Eg gjev opp.
Pepar|5|False|Pepar gjev aldri opp!
Pepar|4|True|Nei!
Fugl|7|False|Kykkelikyyyyyy!!!
Forteljar|6|False|Neste dag ...
Karve|8|False|!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|9|False|– SLUTT –
Bidragsytarar|10|False|Januar 2016 – Teikna og fortald av David Revoy – Omsett av Arild Torvund Olsen og Karl Ove Hufthammer
Skrift|5|False|tannlege|nowhitespace
Skrift|4|True|Drake-|nowhitespace
Skrift|6|False|Gratis!
Lyd|3|False|popp!
Pepar|1|False|No? Imponert, eller?
Pepar|2|False|Eg har hundre draketenner! Minst!
Karve|7|False|... men Pepar, draketann, ...
Karve|8|False|... det er ei plante!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 671 som støtta denne episoden:
Bidragsytarar|2|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot:
Bidragsytarar|3|False|https://www.patreon.com/davidrevoy
Bidragsytarar|4|False|Lisens: Creative Commons Attribution 4.0 Kjeldefiler: Tilgjengelege på www.peppercarrot.com Verktøy: Denne episoden er 100 % teikna med fri programvare Krita 2.9.10, Inkscape 0.91 på Linux Mint 17
