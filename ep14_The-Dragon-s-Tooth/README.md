﻿# Episode 14: The Dragon's Tooth

![cover of episode 14](https://www.peppercarrot.com/0_sources/ep14_The-Dragon-s-Tooth/low-res/Pepper-and-Carrot_by-David-Revoy_E14.jpg)

## Comments from the author

This one was inspired by the dandelions flower ; ( in French , "dent de lions" means "lion's tooth" ). While reading this fun fact, I got the idea of this story.

From [Author's blog of episode 14](https://www.davidrevoy.com/article550/episode-14-the-dragon-s-tooth/show#comments)
