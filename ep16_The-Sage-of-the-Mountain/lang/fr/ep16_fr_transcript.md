# Transcript of Pepper&Carrot Episode 16 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 16 : Le Sage de la Montagne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|9|False|Non, non, non et non, Pepper.
Écriture|1|True|Safran
Écriture|2|True|Sorcellerie
Écriture|3|False|★★★
Écriture|5|False|BOUCHER
Écriture|6|False|15
Écriture|4|False|13
Écriture|7|False|rue Étoile
Écriture|8|False|Coiffeur
Son|10|True|Glou
Son|11|False|Glou

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|2|False|Moi, je ne joue pas les médiatrices, même pas pour une amie. Ça finit toujours mal ce genre d'histoires.
Safran|1|True|C'est quelque chose que tu vas devoir gérer toute seule.
Pepper|3|False|S'te plaît, Safran...
Pepper|4|True|J'apprends rien avec mes marraines, elles ne font que squatter chez moi, se plaindre de leurs vieux os et me crier dessus quoi que je fasse...
Safran|6|True|Écoute, t'es une sorcière, agis donc en sorcière !
Safran|8|False|Comme une grande fille !
Safran|7|True|Parle-leur directement !
Pepper|5|False|Tu peux vraiment pas venir m'aider à leur en parler ?...
Safran|9|False|D'accord d'accord ! Je suppose que je peux venir avec toi leur en parler...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|1|False|Comment ça, tu n'apprends rien ?!
Thym|12|False|Et tu n'es même pas assez courageuse pour venir nous le dire toute seule ?!
Thym|14|False|Si mon dos allait mieux, je te donnerais une bonne leçon ! Et pas qu'une de sorcellerie si tu vois ce que je veux dire !
Cayenne|15|True|Thym, arrête.
Cayenne|16|False|Elles ont raison.
Oiseau|2|True|piou
Oiseau|3|False|piou
Oiseau|4|True|piou
Oiseau|5|False|piou
Oiseau|6|True|piou
Oiseau|7|False|piou
Oiseau|8|True|piou
Oiseau|9|False|piou
Oiseau|10|True|piou
Oiseau|11|False|piou
Son|13|False|PAF !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Nous ne sommes plus toutes jeunes et c'est le bon moment pour une leçon.
Cayenne|2|True|Je propose une rencontre avec
Thym|5|True|Hé hé !
Thym|6|True|Le Sage ?
Thym|7|False|Ne sont-elles pas encore trop jeunes pour comprendre ça ?
Pepper|8|True|Trop jeune pour quoi !?
Pepper|9|False|On y va !
Safran|10|False|"On" ?
Thym|11|True|Bon, nous y voici.
Thym|12|True|Je vous laisse faire connaissance avec le Sage.
Thym|13|False|Il se trouve en haut de cette pente.
Thym|14|True|Prêtes à prendre
Thym|15|False|une bonne leçon ?
Cayenne|3|True|le Sage de la montagne !
Cayenne|4|False|Son enseignement est essentiel.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Prête !
Safran|2|False|Moi aussi !
Thym|3|False|Bien, bien ! Bonne attitude !
Pepper|9|False|ON ATTAQUE !!!
Safran|6|False|Un... UN MONSTRE !
Pepper|7|True|C'EST UN PIÈGE !
Pepper|8|True|JE LE SAVAIS !!!
Pepper|5|False|Oh non !
Thym|4|False|Allez-y !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|GRAVITAS SPIRALIS !
Safran|2|False|BRASERO INTENSIA !
Son|3|False|Plouf !
Thym|4|True|Ahh, vous les jeunes, à vouloir toujours tout attaquer !
Pepper|7|False|Mais c'est quoi ces leçons !
Crédits|11|False|Licence : Creative Commons Attribution 4.0, Logiciels : Krita, Blender, G'MIC, Inkscape sur Ubuntu
Crédits|10|False|Basé sur l'univers d'Hereva crée par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Narrateur|8|False|- FIN -
Crédits|9|False|04/2016 - www.peppercarrot.com - Dessin & Scénario : David Revoy, corrections : Remi Rampin
Thym|6|False|Quand rien ne va plus, rien de tel qu'un bon bain ! C'est bon pour nos rhumatismes et pour vos nerfs !...
Thym|5|True|La leçon du Sage, c'est de l'imiter !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 671 Mécènes :
Crédits|2|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
