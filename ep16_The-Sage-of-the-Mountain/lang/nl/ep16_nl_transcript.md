# Transcript of Pepper&Carrot Episode 16 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 16: De wijze op de berg

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffraan|9|False|Nee, nee, nee, Pepper.
Geschrift|2|True|Saffraan
Geschrift|3|False|Hekserij
Geschrift|1|False|★★★
Geschrift|5|False|SLAGERIJ
Geschrift|6|False|15
Geschrift|4|False|13
Geschrift|7|False|Markt
Geschrift|8|False|Kapper
Geluid|10|True|Gloek
Geluid|11|False|Gloek

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffraan|2|False|Ik speel geen tussen-persoon, ook niet voor vrienden. Zulke zaken lopen altijd slecht af.
Saffraan|1|True|Je zal het in je eentje moeten oplossen, hoor.
Pepper|3|False|Alsjeblieft, Saffraan ...
Pepper|4|True|Ik leer niets van mijn peettantes. Ze zitten de godganse dag in hun stoel, klagen over hun oude gewrichten en roepen wat ik moet doen ...
Saffraan|6|True|Luister, je bent een heks, gedraag je er dan ook naar!
Saffraan|8|False|Verman je eens en praat zelf met hen!
Saffraan|7|True|Kan je me echt niet helpen? Even met hen praten?
Pepper|5|False|Oké, oké! Ik kan wel even meekomen.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|1|False|Hoezo, je leert niets van ons?!
Thym|12|False|En je hebt zelfs het lef niet om ons dat in je eentje te komen zeggen?!
Thym|14|False|Als mijn rug me niet zo parten speelde, zou ik je een lesje leren! En dan niet in de hekserij, als je begrijpt wat ik bedoel!
Cayenne|15|True|Thym, wacht.
Cayenne|16|False|Ze hebben gelijk.
Vogel|2|True|pieuw
Vogel|3|False|pieuw
Vogel|4|True|pieuw
Vogel|5|False|pieuw
Vogel|6|True|pieuw
Vogel|7|False|pieuw
Vogel|8|True|pieuw
Vogel|9|False|pieuw
Vogel|10|True|pieuw
Vogel|11|False|pieuw
Geluid|13|False|PAF!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|We worden al een dagje ouder, en het moment voor een goede les is aangebroken.
Cayenne|2|True|Ik stel een ontmoeting voor met
Thym|5|True|Hè hè!
Thym|6|True|De wijze?
Thym|7|False|Zijn ze niet nog wat te jong om zijn lessen te begrijpen?
Pepper|8|True|Te jong waarvoor!?
Pepper|9|False|We gaan erheen!
Saffraan|10|False|"We"?
Thym|11|True|We zijn er.
Thym|12|True|Ik laat jullie kennismaken met de wijze.
Thym|13|False|Hij is bovenaan deze helling te vinden.
Thym|14|True|Zijn jullie klaar voor
Thym|15|False|een goede les?
Cayenne|3|True|de wijze op de berg!
Cayenne|4|False|Zijn onderricht is onontbeerlijk.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ik ben klaar!
Saffraan|2|False|Ik ook!
Thym|3|False|Prachtig! Zo mag ik het horen!
Pepper|9|False|TEN AANVAL!!!
Saffraan|6|False|Een ... Een monster!
Pepper|8|True|HET IS EEN VAL!
Pepper|7|True|IK WIST HET!
Pepper|5|False|Oh nee!
Thym|4|False|Vooruit!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|GRAVITAS SPIRALIS!
Saffraan|2|False|BRASERO INTENSIA!
Geluid|3|False|Plons!
Thym|4|True|Ahh, jullie jongelingen, altijd alles willen aanvallen!
Pepper|7|False|Wat zijn dat nu voor lessen!
Aftiteling|11|False|Licentie: Creative Commons Naamsvermelding 4.0, Software: Krita, Blender, G'MIC, Inkscape op Ubuntu
Aftiteling|10|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand en Alex Gryson.
Verteller|8|False|- EINDE -
Aftiteling|9|False|04/2016 - www.peppercarrot.com - Tekeningen & verhaal: David Revoy - Vertaling: Willem Sonke & Midgard
Thym|6|False|Als alles tegenzit, doet niets zo veel deugd als een lekker bad! Het is goed tegen onze reuma en voor jullie zenuwen!
Thym|5|True|De les van de wijze is te doen zoals hij!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 671 patronen:
Aftiteling|2|False|Jij kan ook een patroon van Pepper&Carrot worden op https://www.patreon.com/davidrevoy
