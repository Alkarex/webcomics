# Transcript of Pepper&Carrot Episode 36 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 36: El ataque sorpresa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|¡¿UNA DISCULPA?!
Wasabi|2|False|¡No puedes estar hablando en serio!
Wasabi|3|False|¡ENCERRADLA EN EL CALABOZO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|Fschh...
Pimienta|2|True|¡Agh!
Pimienta|3|False|¡No puedo hacer nada!
Pimienta|4|False|¡Maldita prisión mágica! ¡Grrrr!
Sonido|5|False|¡CLANG!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¿Cómo he podido ser tan ingenua?
Shichimi|2|True|¡Shhhhh, Pimienta!
Shichimi|3|False|Baja la voz.
Pimienta|4|False|¡¿Quién anda ahí?!
Shichimi|5|True|¡Shhhhh! ¡Silencio!
Shichimi|6|True|Ven aquí.
Shichimi|7|False|He venido a liberarte.
Sonido|8|False|Kshiii...
Pimienta|9|False|¿Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Yo... Siento lo que ha ocurrido...
Shichimi|2|False|...con nuestra pelea, ya sabes.
Shichimi|3|True|Yo...
Shichimi|4|False|tenía que hacerlo.
Pimienta|5|True|No te preocupes, lo entiendo.
Pimienta|6|False|Gracias por venir.
Shichimi|7|False|Esta celda mágica es realmente poderosa. ¡Y hecha solo para ti!
Pimienta|8|False|¡Ja, ja!
Shichimi|9|False|Baja la voz o nos oirán.
Rata|10|True|LAP
Rata|11|True|LAP
Rata|12|False|LAP
Shichimi|13|True|Sabes,
Shichimi|14|True|también he venido porque, justo después de la ceremonia, me admitieron en el círculo de confianza de Wasabi
Shichimi|15|False|Y me he enterado de sus planes...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|16|False|¡CLANG!
Sonido|18|True|¡CLONK!
Sonido|19|True|¡CLANG!
Sonido|20|True|¡CLANK!
Sonido|21|False|¡CLANG!
Shichimi|1|False|Es horrible, Pimienta.
Shichimi|2|False|Wasabi quiere dominar a todas las otras escuelas de magia...
Shichimi|3|False|Partirá al amanecer con un ejército de brujas hacia Caliciudad...
Pimienta|4|True|¡Oh, no!
Pimienta|5|True|¡Celandria!
Pimienta|6|False|¡Y su reino!
Shichimi|7|True|¡Y la magia de Zombiah!
Shichimi|8|False|¡Tenemos que avisarles de inmediato!
Shichimi|9|False|Una piloto y su dragón nos están esperando en la plataforma para sacarnos de aquí.
Shichimi|10|False|¡Ya está, ha cedido la cerradura!
Sonido|11|False|¡Ching!
Pimienta|12|True|¡Bravo!
Pimienta|13|False|Voy a por Zanahoria y mi sombrero.
Rata|14|False|¡cuic!
Zanahoria|15|False|¡FISSSS!
Rata|17|False|¡cuiii!
Pimienta|22|False|!!!
Shichimi|23|False|!!!
Guardia|24|True|¡GUARDIA!
Guardia|25|False|¡POR AQUÍ!
Guardia|26|False|¡UN INTRUSO ESTÁ LIBERANDO A LA PRISIONERA!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Casi lo teníamos...
Pimienta|2|False|Si, por poco.
Pimienta|3|False|Por cierto, ¿Sabes por qué Wasabi la ha tomado conmigo?
Shichimi|4|False|Porque teme a las brujas de Chaosah, Pimienta...
Shichimi|5|True|Especialmente a tus reacciones en cadena.
Shichimi|6|False|Cree que son una amenaza para sus planes.
Pimienta|7|True|Ah, eso, pfff...
Pimienta|8|False|No tiene nada que temer; nunca he conseguido hacerlo a propósito.
Shichimi|9|False|¿En serio?
Pimienta|10|False|¡Sí, en serio, jajaja!
Shichimi|11|True|¡Jijiji!
Shichimi|12|False|Está realmente paranoica...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|True|¡Oficial!
Rey|2|False|Entonces, ¿es este el castillo de la bruja?
Oficial|3|True|¡Sumamente probable, mi señor!
Oficial|4|False|Varios de nuestros espías la han visto hace poco aquí.
Rey|5|True|GRrrr...
Rey|6|False|¡Así que es donde vive la que amenaza a nuestras tradiciones y nuestro arte de hacer guerra!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|False|Celebraremos nuestra alianza arrasando con este templo de la faz de Hereva.
Sonido|3|False|Clap
Enemigo|2|False|¡Bien dicho!
Ejército.|4|True|Yeah!
Ejército.|5|True|Yeah!
Ejército.|6|True|Yeah!
Ejército.|7|True|Yeah!
Ejército.|8|True|Yeehaaa!
Ejército.|9|True|Yarr!
Ejército.|10|True|Yeehaw!
Ejército.|11|True|Yeee-haw!
Ejército.|12|True|Yaaa!
Ejército.|13|False|Hurrah!
Rey|14|True|¡CATAPULTAS!
Rey|15|False|¡¡¡FUEGO!!!
Sonido|16|False|¡huUBOOOOOOOOOOO!
Sonido|17|True|¡Gouush!
Sonido|18|False|¡Gouush!
Rey|19|False|¡¡¡ATACAAAAAAD!!!
Pimienta|20|True|¿Qué está ocurriendo?
Pimienta|21|False|¡¿Un ataque?!
Sonido|22|True|¡BuM!
Sonido|23|False|¡BuuuM!
Shichimi|24|False|¡¿Cómo?!
Sonido|25|True|¡BUU~UUM!
Sonido|26|False|¡BUM!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡¡cof
Pimienta|2|False|coff!!
Pimienta|3|False|¡Shichimi! ¿Estás bien?
Shichimi|4|True|Sí, ¡Estoy bien!
Shichimi|5|True|¿Y tú?
Shichimi|6|False|¿y Zanahoria?
Pimienta|7|False|Estamos bien.
Pimienta|8|True|Pero qué diantres...
Pimienta|9|False|No... no puedo creerlo...
Shichimi|10|True|¡¿De dónde han salido esos soldados?!
Shichimi|11|True|¡¿Y con catapultas?!
Shichimi|12|False|¿Qué querrán?
Pimienta|13|True|No lo sé...
Pimienta|14|False|Pero a esos dos los conozco.
Shichimi|15|False|?
Shichimi|16|False|!!!
Shichimi|17|False|¡Torreya, aquí!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|¡Shichimi!
Torreya|2|False|Gracias a los espíritus, estás sana y salva.
Sonido|3|False|Grab
Torreya|4|True|¡Estaba tan preocupada cuando escuché que te habían capturado!
Torreya|5|True|¡Y esta batalla!
Torreya|6|False|¡Qué caos!
Shichimi|7|False|Torreya, me alegro tanto de verte.
Pimienta|8|False|Oh, vaya...
Pimienta|9|False|Así que la piloto de ese dragón es la novia de Shichimi…
Pimienta|10|False|No puedo imaginarme qué hubiera pasado si me hubiera deshecho de ella.
Pimienta|11|False|Y ese ejército, han tenido que seguirme.
Pimienta|12|False|Sin ellos, no habríamos escapado.
Pimienta|13|False|Está todo tan conectado...
Pimienta|14|False|...¡OH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|¿Qué le pasa?
Shichimi|2|False|¿Pimienta? ¿Está todo bien?
Pimienta|3|True|Sí, estoy bien.
Pimienta|4|False|Me acabo de dar cuenta de algo.
Pimienta|5|False|Todo lo que ha ocurrido es, directa o indirectamente, el resultado de mis acciones, mis elecciones...
Pimienta|6|False|...en otras palabras, ¡mi reacción en cadena!
Shichimi|7|True|¿De verdad?
Shichimi|8|False|Nos lo tienes que explicar.
Torreya|9|True|Suficiente charla, estamos en medio de un campo de batalla.
Torreya|10|True|Hablaremos de eso cuando estemos volando.
Torreya|11|False|¡Súbete, Pimienta!
Shichimi|12|True|Torreya tiene razón.
Shichimi|13|False|Tenemos que llegar a Caliciudad, como sea.
Pimienta|14|False|Espera un momento.
Pimienta|15|True|El ejército de Wasabi está a punto de contraatacar.
Pimienta|16|False|No podemos dejar que se maten entre ellos.
Pimienta|17|False|Siento que es mi responsabilidad terminar esta batalla.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|¿Pero, cómo?
Arra|2|True|Sí, ¿cómo planeas hacer eso, bruja?
Arra|3|False|No has recuperado mucha de tu Rea, Lo noto.
Pimienta|4|True|Tienes toda la razón, pero sé un hechizo que lo arreglará todo.
Pimienta|5|False|Solo necesito tu Rea para que pueda alcanzar a toda esa gente.
Arra|6|True|¿Dar mi energía a una bruja?
Arra|7|True|¡Está prohibido!
Arra|8|False|¡OLVÍDALO! ¡JAMÁS!
Pimienta|9|False|¿Prefieres ver una masacre?
Torreya|10|True|Por favor, Arra. Haz una excepción. Esas chicas y sus dragones que están peleando son nuestra escuela, nuestra familia.
Torreya|11|False|Y la tuya también.
Shichimi|12|False|Sí, por favor, Arra.
Arra|13|True|¡PFF! ¡Está bien!
Arra|14|False|¡Pero el riesgo es suyo!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡fshhhh!!
Pimienta|2|False|¡GUAU!
Pimienta|3|False|¡Así que así se siente la Rea de dragón!
Shichimi|4|False|¡Pimienta, rápido! ¡No hay tiempo que perder!
Pimienta|5|True|¡Allus... !
Pimienta|6|True|¡Yuus... !
Pimienta|7|True|¡Needum... !
Pimienta|8|True|Est...
Pimienta|9|False|...LOVIUS!
Sonido|10|False|¡¡Dzziooo!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|5|True|¡Fiizz!
Sonido|4|True|¡Dziing!
Sonido|3|True|¡¡Hfhii!
Sonido|2|True|¡¡Schii!
Sonido|1|True|¡¡Schsss!
Sonido|6|True|¡Fiizz!
Sonido|7|True|¡Dziing!
Sonido|9|True|¡Schii!
Sonido|8|True|¡Hfheee!
Sonido|10|False|Schsss!
Pimienta|11|False|Este fue mi primer intento de hechizo contra la guerra.
Pimienta|12|False|Convierte a los enemigos en amigos...
Pimienta|13|False|…y la violencia en amor y compasión.
Shichimi|14|True|¡Guau!
Shichimi|15|True|¡Es... es genial, Pimienta!
Shichimi|16|False|¡Han parado de pelear!
Torreya|17|False|¡Funciona!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Pero, ¿qué está pasando?
Shichimi|2|False|Algunos se están... ¡¿besando?!
Torreya|3|True|Vaya... ¡Esas son un montón de parejas nuevas!
Torreya|4|False|¿Era eso lo que tenía que pasar, Pimienta?
Pimienta|5|False|¡Oh, no! Creo que ha sido la Rea de dragón, que ha amplificado el amor en mi hechizo.
Torreya|6|False|Jaja, esta batalla va a ir directa a los libros de historia.
Shichimi|7|True|Jiji,
Shichimi|8|False|¡definitivamente!
Pimienta|9|False|¡Ooohhh, qué vergüenza!
Escritura|10|False|- FIN -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|15 de diciembre de 2021 Dibujo & Guion: David Revoy. Lectores de la versión beta: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Versión en castellano Traducción : Raúl García Revisión : TheFaico Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0β, Inkscape 1.1 en Kubuntu Linux 20.04. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
Pimienta|2|False|¿Sabías qué?
Pimienta|3|False|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 1036 mecenas!
Pimienta|5|False|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|6|False|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|7|False|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|8|False|¡Gracias!
