# Transcript of Pepper&Carrot Episode 04 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 4: Jijon fe Daycinon

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Fe noce lefe konkurexey fe iksir, 2:00 LMN...
Pilpil|2|True|Fe fini... Kos hin iksir, nilte, no hata Safran, abil na egalgi misu mahara!
Pilpil|3|False|Mi haja sol na eksame to yon bante ...
Pilpil|4|False|KAROOOOO~ ~OOOOOTE!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Eskrixey|14|False|MYAM
Pilpil|1|False|Karote?
Pilpil|2|False|Waopul! No hata bax bistar?
Pilpil|4|False|Karote?
Pilpil|5|False|Karote?!
Pilpil|6|False|Karote?!!!
Pilpil|3|False|Karote!
Pilpil|7|False|Mi le sati no fikir ki mi xa musi na yongu hin magikaxey...
Pilpil|8|False|Karote ci!
Soti|9|False|Krr Krr Krr Krr Krr Krr
Soti|10|False|Xx Xx Xx Xx Xx Xx Xx
Soti|11|False|Xx Xx Xx Xx Xx Xx Xx
Soti|12|False|Krr Krr Krr Krr Krr Krr Krr
Soti|13|False|Krr Krr Krr Krr Krr Krr Krr
Soti|15|False|Xx Xx Xx Xx Xx Xx Xx
Karote|16|False|Gruuuuu
Soti|17|False|Fyuuuuuuuu!
Karote|18|False|O, Solo Myawwwww!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|True|Aaa, azizu Karote! Daydenmo lutuffil myaw ci, yu le ruata yon suli vole!
Pilpil|2|False|Fe preciso momento feki mi haja misu preferido sahayyen!
Pilpil|3|True|Prehay, mi presenta tas yu misu finili ustakreaxey.
Pilpil|4|False|Iksir fe daycinon
Pilpil|5|False|Fe lutuf, am cudu glumon.
Pilpil|6|False|Eger to funsyon, yu xa yakin kingem jijon fe daycinon.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|1|False|Pok
Pilpil|3|False|WAO... Karote! Dento sen waone... kam harufi?
Pilpil|5|False|Yu... Kam yu abil na eskri?
Pilpil|6|False|E?
Pilpil|7|False|Energi?
Pilpil|8|False|Estare?
Pilpil|9|False|Ewreka?
Soti|4|False|Xrrrrrrr
Soti|2|False|Xrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|Hinto abil na mena ke aloto?
Pilpil|2|False|Emoji?
Pilpil|3|False|Emu?
Pilpil|4|False|Fe lutuf, Karote! Mi no abil na sayso. Hinto sati sen totalmo menakal!
Pilpil|5|False|...hinto sati sen totalmo menakal...
Pilpil|6|True|Grrrrrrr!!!
Pilpil|7|False|Nilto he banwatu funsyon tas mi!
Pilpil|8|True|Nenmuhim...
Pilpil|9|False|Mi haji hare watu cel na fatmin alo iksir.
Soti|10|False|KAA... AANN... CAAAA!
Pilpil|11|False|Bonsomno, Karote...
Narrator|12|False|To xa dure...
Credits|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo and special thanks to Amireeti for helping me with english corrections!
Credits|1|False|Pepper&Carrot is totally free(libre), open-source, and sponsored thanks to the help of patrons. This episode was sponsored by 156 patrons:
Credits|6|False|https://www.patreon.com/davidrevoy
Credits|5|False|Support the project, a single dollar donation per webcomic can help Pepper&Carrot a lot!
Credits|9|False|Tools: This episode was made with 100% free(libre) and open-source tools Krita, G'MIC, Blender, GIMP on Ubuntu Gnome (GNU/Linux)
Credits|8|False|Open-source: high resolution layered source files for printing, with fonts available to download, sell, modify, translate, etc...
Credits|7|False|License: Creative Commons Attribution to 'David Revoy' you can do derivations, modifications, repost, sell-it, etc...
Karote|4|False|Thank you!
