# Transcript of Pepper&Carrot Episode 04 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 4: Een geniaal moment

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|De nacht voor de toverdrankwedstrijd, rond een uur of 2 …
Pepper|2|True|Eindelijk … Met deze drank kan niemand me verslaan, zelfs Saffraan niet!
Pepper|3|False|Ik moet hem wel nog op iemand uittesten …
Pepper|4|False|CAAAAAAAA-AAAARROT!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|14|False|BROKJES
Pepper|1|False|Carrot?
Pepper|2|False|Ongelofelijk! Zelfs niet onder het bed?
Pepper|4|False|Carrot?
Pepper|5|False|Carrot?!
Pepper|6|False|Carrot?!!!
Pepper|3|False|Carrot!
Pepper|7|False|Tijd om mijn oude truc te gebruiken …
Pepper|8|False|Carrot!
Geluid|9|True|Krrr
Geluid|10|True|Krrr
Geluid|11|True|Krrr
Geluid|12|True|Krrr
Geluid|13|False|Krrr
Geluid|15|True|Schud
Geluid|16|True|Schud
Geluid|17|True|Schud
Geluid|18|True|Schud
Geluid|19|False|Schud
Geluid|21|False|Schud Schud Schud Schud Schud
Geluid|20|False|Krrr Krrr Krrr Krrr Krrr
Geluid|23|False|Krrr Krrr Krrr Krrr Krrr
Geluid|24|False|Schud Schud Schud Schud Schud
Carrot|25|False|Groe
Geluid|26|False|Pfiiieuw!
Carrot|22|False|O Sole Miauwww!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Wat goed, Carrot! Ben je helemaal uit jezelf gekomen?
Pepper|2|False|Net nu ik mijn favoriete assistent nodig heb!
Pepper|3|True|Hier, dit is mijn meesterwerk.
Pepper|4|False|De toverdrank der Genialiteit!
Pepper|5|False|Neem maar een slokje.
Pepper|6|False|Als het goed is, krijg je nu een geniaal moment.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Pok
Pepper|3|False|WAUW! Carrot! Dat is geweldig … een echte letter …
Pepper|5|False|Kun … kun je schrijven?
Pepper|6|False|E?
Pepper|7|False|“Energie”?
Pepper|8|False|“Ergernis”?
Pepper|9|False|“Emotie”?
Geluid|4|False|Kriiiieeee
Geluid|2|False|Kriiee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Wat moet dit nu betekenen?
Pepper|2|False|“Emmer”?
Pepper|3|False|“Emulsie”?
Pepper|4|False|Kom op Carrot, hier kan ik niks van maken. Wat bedoel je hiermee?
Pepper|5|False|… dit slaat echt nergens op …
Pepper|6|True|Grrrrrrr!!!
Pepper|7|False|Ik heb ook nooit eens geluk!
Pepper|8|True|Blegh …
Pepper|9|False|Ik heb nog genoeg tijd om een andere drank uit te vinden …
Geluid|10|False|CR…A…SHHHH!
Pepper|11|False|Welterusten, Carrot.
Verteller|12|False|Wordt vervolgd …
Aftiteling|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo en speciale dank aan Amireeti voor de hulp met de Engelse correcties!
Aftiteling|1|False|Pepper&Carrot is geheel vrij en open-bron door de giften van haar lezers. Deze aflevering is mogelijk gemaakt door 156 patronen:
Aftiteling|6|False|https://www.patreon.com/davidrevoy
Aftiteling|5|False|Draag bij aan dit project; een donatie van één euro helpt al bij het maken van Pepper&Carrot!
Aftiteling|9|False|Software: deze aflevering is gemaakt met 100% vrije, open-bron-software Krita, G'MIC, Blender, GIMP op Ubuntu Gnome (GNU/Linux)
Aftiteling|8|False|Open-bron: gelaagde bronbestanden in hoge resolutie zijn beschikbaar om te downloaden, te verkopen, aan te passen, te vertalen …
Aftiteling|7|False|Licentie: Creative Commons Naamsvermelding voor ‘David Revoy' Afgeleide werken, aanpassingen, commercieel gebruik enz. zijn toegestaan
Carrot|4|False|Bedankt!
