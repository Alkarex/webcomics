# Transcript of Pepper&Carrot Episode 04 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 4: Preblisk genialnosti

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Noč pred tekmovanjem v napojih, ob 2. luni zjutraj
Paprika|2|True|No, končno! S tem napitkom mi niti Žafranka ne bo segla do kolen!
Paprika|3|False|Potrebujem le še nekoga, da ga preizkusi.
Paprika|4|False|KOOREEEE~ ~EENČEEK!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|14|False|BRIKETI
Paprika|1|False|Korenček?
Paprika|2|False|Neverjetno, niti pod posteljo ga ni?
Paprika|4|False|Korenček?
Paprika|5|False|Korenček?!
Paprika|6|False|Korenček!!!
Paprika|3|False|Korenček!
Paprika|7|False|Pa ne, da bom morala uporabiti to zvijačo …
Paprika|8|False|Korenček!
Zvok|9|True|Krrc
Zvok|10|True|Krrc
Zvok|11|True|Krrc
Zvok|12|True|Krrc
Zvok|13|False|Krrc
Zvok|15|True|Šššš
Zvok|16|True|Šššš
Zvok|17|True|Šššš
Zvok|18|True|Šššš
Zvok|19|False|Šššš
Zvok|21|False|Šššš Šššš Šššš Šššš Šššš
Zvok|20|False|Krrc Krrc Krrc Krrc Krrc
Zvok|23|False|Krrc Krrc Krrc Krrc Krrc
Zvok|24|False|Šššš Ššš Šššš Šššš Šššš
Korenček|25|False|Kruuul
Zvok|26|False|Fiiiijuuuuu
Korenček|22|False|O sole mjauuu!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|O, Korenček, ti ljubi mucek, ti! Kar sam od sebe si prišel!
Paprika|2|False|Ravno v trenutku, ko potrebujem svojega najljubšega pomočnika!
Paprika|3|True|Predstavljam ti svojo končno mojstrovino:
Paprika|4|False|Napoj genialnosti!
Paprika|5|False|Naredi požirek.
Paprika|6|False|Če napoj deluje, boš za kratek čas dobil preblisk genialnosti.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Krc
Paprika|3|False|Vau! Korenček! To je neverjetno! To so črke…
Paprika|5|False|A slučajno … pišeš?
Paprika|6|False|E?
Paprika|7|False|Energija?
Paprika|8|False|Evolucija?
Paprika|9|False|Emocija?
Zvok|4|False|Ššššššššššš
Zvok|2|False|Šššš

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Kaj pa je potem?
Paprika|2|False|Emulzija?
Paprika|3|False|Eureka?
Paprika|4|False|Daj no, Korenček, zmanjkuje mi besed. Tole nima nobenega smisla!
Paprika|5|False|To res nima nobenega smisla …
Paprika|6|True|Grrrrrrr!!!
Paprika|7|False|Nikoli mi nič ne uspe!
Paprika|8|True|Pha…
Paprika|9|False|Saj imam še čas, da namešam kak drug napoj.
Zvok|10|False|TRESK!
Paprika|11|False|Sladke sanje, Korenček.
Pripovedovalec|12|False|Se nadaljuje …
Zasluge|13|False|David Revoy November 2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo in posebna zahvala Amireeti za pomoč pri angleških popravkih!
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 156 bralcev:
Zasluge|6|False|https://www.patreon.com/davidrevoy
Zasluge|5|False|Prevedla Andrej Ficko in Gorazd Gorup Že en evro lahko zelo pomaga pri nastajanju stripa, zato bomo neizmerno veseli vsake podpore!
Zasluge|9|False|Orodja: Ta epizoda je nastala s 100% prostimi in odprtokodnimi orodji Krita, G'MIC, Blender, GIMP na Ubuntu Gnomeu (GNU/Linux)
Zasluge|8|False|Prost dostop: na voljo so izvorne datoteke pisav in slikovnega gradiva, ki jih je mogoče prenesti, natisniti, prodajati, prirejati, prevesti itd.
Zasluge|7|False|Licenca: Avtorstvo priznano Davidu Revoyju pod licenco Creative Commons, Vsebino smete uporabiti za lastna izpeljana dela, popravke ali priredbe, delo ponovno objavite drugod, ga prodate itd.
Korenček|4|False|Hvalaaaa!
