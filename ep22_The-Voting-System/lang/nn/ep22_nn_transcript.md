# Transcript of Pepper&Carrot Episode 22 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 22: Røystesystemet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|1|False|Vår store trolldoms-konkurranse kan endeleg starta!
Skrift|2|False|Trolldomkonkurranse

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|1|False|Og takka vere dei geniale ingeniørane våre kan de òg delta!
Ordføraren i Komona|2|False|Ja, det stemmer, mine vener. Sjå nærare på desse teknologiske underverka som publikumsvertane no deler ut!
Ordføraren i Komona|3|False|Det er no de som vel vinnaren! Trykk på den grøne knappen for å gje ein deltakar eit poeng eller på den raude for å gje deltakaren eit minuspoeng.
Ordføraren i Komona|4|True|Kva så med juryen, lurer de kanskje på?
Ordføraren i Komona|5|False|Slapp av – me har tenkt på alt!
Ordføraren i Komona|6|False|Jurymedlemmane har fått ein spesial-trykkjar som gjev heile hundre pluss-eller minuspoeng!
Pepar|7|False|Jøss!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|5|False|... 50 000 Ko!
Ordføraren i Komona|1|False|Og prikken over i-en: Poenga vert viste fram over hovuda på deltakarane!
Ordføraren i Komona|3|False|Dei tre heksene med flest poeng går vidare til finalen!
Ordføraren i Komona|4|True|Og hugs at vinnaren får med seg fyrste-premien på utrulege ...
Skrift|2|False|1337
Publikum|6|False|Klapp

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Gulrot|2|False|Trykk
Pepar|3|False|Eit fantastisk opplegg, ikkje sant, Gulrot?
Pepar|4|True|Nyskapande ...
Pepar|5|True|Underhaldande ...
Pepar|6|True|Demokratisk ...
Pepar|7|False|... eit perfekt system!
Pepar|8|True|Me treng ikkje lenger ekspertar til å vurdera kvaliteten!
Pepar|9|False|For eit framsteg! Me lever verkeleg i ei fantastisk tid!
Ordføraren i Komona|10|False|Har alle funne fram trykkjaren sin?
Publikum|11|False|Supert!
Publikum|12|False|Ja!
Publikum|13|False|Jepp!
Publikum|14|False|Ja!
Publikum|15|False|Ja!
Ordføraren i Komona|16|True|Bra!
Ordføraren i Komona|17|True|La konkurransen ...
Ordføraren i Komona|18|False|STARTA!!!
Publikum|1|False|Klapp

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|1|False|Kamille er fyrste deltakar ut!
Lyd|2|False|Dgiiioo ...
Kamille|3|False|SYLVESTRIS!
Lyd|4|False|Smell!
Publikum|5|True|Trykk
Publikum|6|True|Trykk
Publikum|7|True|Trykk
Publikum|8|True|Trykk
Publikum|9|True|Trykk
Publikum|10|True|Trykk
Publikum|11|False|Trykk

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|2|False|Ei sterk opning av Kamille! Så er det Shichimi sin tur!
Skrift|1|False|5861
Shichimi|4|True|LUX ...
Lyd|6|False|Visssssl!
Shichimi|5|False|MAXIMA!
Publikum|8|False|Ååå!
Publikum|7|False|Au!
Publikum|9|False|Auga mine!!!
Pepar|12|False|Gulrot ... Gje henne ei stemme likevel. Ho er jo venen vår ...
Gulrot|13|False|Trykk
Publikum|10|True|Buu!
Publikum|11|False|Buuu!
Publikum|14|True|Buuu!
Publikum|15|False|Buuu!
Ordføraren i Komona|17|False|Her vart nok publikum blenda på feil måte av denne «strålande» framvisinga! Me går vidare til Spirulina!
Skrift|16|False|−42
Publikum|18|True|Buuuu!
Publikum|19|True|Buuu!
Publikum|20|True|Buuu!
Publikum|21|False|Buu!
Publikum|3|False|Klapp

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spirulina|1|False|LAUSLATUS KRAKENIS!
Lyd|2|False|Flooo!
Lyd|3|False|Splooosj!
Ordføraren i Komona|5|False|Vakkert! Storslege! Spirulina tek leiinga!
Skrift|4|False|6225
Spirulina & Durian|6|False|Klask!
Koriander|8|False|MORTUS REDITUS!
Lyd|9|False|Knuuuur!
Ordføraren i Komona|11|False|Hm, det ser ut til at skjelett ikkje heilt fengjer lenger. Då er det vår eiga Safran sin tur!
Skrift|10|False|2023
Publikum|7|False|Klapp

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Grrr! Eg kan ikkje la meg slå av Spirulina! No må eg gje alt!
Safran|2|False|Hald deg unna, Trøffel!
Lyd|3|False|Frrrssj!
Lyd|4|False|Frrrssj!
Lyd|5|False|Skrap!
Trøffel|6|False|Mjau!
Safran|7|False|SPIRALIS
Safran|8|False|FLAMMAaaaaaah!
Lyd|9|False|Fvvvoooosj!
Lyd|10|False|Svipp!
Lyd|11|False|Fsssj!
Lyd|12|False|Paff!
Safran|13|False|?!!
Lyd|14|False|Fsssj!
Lyd|15|False|Fsssj!
Safran|16|False|Aj-aj-aj!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Nei, å nei ... Så flautt!
Safran|3|False|?!
Skrift|4|True|14
Skrift|5|False|849
Pepar|6|False|Trykk
Lord Azeirf|7|True|Trykk
Lord Azeirf|8|True|Trykk
Lord Azeirf|9|False|Trykk
Skrift|10|True|18
Skrift|11|False|231
Ordføraren i Komona|12|True|Kom igjen ... Oppfør dykk no!
Ordføraren i Komona|13|False|Shichimi og Koriander er ute av konkurransen!
Ordføraren i Komona|14|False|Og Kamille, Spirulina og Safran er vidare til finalen!
Publikum|2|False|Klapp

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|1|False|Hekser, er de klare for ei siste utfordring?
Ordføraren i Komona|2|False|Set i gang!
Skrift|3|True|15
Skrift|4|False|703
Skrift|5|True|19
Skrift|6|False|863
Skrift|7|True|13
Skrift|8|False|614
Lyd|10|False|Dunk!
Pepar|11|False|Eg trekkjer tilbake alt eg sa om dette røystesystemet ...
Lord Azeirf|12|True|Trykk
Lord Azeirf|13|True|Trykk
Lord Azeirf|14|False|Trykk
Forteljar|15|False|– SLUTT –
Publikum|9|False|Klapp

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Mai 2017 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Bidragsytarar|2|False|Forbetring av dialogen: Nicolas Artance, Valvin og Craig Maloney.
Bidragsytarar|3|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|4|False|Programvare: Krita 3.1.3 og Inkscape 0.92.1 på Linux Mint 18.1
Bidragsytarar|5|False|Lisens: Creative Commons Attribution 4.0
Bidragsytarar|6|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 864 som støtta denne episoden:
Bidragsytarar|7|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
