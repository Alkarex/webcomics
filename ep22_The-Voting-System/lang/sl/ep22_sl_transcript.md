# Transcript of Pepper&Carrot Episode 22 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 22: Glasovalni sistem

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|1|False|Naše veliko tekmovanje v čaranju se lahko končno prične!
Napis|2|False|Tekmovanje v čaranju

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|1|False|In zahvaljujoč našim genialnim inženirjem boste lahko sodelovali vsi!
Župan Komone|2|False|Tako je, kar napasite si oči na tem tehnološkem čudežu, ki vam ga bodo prinesli hostese in hostesniki!
Župan Komone|3|False|Zeleni gumb kandidatu dodeli eno točko, rdeči gumb pa eno odvzame. Vi odločite!
Župan Komone|4|True|„Kaj pa žirija?“ vas slišim.
Župan Komone|5|False|Ne skrbite, na vse smo pomislili!
Župan Komone|6|False|Vsak žirant ima poseben daljinec, ki lahko doda ali odbije sto točk z enim samim pritiskom!
Paprika|7|False|Vau!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|5|False|… 50 000 kojev!
Župan Komone|1|False|Za piko na i se bodo seštevki točk prikazali nad glavami tekmovalk!
Župan Komone|3|False|Tri čarovnice z največ točkami napredujejo v finale!
Župan Komone|4|True|Ne pozabite, glavna zmagovalka bo za nagrado prejela neverjeten znesek …
Napis|2|False|1337
Občinstvo|6|False|Plosk

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Korenček|2|False|Tap
Paprika|3|False|Kako krasna zasnova, kajne, Korenček?
Paprika|4|True|Domiselna …
Paprika|5|True|Zabavna …
Paprika|6|True|Demokratična …
Paprika|7|False|Popoln sistem!
Paprika|8|True|Kakovosti ne ocenjujejo več samo strokovnjaki!
Paprika|9|False|Kakšen napredek! Zares živimo v fantastičnih časih!
Župan Komone|10|False|Imate vsi svoje daljince?
Občinstvo|11|False|Super!
Občinstvo|12|False|Ja!
Občinstvo|13|False|Juhu!
Občinstvo|14|False|Ja!
Občinstvo|15|False|Jaa!
Župan Komone|16|True|Dobro!
Župan Komone|17|True|Naj se tekmovanje …
Župan Komone|18|False|PRIČNE!!
Občinstvo|1|False|Plosk

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|1|False|Prva bo nastopila Kamilica!
Zvok|2|False|Dgiiioo …
Kamilica|3|False|SYLVESTRIS!
Zvok|4|False|Bam!
Občinstvo|5|True|Tap
Občinstvo|6|True|Tap
Občinstvo|7|True|Tap
Občinstvo|8|True|Tap
Občinstvo|9|True|Tap
Občinstvo|10|True|Tap
Občinstvo|11|False|Tap

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|2|False|Kamilica je na dobri poti! In že je na vrsti Šičimi!
Napis|1|False|5861
Šičimi|4|False|LUX …
Zvok|5|False|Fiiiiiiizz!!
Šičimi|6|False|MAXIMA!
Občinstvo|7|False|Aa!!
Občinstvo|8|False|Au!!
Občinstvo|9|False|Moje oči!!!
Paprika|12|False|Korenček … Vseeno ji dodeli točke. Konec koncev je najina prijateljica …
Korenček|13|False|Tap
Občinstvo|10|True|Buu!
Občinstvo|11|False|Buuu!
Občinstvo|14|True|Buuu!
Občinstvo|15|False|Buuu!
Župan Komone|17|False|Ah, kot kaže, „slepeča“ predstava ni navdušila občinstva! Nadaljujemo k Spirulini!
Napis|16|False|-42
Občinstvo|18|True|Buuuu!
Občinstvo|19|True|Buuu!
Občinstvo|20|True|Buuu!
Občinstvo|21|False|Buu!
Občinstvo|3|False|Plosk

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spirulina|1|False|IZPUSTITIS KRAKENIS!
Zvok|2|False|Vuuloou!
Zvok|3|False|Pljusk!
Župan Komone|5|False|Čudovito! Mogočno! Spirulina je prevzela vodstvo! Koriandrika, sedaj pa k tebi!
Napis|4|False|6225
Spirulina in Durian|6|False|Tap
Koriandrika|8|False|MORTUS REDITUS!
Zvok|9|False|Gruuvouu!
Župan Komone|11|False|Hm, videti je, da okostnjaki niso več v modi … Sedaj pa k naši preljubi Žafranki!
Napis|10|False|2023
Občinstvo|7|False|Plosk

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Žafranka|1|False|Grrr, niti pod razno si ne bom dopustila biti slabša od Spiruline! Vse moram dati od sebe!
Žafranka|2|False|Umakni se, Gomoljika!
Zvok|3|False|Frrršš!
Zvok|4|False|Frrršš!
Zvok|5|False|Švist!
Gomoljika|6|False|Mijav!
Žafranka|7|False|SPIRALIS
Žafranka|8|False|FLAMAaaaaaaah!
Zvok|9|False|Fvuuuuušš!
Zvok|10|False|Svvviiip!
Zvok|11|False|Fššš!
Zvok|12|False|Paf!
Žafranka|13|False|?!!
Zvok|14|False|Fššš!
Zvok|15|False|Fššš!
Žafranka|16|False|Aaaaah!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Žafranka|1|False|O, ne… Kakšna blamaža!
Žafranka|3|False|?!
Napis|4|True|14
Napis|5|False|849
Paprika|6|False|Tap
Knez Azeirf|7|True|Tap
Knez Azeirf|8|True|Tap
Knez Azeirf|9|False|Tap
Napis|10|True|18
Napis|11|False|231
Župan Komone|12|True|No, no, prosim za malo uvidevnosti!
Župan Komone|13|False|Šičimi in Koriandrika sta izločeni!
Župan Komone|14|False|Kamilica, Spirulina in Žafranka so se uvrstile v finale!
Občinstvo|2|False|Plosk

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Župan Komone|1|False|Dame, ste pripravljene na zadnji izziv?
Župan Komone|2|False|Zdaj!
Napis|3|True|15
Napis|4|False|703
Napis|5|True|19
Napis|6|False|863
Napis|7|True|13
Napis|8|False|614
Zvok|10|False|Plonk!
Paprika|11|False|Nazaj vzamem vse, kar sem rekla o tem sistemu …
Knez Azeirf|12|True|Tap
Knez Azeirf|13|True|Tap
Knez Azeirf|14|False|Tap
Pripovedovalec|15|False|- KONEC -
Občinstvo|9|False|Plosk

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Maj 2017 - www.peppercarrot.com - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Zasluge|2|False|Izboljšave dialoga: Nicolas Artance, Valvin in Craig Maloney.
Zasluge|3|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|4|False|Programska oprema: Krita 3.1.3 in Inkscape 0.92.1 na Linux Mint 18.1
Zasluge|5|False|Licenca: Creative Commons Priznanje avtorstva 4.0.
Zasluge|6|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 864 bralcev:
Zasluge|7|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
